# gkbpay
=======
# GKB PAY BACKEND

A Node server using ExpressJs and MongoDB as database
## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes.

### Prerequisites

What things you need to install the software and how to install them

```
Install MongoDB for your OS(Windows/ Linux)
https://docs.mongodb.com/manual/administration/install-community/

Install NodeJS 8 and npm 5 and above for running this project.
https://nodejs.org/en/ 
```

### Running Backend Server


#####If running application for the first time
```
npm install 
```

#####Run Node Server

```
npm start
```

#####The Backend Server is up on port 8000.
```
localhost:8000
```



To Start the application run following commands:-
    1. npm install
    2. npm start