const constant = require('./../utils/constant');
const utils = require('../utils/utilities');
exports.getWelcomeSMS = function (firstname) {
    return 'Hello ' + firstname + ', \nCongratulations. Your Profile has been successfully created. Stay tuned for more Updates\n\nRegards\nTeam GKBPAY'
};
exports.changePasswordSMS = function (firstname) {
    return 'Hello ' + firstname + 'Your Password has been changed successfully, Please Log in to your account for further operations.\n\nRegards\nTeam GKBPAY'
};
exports.getBatchDeletionSMS = function (firstname, batchId, batchName) {
    return 'Hello ' + firstname + 'This is to inform that the ABC Batch( ' + batchId + ' and ' + batchName + ' ) is deleted successfully. Please Log in to your account for further updates.\n\nRegards\nTeam ABC'
};
exports.getBatchCreationSMS = function (firstname, batchId, batchName) {
    return 'Hello ' + firstname + 'This is to inform that the ABC Batch( ' + batchId + ' and ' + batchName + ' ) is created. Please Log in to your account for further updates.\n\nRegards\nTeam ABC'
};
exports.createMockTestSMS = function (firstname, mockData) {
    return 'Hello ' + firstname + 'This is to inform that you have been selected for the Mock Interview - ( ' + mockData.mockTopic + ' ) on ( ' + mockData.mockDate + ' & ' + mockData.mockTime + ' of Mock Interview) by the Trainer( ' + mockData.mockCreatedUserId + ' & ' + mockData.mockCreatedBy + ' ).Please Log in to your account for further updates.\n\nRegards\nTeam ABC'
};
exports.updateMockTestSMS = function (firstname, mockData) {
    return 'Hello ' + firstname + 'This is to inform that you have been selected for the Mock Interview - ( ' + mockData.mockTopic + ' ) on ( ' + mockData.mockDate + ' & ' + mockData.mockTime + ' of Mock Interview) by the Trainer( ' + mockData.mockCreatedUserId + ' & ' + mockData.mockCreatedBy + ' )has been rescheduled to ( ' + mockData.newMockDate + ' & ' + mockData.newMockTime + ' of Mock Interview).Please Log in to your account for further updates.\n\nRegards\nTeam ABC'
};
exports.cancelMockTestSMS = function (firstname, mockData) {
    return 'Hello ' + firstname + 'This is to inform that the scheduled Mock Interview - ( ' + mockData.mockTopic + ' ) on ( ' + mockData.mockDate + ' & ' + mockData.mockTime + ' of Mock Interview) by the Trainer( ' + mockData.mockCreatedUserId + ' & ' + mockData.mockCreatedBy + ' ) has been cancelled due to ( ' + mockData.cancelReason + ' )\n\nWe will keep you posted on the further schedule for the same. Please Log in to your account for further updates.\n\nRegards\nTeam ABC'
};
exports.sendOTPSMS = function (name, otp) {
    return `Hello ${name}, This is to inform you that the OTP is ${otp}, please note that the OTP will expires in 3 Minutes.`
};


exports.getWelcomeEmail = function (toEmail, name) {
    return {
        to: toEmail, // list of receivers
        subject: 'Welcome to GKBPay', // Subject line
        text: 'Dear Customer,',
        html:`
            <p>Welcome to GKBPay.</p>
            <p>Your unique customer code will be generated in 72 hrs subject to final verification.</p>
            <p>Thank you for choosing GKBHTL for your lens requirements.</p><br/>
            <span>Sincerely,</span><br/>
            <span>GKBPay Team</span><br/>
            <span style="border-bottom: 1px dashed grey;"></span><br/> 
            <span style="color: grey;">GKB Hi-Tech Lenses Pvt Ltd, Goa, India</span>
            `
    };
};
exports.createKYCEmail = function (toEmail, name, custCode) {
    return {
        to: toEmail, // list of receivers
        subject: 'Welcome to GKBPay', // Subject line
        text: 'Dear '+name+',',
        html: '<p>New KYC created for '+custCode+' .</p>' +
            '<span>Sincerely,</span>' +
            '<span>GKBPay Team</span>' +
            '<span style="border-bottom: 1px dashed grey;"></span>' + 
            '<span style="color: grey;">GKB Hi-Tech Lenses Pvt Ltd, Goa, India</span>'
    };
};
exports.getActivatedInviteEmail = function (toEmail, name) {
    return {
        to: toEmail, // list of receivers
        subject: 'Welcome to GKBPay', // Subject line
        text: 'Dear ' + name + ',',
        html: '<span>Dear ' + name + ',</span>'+
            '<p>Welcome to GKBPay.</p>' +
            '<span>Thank you for choosing GKBHTL for your lens requirements.</span>' +
            '<span>You can now login with your registered mobile number.</span><br/>' +
            '<span>Sincerely,</span>' +
            '<span>GKBPay Team</span>' +
            '<span style="border-bottom: 1px dashed grey;"></span>' + 
            '<span style="color: grey;">GKB Hi-Tech Lenses Pvt Ltd, Goa, India</span>'
    };
};
exports.sendOTPEmail = function (toEmail, name, otp) {
    return {
        to: toEmail, // list of receivers
        subject: 'Intimation of OTP ', // Subject line
        text: 'Dear ' + name + ',',
        html: 'Dear ' + name + ',<br/>' +
            'This is to inform you that the OTP is <b>' + otp + ' </b> <br />' +
            'If you have any concerns or queries, we’re happy to help. Please reach out to us at gkbpayapp@gkbhitech.com.</p>' +
            '<span>Sincerely,</span>' +
            '<span>GKBPay Team</span>' +
            '<span style="border-bottom: 1px dashed grey;"></span>' + 
            '<span style="color: grey;">GKB Hi-Tech Lenses Pvt Ltd, Goa, India</span>'
    };
};
exports.sendForgotOTPEmail = function (toEmail, name, otp) {
    return {
        to: toEmail, // list of receivers
        subject: 'Forgot Password ', // Subject line
        text: 'Hello ' + name + 'Your Password has been changed successfully, Please Log in to your account for further operations.',
        html: 'Hello ' + name + ',<br/><br/>' +
            'The OTP for reset password is: <b>' + otp + '</b> <br/> <br/>' +
            'If you have any concerns or queries, we’re happy to help. Please reach out to us at contact@gkbpay.com.</p>' +
            '<p><b>Regards,</b></p>' +
            '<p><b>Team GKBPAY</b></p>'// html body
    };
};
exports.sendWelcomeMessageEmail = function (toEmail, name, ) {
    let now = new Date().toLocaleString("en-US", { timeZone: "Asia/Kolkata" });
            now = new Date(now);

    return {
        to: toEmail, // list of receivers
        subject: 'Welcome to GKBPAY', // Subject line
        text: 'Hello ' + name + 'Your Password has been changed successfully, Please Log in to your account for further operations.',
        html: 'Hello ' + name + ',<br/><br/>' +
            'You have successfully loggedIn from IP: ' + ipaddress + 'at ' +utils.convert24To12Hour( now.getHours() + ':' + now.getMinutes()) + ' <br />' +
            'If you have any concerns or queries, we’re happy to help. Please reach out to us at contact@gkbpay.com.</p>' +
            '<p><b>Regards,</b></p>' +
            '<p><b>Team GKBPAY</b></p>'// html body
    };
};
exports.sendMarksChangedEmail = function (toEmail, name, RegId, ipAddress) {
    let now = new Date().toLocaleString("en-US", { timeZone: "Asia/Kolkata" });
            now = new Date(now);

    return {
        to: toEmail, // list of receivers
        subject: 'Marks UnFreezed For change', // Subject line
        text: 'Hello ' + name + 'Your Password has been changed successfully, Please Log in to your account for further operations.',
        html: 'Hi ' + name + ',<br/><br/>' +
            'You have loggedId from IP: '+ipAddress+' to unfreeze the marks of student having registrationId: ' + RegId + ' at ' +utils.convert24To12Hour( now.getHours() + ':' + now.getMinutes()) + ' <br />' +
            'If this is not you, Please reach out to us at contact@gkbpay.com.</p>' +
            '<p><b>Regards,</b></p>' +
            '<p><b>Team GKBPAY</b></p>'// html body
    };
};


exports.sendExamCenterPwdEmail = function (toEmail, examCenterCode, password ) {
    let now = new Date().toLocaleString("en-US", { timeZone: "Asia/Kolkata" });
            now = new Date(now);

    return {
        to: toEmail, // list of receivers
        subject: 'Exam Center Login', // Subject line
        text: 'Hello Please find the login details for Examination.',
        html: 'Hello ' + ',<br/><br/>' +
            'Please find the login details for Examination ' +
            '<b>Username : ' + examCenterCode + '</b><br/></p>' +
            '<b>Password : ' + password + '</b><br/>' +
            'If you have any concerns or queries, we’re happy to help. Please reach out to us at contact@gkbpay.com.</p>' +
            '<p><b>Regards,</b></p>' +
            '<p><b>Team GKBPAY</b></p>'// html body
    };
};

exports.sendValuatorPwdEmail = function (toEmail, username, password ) {
    let now = new Date().toLocaleString("en-US", { timeZone: "Asia/Kolkata" });
            now = new Date(now);

    return {
        to: toEmail, // list of receivers
        subject: 'Valuator Login', // Subject line
        text: 'Hello Please find the login details for Valuations.',
        html: 'Hello ' + ',<br/><br/>' +
            'Please find the login details for Valuations ' +
            '<b>Username : ' + username + '</b><br/></p>' +
            '<b>Password : ' + password + '</b><br/>' +
            'If you have any concerns or queries, we’re happy to help. Please reach out to us at contact@gkbpay.com.</p>' +
            '<p><b>Regards,</b></p>' +
            '<p><b>Team GKBPAY</b></p>'// html body
    };
};
exports.sendReviewerPwdEmail = function (toEmail, username, password ) {
    let now = new Date().toLocaleString("en-US", { timeZone: "Asia/Kolkata" });
            now = new Date(now);

    return {
        to: toEmail, // list of receivers
        subject: 'Reviewer Login', // Subject line
        text: 'Hello Please find the login details for revaluation.',
        html: 'Hello ' + ',<br/><br/>' +
            'Please find the login details for Revaluation ' +
            '<b>Username : ' + username + '</b><br/></p>' +
            '<b>Password : ' + password + '</b><br/>' +
            'If you have any concerns or queries, we’re happy to help. Please reach out to us at contact@gkbpay.com.</p>' +
            '<p><b>Regards,</b></p>' +
            '<p><b>Team GKBPAY</b></p>'// html body
    };
};
exports.sendRevaluatorPwdEmail = function (toEmail, username, password ) {
    let now = new Date().toLocaleString("en-US", { timeZone: "Asia/Kolkata" });
            now = new Date(now);

    return {
        to: toEmail, // list of receivers
        subject: 'Revaluator Login', // Subject line
        text: 'Hello Please find the login details for revaluation.',
        html: 'Hello ' + ',<br/><br/>' +
            'Please find the login details for Revaluation ' +
            '<b>Username : ' + username + '</b><br/></p>' +
            '<b>Password : ' + password + '</b><br/>' +
            'If you have any concerns or queries, we’re happy to help. Please reach out to us at contact@gkbpay.com.</p>' +
            '<p><b>Regards,</b></p>' +
            '<p><b>Team GKBPAY</b></p>'// html body
    };
};
// sendExaminerPwdEmail
exports.sendExaminerPwdEmail = function (toEmail, username, password ) {
    let now = new Date().toLocaleString("en-US", { timeZone: "Asia/Kolkata" });
            now = new Date(now);

    return {
        to: toEmail, // list of receivers
        subject: 'Examiner Login', // Subject line
        text: 'Hello Please find the login details for Examiner.',
        html: 'Hello ' + ',<br/><br/>' +
            'Please find the login details for Examiner ' +
            '<b>Username : ' + username + '</b><br/></p>' +
            '<b>Password : ' + password + '</b><br/>' +
            'If you have any concerns or queries, we’re happy to help. Please reach out to us at contact@gkbpay.com.</p>' +
            '<p><b>Regards,</b></p>' +
            '<p><b>Team GKBPAY</b></p>'// html body
    };
};


exports.sendCollegePwdEmail = function (toEmail, username, password ) {
    let now = new Date().toLocaleString("en-US", { timeZone: "Asia/Kolkata" });
            now = new Date(now);

    return {
        to: toEmail, // list of receivers
        subject: 'College Login', // Subject line
        text: 'Hello Please find the login details for College.',
        html: 'Hello ' + ',<br/><br/>' +
            'Please find the login details for College ' +
            '<b>Username : ' + username + '</b><br/></p>' +
            '<b>Password : ' + password + '</b><br/>' +
            'If you have any concerns or queries, we’re happy to help. Please reach out to us at contact@gkbpay.com.</p>' +
            '<p><b>Regards,</b></p>' +
            '<p><b>Team GKBPAY</b></p>'// html body
    };
};
// exports.getWelcomeEmailStudent = function (toEmail, name, username, password) {
//     return {
//         to: toEmail, // list of receivers
//         subject: 'Welcome to ABC', // Subject line
//         text: 'Welcome!' +
//             'Hi ' + name + ', We’re glad you’re here!' +
//             'We are happy to inform that your account has been created for the ABC Portal and the details for the same are mentioned below' +
//             'Your account Information :' +
//             'Username :' +
//             'Password :' +
//             'Email :' +
//             'Remember to make it a great time. We will see you soon.' +
//             'This email contains private information for your ABC account — please don’t forward it.' +
//             'For any Questions & enquiries, Email us at info@abctechtraining.com.',
//         html: 'Welcome!<br/>' +
//             '<p>Hello ' + name + ', </p>' +
//             '<p>Greetings from ABC… </p>' +

//             '<p>We are happy to inform that your ABC account has been successfully created. Please go through the below-mentioned User Account Credentials.</p>' +
//             '<p>Your account Information :<br/>' +
//             '<b>Login URL: <a href="http://portal.abctechtraining.com">portal.abctechtraining.com</a></b><br/>' +
//             '<b>Username : ' + username + '</b><br/>' +
//             '<b>Password : ' + password + '</b><br/>' +
//             '<b>Email : ' + toEmail + '</b><br/></p>' +
//             '<p>Kindly Login to your account for updating your Profile. Contact the ABC Support team for further concerns.</p>' +
//             '<p>We wish you all the very best.</p>' +
//             '<p><b>Note:</b></p>' +
//             '<p>This email contains private information for your ABC account — Please don’t forward it.<br/>' +
//             'For any Questions & enquiries, Email us at support@abctechtraining.com.</p>' +
//             '<p><b>Regards,</b></p>' +
//             '<p><b>ABC Team</b></p>'// html body
//     };
// };
// exports.studentChangedPasswordEmail = function (toEmail, name) {
//     return {
//         to: toEmail, // list of receivers
//         subject: 'Changed Password', // Subject line
//         text: 'Hello ' + name + 'Your Password has been changed successfully, Please Log in to your account for further operations.',
//         html: 'Hello ' + name + ',<br/>' +
//             'Your Password has been changed successfully<br/>, Please Log in to your account for further operations.<br/><br/>' +
//             'If you have any concerns or queries, we’re happy to help. Please reach out to us at support@abctechtraining.com</p>' +
//             '<p><b>Regards,</b></p>' +
//             '<p><b>Team ABC</b></p>'// html body
//     };
// };

// exports.getBatchDeletionEmail = function (toEmail, name, batchName, batchId) {
//     return {
//         to: toEmail, // list of receivers
//         subject: 'Batch Deletion', // Subject line
//         text: 'Hello ' + name + 'Your Password has been changed successfully, Please Log in to your account for further operations.',
//         html: 'Hello ' + name + ',<br/>' +
//             'This is to inform that the ABC Batch' + ' ( ' + batchId + ' and ' + batchName + ' ) ' + 'is deleted successfully. Please Log in to your account for further updates.<br/><br/>' +
//             'If you have any concerns or queries, we’re happy to help. Please reach out to us at support@abctechtraining.com</p>' +
//             '<p><b>Regards,</b></p>' +
//             '<p><b>Team ABC</b></p>'// html body
//     };
// };
// exports.getBatchCreationEmail = function (toEmail, name, batchName, batchId) {
//     return {
//         to: toEmail, // list of receivers
//         subject: 'Batch Creation', // Subject line
//         text: 'Hello ' + name + 'Your Password has been changed successfully, Please Log in to your account for further operations.',
//         html: 'Hello ' + name + ',<br/>' +
//             'This is to inform that the ABC Batch' + ' ( ' + batchId + ' and ' + batchName + ' ) ' + 'is created. Please Log in to your account for further updates.<br/><br/>' +
//             'If you have any concerns or queries, we’re happy to help. Please reach out to us at support@abctechtraining.com</p>' +
//             '<p><b>Regards,</b></p>' +
//             '<p><b>Team ABC</b></p>'// html body
//     };
// };


// exports.createMockTestEmail = function (toEmail, name, mockData) {
//     return {
//         to: toEmail, // list of receivers
//         subject: 'Mock Test Creation', // Subject line
//         text: 'Hello ' + name + 'Your Password has been changed successfully, Please Log in to your account for further operations.',
//         html: 'Hello ' + name + ',<br/>' +
//             'This is to inform that you have been selected for the Mock Interview - ' + mockData.mockTopic + ' on ' + mockData.mockDate + ' & ' + mockData.mockTime + ' by the Trainer( ' + mockData.mockCreatedUserId + ' & ' + mockData.mockCreatedBy + ' ).Please Log in to your account for further updates.<br/> <br/>' +
//             'If you have any concerns or queries, we’re happy to help. Please reach out to us at support@abctechtraining.com</p>' +
//             '<p><b>Regards,</b></p>' +
//             '<p><b>Team ABC</b></p>'// html body
//     };
// };

// exports.updateMockTestEmail = function (toEmail, name, mockData) {
//     return {
//         to: toEmail, // list of receivers
//         subject: 'Mock Test Updation', // Subject line
//         text: 'Hello ' + name + 'Your Password has been changed successfully, Please Log in to your account for further operations.',
//         html: 'Hello ' + name + ',<br/>' +
//             'This is to inform that the scheduled Mock Interview - ' + mockData.mockTopic + ' on ( ' + mockData.mockDate + ' & ' + mockData.mockTime + ' ) by the Trainer( ' + mockData.mockCreatedUserId + ' & ' + mockData.mockCreatedBy + ' )has been rescheduled to ( ' + mockData.newMockDate + ' & ' + mockData.newMockTime + ' of Mock Interview).Please Log in to your account for further updates.<br/> <br/>' +
//             'If you have any concerns or queries, we’re happy to help. Please reach out to us at support@abctechtraining.com</p>' +
//             '<p><b>Regards,</b></p>' +
//             '<p><b>Team ABC</b></p>'// html body
//     };
// };

// exports.cancelMockTestEmail = function (toEmail, name, mockData) {
//     return {
//         to: toEmail, // list of receivers
//         subject: 'Mock Test Cancelled', // Subject line
//         text: 'Hello ' + name + 'Your Password has been changed successfully, Please Log in to your account for further operations.',
//         html: 'Hello ' + name + ',<br/>' +
//             'This is to inform that the scheduled Mock Interview - ' + mockData.mockTopic + ' on ' + mockData.mockDate + ' & ' + mockData.mockTime + ' by the Trainer( ' + mockData.mockCreatedUserId + ' & ' + mockData.mockCreatedBy + ' ) has been cancelled due to ( ' + mockData.cancelReason + ' ). <br/><br/>' +
//             'We will keep you posted on the further schedule for the same. Please Log in to your account for further updates.<br/><br/>' +
//             'If you have any concerns or queries, we’re happy to help. Please reach out to us at support@abctechtraining.com</p>' +
//             '<p><b>Regards,</b></p>' +
//             '<p><b>Team ABC</b></p>'// html body
//     };
// };

// exports.createPlacementEmail = function (toEmail, name, placementData, bdUser) {
//     return {
//         to: toEmail, // list of receivers
//         subject: 'Placement Creation.', // Subject line
//         text: 'Hello ' + name + 'Your Password has been changed successfully, Please Log in to your account for further operations.',
//         html: 'Hello ' + name + ',<br/>' +
//             'This is to inform that the new Placement ( ' + placementData.placementId + ' ) has been created Successfully by the BD Manager ( ' + bdUser.name + ' ) for the ( ' + placementData.driveType + ' ) on ( ' + placementData.date + ' & ' + placementData.time + ' ) at ( ' + placementData.driveLocation + ' ). <br/><br/>' +
//             'If you have any concerns or queries, Please contact the concerned BD Manager on the same.</p>' +
//             '<p><b>Regards,</b></p>' +
//             '<p><b>Team ABC</b></p>'// html body
//     };
// };
// exports.adminApprovalToBdEmail = function (toEmail, name, placementData, bdUser) {
//     return {
//         to: toEmail, // list of receivers
//         subject: 'Placement Approval.', // Subject line
//         text: 'Hello ' + name + 'Your Password has been changed successfully, Please Log in to your account for further operations.',
//         html: 'Hello ' + name + ',<br/>' +
//             'This is to inform that the new Placement ( ' + placementData.placementId + ' ) has been created Successfully by the BD Manager ( ' + bdUser.name + ' ) for the ( ' + placementData.driveType + ' ) on ( ' + placementData.date + ' & ' + placementData.time + ' ) at ( ' + placementData.driveLocation + ' ). <br/><br/>' +
//             'If you have any concerns or queries, Please contact the concerned BD Manager on the same.</p>' +
//             '<p><b>Regards,</b></p>' +
//             '<p><b>Team ABC</b></p>'// html body
//     };
// };


