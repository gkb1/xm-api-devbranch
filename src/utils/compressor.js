
const imagemin = require('imagemin');
const imageminWebp = require('imagemin-webp');

const Compressor = {
    async compressImageInWebP(file) {
        try {
            console.log('BEFORE COMPRESS FILE SIZE: ', file.size)
            const bufferType = (file.mimetype);
            let plugin;
            if (bufferType === 'image/png') {
                plugin = imageminWebp({ quality: 50 });
                // plugin = imageminPngquant({
                //     quality: [0.6, 0.8]
                // });
            } else if (bufferType === 'image/jpeg') {
                // plugin = imageminPngquant({
                //     quality: [0.6, 0.8]
                // })
                plugin = imageminWebp({ quality: 50 });

            }
            const buf = await imagemin.buffer(file.buffer, {
                plugins: [
                    plugin
                ]
            })
            if (file.buffer.length > buf.length) {
                console.log('image compressed');
            }
            file.buffer = buf;
            file.size = buf.length;
            console.log('AFTER COMPRESS FILE SIZE: ', file.size)
            return file;
        } catch (error) {
            return error;
        }
    }
}

module.exports = Compressor;

