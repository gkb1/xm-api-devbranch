var request = require('request');
var nodemailer = require('nodemailer');
var _ = require('lodash');
var config = require('./s3_util/Config');
const customResponse = require('./../utils/custom-response');

var smtpTransport = nodemailer.createTransport({
    // service: 'gmail',
    // port: 465,
    host: 'smtp.gkbpay.com',
    port: 587,
    // secure: true, // use SSL
    secure: false,
    auth: {
        user: config.emailConfig.email,
        pass: config.emailConfig.password
    },
    tls: {
        rejectUnauthorized: false
    }
});
exports.sendSMS = function (mobileNumber, messageString , messageLabel) {
    const options = {
        method: 'GET',
        url: 'https://alerts.solutionsinfini.com/api/v4/?api_key='+config.sms.API_KEY+'&method=sms&message='+messageString+'&to='+mobileNumber+'&sender=GKBHTL',
    };
    request(options, function (error, response, body) {
        if (error) throw customResponse.error(error.status || constant.HTML_STATUS_CODE.INTERNAL_ERROR, error);
        console.log('SMS sent to: ' +mobileNumber + ' regarding ' + messageLabel );
    });
};


exports.sendMail = function (mailOptions) {
    mailOptions.from = '"GKBPAY" <contact@gkbpay.com>';
    smtpTransport.sendMail(mailOptions, function (error, info) {
            if (error) {
                console.log(error);
            } else {
                console.log('Mail sent: ', info);
            }
        });
}