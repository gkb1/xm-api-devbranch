exports.PORT = 9000;
exports.MONGO_URI = 'mongodb://localhost:27017/xm'; // Need to give username and password
exports.baseURI = '/api/';
exports.GKB_PHP_BACKEND_URL = 'http://mobile.gkbhitech.com/persistent/testapi/v1/lib/';
exports.HTML_STATUS_CODE = { SUCCESS: 200, CREATED: 201, UNAUTHORIZED: 401, FORBIDDEN: 403, INVALID_DATA: 406, CONFLICT: 409, INTERNAL_ERROR: 500, TIME_OUT: 504, BAD_REQUEST: 400, NOT_FOUND: 404 };
exports.DOMAIN = 'http://localhost:3000';
exports.TOKEN_TIMEOUT = '30d'; // 30 days
exports.APP_SECRETE = 'qwickbit@xm';
exports.SMS_CREDENTIALS = {
    apiKey: 'jjj',
    senderId: 'jjj',
    base_url: 'https://www.jjj.center/SMSApi/rest/send'
}

exports.lookup = {
    SUPERADMIN_ROLE: 'SUPERADMIN',
    ADMIN_ROLE: 'ADMIN',
    CUSTOMER_ROLE: 'CUSTOMER',
    ECP_CUSTOMER_ROLE: 'ECP_CUSTOMER',   // seller or optician
    FRAME_COLLECTOR_ROLE: 'FRAME_COLLECTOR',
    FRAME_MODERATOR_ROLE: 'FRAME_MODERATOR',
    ROLE: ['SUPERADMIN', 'ADMIN', 'CUSTOMER', 'ECP_CUSTOMER', 'PHOTOGRAPHER', 'FRAME_COLLECTOR', 'FRAME_MODERATOR'],
    PRODUCT_GENDER_LOOKUP: ['Kids', 'Men', 'Women'],
    ADDRESS_TYPE: ['DELIVERY', 'BILLING', 'SHIPPING'],
    MEDIA_TYPE: ['Image', 'Video'],
    SIZE: [
        { code: 1, value: 'Medium' },
        { code: 2, value: 'Large' },
        { code: 3, value: 'Small' }
    ],
    GENDER_LOOKUP: ['Male', 'Female', 'Kids', 'Unisex'],
    // GENDER_LOOKUP: [{ value: 'Male', code: 1 }, { value: 'Female', code: 2 }, { value: 'Kids', code: 3 }, { value: 'Unisex', code: 4 }],
    COLOUR_LOOKUP: ['Black', 'Blue', 'Brown', 'Yellow', 'Green', 'Grey', 'Red', 'White']
};

exports.FILE_TYPE = ['application/pdf', 'image/jpeg', 'image/png', 'application/vnd.openxmlformats-officedocument.wordprocessingml.document', 'application/msword', 'text/csv', 'application/vnd.ms-excel', 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'];
exports.NUMBER_ZERO = 0.00;
exports.GKB_PAY_BACKEND_URL = 'http://gkbpay.com:3000/';