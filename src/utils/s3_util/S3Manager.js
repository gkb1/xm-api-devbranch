const AWS = require('aws-sdk')
const Constants = require('./Config')
const bucketName = "gkbpay"
const path = require('path')
const fs = require('fs')
var shortid = require('shortid');
//let pathParams, image, imageName;

/** Load Config File */
//AWS.config.loadFromPath('config.json')
AWS.config.update(Constants.s3);
/** After config file load, create object for s3*/
const s3 = new AWS.S3({
	// params: {
	// 	Bucket: bucketName
	// },
	region: 'us-east-1'
})


module.exports.generateUrlKey = function (texonomy, filename, callback) {
	// return texonomy + Date.now().toString() + filename;
	return texonomy + Date.now().toString() + shortid.generate() + '.' + filename.split('.').pop();
}

module.exports.uploadS3 = function (params, callback) {
	if (params.Key && params.Body) {
		s3.putObject(params, function (err, data) {
			if (err) {
				console.log("Error uploading image: ", err);
				callback(err, null);
			} else {
				console.log("Successfully uploaded image on S3", data);
				data.url = 'https://'+params.Bucket+'.s3.ap-south-1.amazonaws.com/'+ params.Key;
				callback(null, data);
			}
		});
	} else {
		callback("Invalid Parameter", null);
	}
}

module.exports.uploadToS3Bucket = async function(params) {
	if (params.Key && params.Body) {
		let data = await s3.putObject(params).promise();
		data.url = 'https://'+params.Bucket+'.s3.ap-south-1.amazonaws.com/'+ params.Key;
		return data.url;
	} else {
		return null;
	}
};

module.exports.deleteObject = function(params, callback) {
	s3.deleteObject(params, function (err, data) {
		if (err) {
			console.log("Error while deleting image: ", err.stack);
			callback(err, null);
		} else {
			callback(null, data);
		}
	});
}

module.exports.deleteObjects = function(params, callback) {
	s3.deleteObjects(params, function (err, data) {
		if (err) {
			console.log("Error while deleting image: ", err.stack);
			callback(err, null);
		} else {
			callback(null, data);
		}
	});
}

module.exports.listObjectsV2 = function(params, callback) {
	s3.listObjectsV2(params, function (err, data) {
		if (err) {
			console.log("Error while reading ", err.stack);
			callback(err, null);
		} else {
			callback(null, data);
		}
	});
}

module.exports.getObject = function(params, callback) {
	s3.getObject(params, function (err, data) {
		if (err) {
			console.log("Error while reading ", err.stack);
			callback(err, null);
		} else {
			callback(null, data);
		}
	});
}

module.exports.headObject = function(params, callback) {
	s3.headObject(params, function (err, data) {
		if (err) {
			console.log("Error while reading ", err.stack);
			callback(err, null);
		} else {
			callback(null, data);
		}
	});
}

module.exports.copyObject = function(params, callback) {
	s3.copyObject(params, function (err, data) {
		if (err) {
			console.log("Error while reading ", err.stack);
			callback(err, null);
		} else {
			callback(null, data);
		}
	});
}