const alphanuminc = require('alphanum-increment');
const increment = alphanuminc.increment;

exports.decode = (b64string) => {
    let buf = '';
    if (typeof Buffer.from === 'function') {
        // Node 5.10+
        buf = Buffer.from(b64string, 'base64').toString();
    } else {
        // older Node versions
        buf = new Buffer(b64string, 'base64').toString();
    }
    return buf;
};

exports.encode = (argString) => {
    if (argString == null) {
        return undefined;
    }
    let buf = '';
    if (typeof Buffer.from === 'function') {
        // Node 5.10+
        buf = Buffer.from(argString).toString('base64');
    } else {
        // older Node versions
        buf = new Buffer(argString).toString('base64');
    }
    return buf;
}

// ADDING 3 MINUTES TO EXPIRE OTP
exports.otpExpireTime = () => {
    var timeToExpire = new Date();
    timeToExpire.setMinutes(timeToExpire.getMinutes() + 3);
    return timeToExpire;
}
exports.getRandomNumber = (length) => {
    const number = '0123456789';
    let result = '';
    for (let i = length; i > 0; --i) {
        result += number[Math.floor(Math.random() * number.length)];
    }
    return result;
}

exports.getRandomString = (length) => {
    const chars = 'ABCDEFGHJKMNPQRSTUVWXYZ123456789';
    let result = '';
    for (let i = length; i > 0; --i) {
        result += chars[Math.floor(Math.random() * chars.length)];
    }
    return result;
}

const crypto = require('crypto');

const ENCRYPTION_KEY = 'as12der23uji78uy6tg67uj8ikju90uj' // Must be 256 bytes (32 characters)
const IV_LENGTH = 16; // For AES, this is always 16

exports.encrypt = (text) => {
    let iv = crypto.randomBytes(IV_LENGTH);
    let cipher = crypto.createCipheriv('aes-256-cbc', new Buffer(ENCRYPTION_KEY), iv);
    let encrypted = cipher.update(text);

    encrypted = Buffer.concat([encrypted, cipher.final()]);

    return iv.toString('hex') + '@' + encrypted.toString('hex');
}

exports.decrypt = (text) => {
    let textParts = text.split('@');
    let iv = new Buffer(textParts.shift(), 'hex');
    let encryptedText = new Buffer(textParts.join('@'), 'hex');
    let decipher = crypto.createDecipheriv('aes-256-cbc', new Buffer(ENCRYPTION_KEY), iv);
    let decrypted = decipher.update(encryptedText);

    decrypted = Buffer.concat([decrypted, decipher.final()]);

    return decrypted.toString();
}
exports.checkLock = (date) => {
    let lockTime = new Date(date);
    let currentTime = new Date().toLocaleString("en-US", { timeZone: "Asia/Kolkata" });
    currentTime = new Date(currentTime);
    currentTime.setHours(currentTime.getHours());
    currentTime.setMinutes(currentTime.getMinutes());
    if (currentTime > lockTime) {
        return true;
    } else {
        return false;
    }
}
exports.convert24To12Hour = (time) => {

    let h_24 = time.split(':');
    let type;
    if (parseInt(h_24[0]) < 10) {
        type = 'am';
    } else {
        type = 'pm';
    }
    var h = h_24[0] % 12;
    return ((h < 10 ? '0' : '') + h) + ':' + (h_24[1] < 10 ? '0' + h_24[1] : '' + h_24[1]) + ' ' + type;

}

function isLetter(str) {
    const result = str.length === 1 && str.match(/[a-z]/i);
    return result;
}


exports.getCodes = code => {
    let convertedCode;
    const divideString = code.split('')
    // For 2 digit code
    if (divideString.length === 2) {
        // check if given code is letter or not 
        if (isLetter(divideString[divideString.length - 1]) && divideString[divideString.length - 1] != 'Z') {
            convertedCode = divideString[0] + increment(divideString[divideString.length - 1]); // if its not equal to 'Z' then increment Alphabate (ex: a => b)
            convertedCode = convertedCode.toUpperCase()
        } else if (isLetter(divideString[divideString.length - 1]) &&
            divideString[divideString.length - 1] === 'Z') { // if its equal to 'Z' then increment Number (ex: 1Z => 21)
            convertedCode = increment(divideString[0]) + 1;
        } else if (parseInt(divideString[divideString.length - 1]) &&
            parseInt(divideString[divideString.length - 1]) == 9) { // if its a number and equal to 9 then Assign Alphabate to 'A' (ex: 09 => 0A)
            convertedCode = divideString[0] + 'A';
        } else if (parseInt(divideString[divideString.length - 1]) && parseInt(divideString[divideString.length - 1]) != 9) { // if its a number and not equal to 9 then increment Alphabate (ex: 0A => )
            convertedCode = divideString[0] + increment(divideString[divideString.length - 1]);
        }
    } else if (divideString.length === 3) { // For 3 digit code
        if (isLetter(divideString[divideString.length - 1]) && divideString[divideString.length - 1] != 'Z') {
            convertedCode = (divideString[divideString.length - 3]) + (divideString[divideString.length - 2]) + increment(divideString[divideString.length - 1]);
            convertedCode = convertedCode.toUpperCase()
        } else if (isLetter(divideString[divideString.length - 1]) && divideString[divideString.length - 1] === 'Z') {
            convertedCode = divideString[divideString.length - 3] + increment(divideString[divideString.length - 2]) + 1;
        } else if ((parseInt(divideString[divideString.length - 1]) && parseInt(divideString[divideString.length - 1]) == 9) &&
            (parseInt(divideString[divideString.length - 1]) && parseInt(divideString[divideString.length - 1]) == 9)) {
            convertedCode = divideString[divideString.length - 3] + divideString[divideString.length - 2] + 'A';
        } else if ((parseInt(divideString[divideString.length - 1]) && parseInt(divideString[divideString.length - 1]) != 9) &&
            (parseInt(divideString[divideString.length - 1]) && parseInt(divideString[divideString.length - 1]) != 9)) {
            convertedCode = divideString[divideString.length - 3] + divideString[divideString.length - 2] + increment(divideString[divideString.length - 1]);
        }
    }
    return convertedCode;
}


exports.getActulaPriceCode = price => {
    let actualPrice = Math.round((price) / 1000).toString();
    if (actualPrice.length) {
        switch (actualPrice.length) {
            case 1: actualPrice = '000' + actualPrice; break;
            case 2: actualPrice = '00' + actualPrice; break;
            case 3: actualPrice = '0' + actualPrice; break;
            case 4: actualPrice = actualPrice; break;
            case 5: actualPrice = 'CROR'; break;
        }
    }
    return actualPrice;
}