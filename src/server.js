const express = require('express');
const bodyParser = require('body-parser');
const path = require('path');
const compression = require('compression');
const helmet = require('helmet');
const cors = require('cors');
const app = express();
const multipart = require('connect-multiparty');
// Setting cors
app.use(cors());

app.use(bodyParser.json({ limit: '1000mb', extended: true }));
app.use(multipart({ maxFieldsSize: '1GB'}));
app.use(compression());
app.use(helmet());

//Set static folder
app.use(express.static(path.join(__dirname, 'public')));
app.use(express.static(path.join(__dirname, 'temp')));
app.use('/files', express.static('uploads'));

// app.use('*', (req, res, next) => {
//     res.on('finish', (data) => {
//         console.log(res.statusCode);
//     });
//     next();
// });

// Setting cors
// app.use((req, res, next) => {
//     const origin = req.headers.origin;
//     if (allowedOrigins.indexOf(origin) > -1) {
//         res.setHeader('Access-Control-Allow-Origin', origin);
//     }
//     res.header('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE, OPTIONS');
//     res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept, Authorization, Access-Control-Allow-Credentials');
//     res.header('Access-Control-Allow-Credentials', 'true');
//     next();
// });
// app.use(express.static(path.join(__dirname, '/src/views'), { maxAge: 31557600000 }));


app.use('/users', require('./controller/user-controller'));
app.use('/auth', require('./controller/auth-controller'));
app.use('/product', require('./controller/product-controller'));
app.use('/category', require('./controller/category-controller'));
app.use('/brand', require('./controller/brand-controller'));
app.use('/banner', require('./controller/banner-controller'));
app.use('/file-upload', require('./controller/file-controller'));
app.use('/cart', require('./controller/cart-controller'));
app.use('/wishlist', require('./controller/wishlist-controller'));
app.use('/frame', require('./controller/frameId-controller'));
app.use('/lookup', require('./controller/lookup-controller'));

// app.use('/notification', require('./controller/notification-controller'));
// app.use('/bulk-upload', require('./controller/bulk-upload-controller'));


//Index Route
app.get('/', function (req, res) {
    res.send('invalid rest point');
})

app.get('*', (req, res) => {
    res.sendFile(path.join(__dirname, 'public/index.html'));
})

module.exports = app;