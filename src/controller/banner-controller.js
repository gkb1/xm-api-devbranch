const express = require('express');
const bannerService = require('../service/banner-service');
const route = express.Router();
const constant = require('../utils/constant');
const isAuthenticate = require('../service/token-service');
const multipartMiddleware = require('connect-multiparty')();


route.post('/create', multipartMiddleware, isAuthenticate, (req, res) => {
    bannerService.createBanner(req, req.user).then((result) => {
        res.status(constant.HTML_STATUS_CODE.SUCCESS).json(result);
    }).catch((error) => {
        res.status(error.status || constant.HTML_STATUS_CODE.INTERNAL_ERROR).json(error);
    })
});

route.get('/:bannerId', isAuthenticate, (req, res) => {
    bannerService.getBannerById(req.user, req.params.bannerId).then((result) => {
        res.status(constant.HTML_STATUS_CODE.SUCCESS).json(result);
    }).catch((error) => {
        res.status(error.status || constant.HTML_STATUS_CODE.INTERNAL_ERROR).json(error);
    })
});

route.post('/getAllBannersByCond', (req, res) => {
    bannerService.listAllBanners(req.body).then((result) => {
        res.status(constant.HTML_STATUS_CODE.SUCCESS).json(result);
    }).catch((error) => {
        res.status(error.status || constant.HTML_STATUS_CODE.INTERNAL_ERROR).json(error);
    })
});

route.put('/updateBanner/:bannerId', isAuthenticate, (req, res) => {
    bannerService.updateBanner(req.params.bannerId, req.body).then((result) => {
        if (result.n > 0 || result.nModified > 0) {
            res.status(constant.HTML_STATUS_CODE.SUCCESS).json({ message: 'Banner updated successfully!' });
        }
    }).catch((error) => {
        res.status(error.status || constant.HTML_STATUS_CODE.INTERNAL_ERROR).json(error);
    })
});

route.delete('/deleteBanner/:bannerId', isAuthenticate, (req, res) => {
    bannerService.deleteBanner(req.params.bannerId, req.user).then((result) => {
        if (result.n > 0 || result.nModified > 0) {
            res.status(constant.HTML_STATUS_CODE.SUCCESS).json({ message: 'Banner deleted successfully!' });
        }
    }).catch((error) => {
        res.status(error.status || constant.HTML_STATUS_CODE.INTERNAL_ERROR).json(error);
    })
});

module.exports = route;