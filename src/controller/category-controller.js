const express = require('express');
const constant = require('./../utils/constant');
const categoryService = require('./../service/category-service');
const isAuthenticate = require('./../service/token-service');

const route = express.Router();


// create category
route.post('/create', isAuthenticate, (req, res) => {
    categoryService.createCategory(req.body, req.user).then((result) => {
        res.status(constant.HTML_STATUS_CODE.SUCCESS).json(result);
    }).catch((error) => {
        res.status(error.status || constant.HTML_STATUS_CODE.INTERNAL_ERROR).json(error);
    });
});

// create child category with/without subCategory
route.post('/createChildCat',isAuthenticate, (req, res) => {
    categoryService.createChildCat(req.body, req.user).then((result) => {
        res.status(constant.HTML_STATUS_CODE.SUCCESS).json(result);
    }).catch((error) => {
        res.status(error.status || constant.HTML_STATUS_CODE.INTERNAL_ERROR).json(error);
    });
});

// get all category
route.post('/getAllCategories', (req, res) => {
    categoryService.getAllCategories(req.body).then((result) => {
        res.status(constant.HTML_STATUS_CODE.SUCCESS).json(result);
    }).catch((error) => {
        res.status(error.status || constant.HTML_STATUS_CODE.INTERNAL_ERROR).json(error);
    });
});

// get all category with trending for home
route.post('/getAllCategoriesWithTrendingCat', (req, res) => {
    categoryService.getAllCategoriesWithTrendingCat(req.body).then((result) => {
        res.status(constant.HTML_STATUS_CODE.SUCCESS).json(result);
    }).catch((error) => {
        res.status(error.status || constant.HTML_STATUS_CODE.INTERNAL_ERROR).json(error);
    });
});

// get by category ID
route.get('/getByCategoryId/:categoryId', (req, res) => {
    categoryService.getByCategoryId(req.params.categoryId).then((result) => {
        res.status(constant.HTML_STATUS_CODE.SUCCESS).json(result);
    }).catch((error) => {
        res.status(error.status || constant.HTML_STATUS_CODE.INTERNAL_ERROR).json(error);
    });
});

// update Sel category
route.put('/updateCategory/:categoryId', isAuthenticate, (req, res) => {
    categoryService.updateCategory(req.params.categoryId, req.body, req.user).then((result) => {
        res.status(constant.HTML_STATUS_CODE.SUCCESS).json(result);
    }).catch((error) => {
        res.status(error.status || constant.HTML_STATUS_CODE.INTERNAL_ERROR).json(error);
    });
});


// get by sel category details with child/sub cat
// route.post('/getSelCategoryByCond', (req, res) => {
//     categoryService.getSelCategoryByCond(req.body).then((result) => {
//         res.status(constant.HTML_STATUS_CODE.CREATED).json(result);
//     }).catch((error) => {
//         res.status(error.status || constant.HTML_STATUS_CODE.INTERNAL_ERROR).json(error);
//     });
// });
module.exports = route;
