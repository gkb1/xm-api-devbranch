const express = require('express');
const cartService = require('../service/cart-service');
const route = express.Router();
const constant = require('../utils/constant');
const isAuthenticate = require('../service/token-service');


route.post('/create', isAuthenticate, (req, res) => {
    cartService.createCart(req, req.user).then((result) => {
        res.status(constant.HTML_STATUS_CODE.SUCCESS).json(result);
    }).catch((error) => {
        res.status(error.status || constant.HTML_STATUS_CODE.INTERNAL_ERROR).json(error);
    })
});

route.get('/:cartId', isAuthenticate, (req, res) => {
    cartService.getCartItemById(req.user, req.params.cartId).then((result) => {
        res.status(constant.HTML_STATUS_CODE.SUCCESS).json(result);
    }).catch((error) => {
        res.status(error.status || constant.HTML_STATUS_CODE.INTERNAL_ERROR).json(error);
    })
});

route.post('/getAllCartItemsByCond', (req, res) => {
    cartService.listAllCartItems(req.body).then((result) => {
        res.status(constant.HTML_STATUS_CODE.SUCCESS).json(result);
    }).catch((error) => {
        res.status(error.status || constant.HTML_STATUS_CODE.INTERNAL_ERROR).json(error);
    })
});

route.put('/updateCart/:cartId/:productId', isAuthenticate, (req, res) => {
    cartService.updateCart(req.params, req.body).then((result) => {
        if (result.n > 0 || result.nModified > 0) {
            res.status(constant.HTML_STATUS_CODE.SUCCESS).json({ message: 'Cart updated successfully!' });
        } else {
            res.status(constant.HTML_STATUS_CODE.TIME_OUT).json({message: 'Updation failed try again!'})
        }
    }).catch((error) => {
        res.status(error.status || constant.HTML_STATUS_CODE.INTERNAL_ERROR).json(error);
    })
});

route.delete('/deleteCart/:cartId/:productId', isAuthenticate, (req, res) => {
    cartService.deleteCart(req.params, req.user).then((result) => {
        if (result.n > 0 || result.nModified > 0) {
            res.status(constant.HTML_STATUS_CODE.SUCCESS).json({ message: 'Cart deleted successfully!' });
        }
    }).catch((error) => {
        res.status(error.status || constant.HTML_STATUS_CODE.INTERNAL_ERROR).json(error);
    })
});

module.exports = route;