const express = require('express');
const constant = require('./../utils/constant');
const brandService = require('./../service/brand-service');
const isAuthenticate = require('./../service/token-service');

const route = express.Router();


// create brand
route.post('/create', isAuthenticate, (req, res) => {
    brandService.createBrand(req.body, req.user).then((result) => {
        res.status(constant.HTML_STATUS_CODE.CREATED).json(result);
    }).catch((error) => {
        res.status(error.status || constant.HTML_STATUS_CODE.INTERNAL_ERROR).json(error);
    });
});

// get brand by condition name and id
route.get('/getBrandByNameAndId', isAuthenticate, (req, res) => {
    brandService.getBrandByNameAndId(req.query).then((result) => {
        res.status(constant.HTML_STATUS_CODE.CREATED).json(result);
    }).catch((error) => {
        res.status(error.status || constant.HTML_STATUS_CODE.INTERNAL_ERROR).json(error);
    });
});

// get all brands by condition
route.post('/getAllBrandsByCond', isAuthenticate, (req, res) => {
    brandService.getAllBrandsByCond(req.body).then((result) => {
        res.status(constant.HTML_STATUS_CODE.CREATED).json(result);
    }).catch((error) => {
        res.status(error.status || constant.HTML_STATUS_CODE.INTERNAL_ERROR).json(error);
    });
});

// update brand by condition name and id
route.put('/updateBrand', isAuthenticate, (req, res) => {
    brandService.updateBrandByNameAndId(req.query, req.body).then((result) => {
        if (result.n > 0 || result.nModified > 0) {
            res.status(constant.HTML_STATUS_CODE.CREATED).json({ message: 'Brand updated successfully!' });
        }
    }).catch((error) => {
        res.status(error.status || constant.HTML_STATUS_CODE.INTERNAL_ERROR).json(error);
    });
});

route.delete('/deleteBrand', isAuthenticate, (req, res) => {
    brandService.deleteBrandByNameAndId(req.query, req.user).then((result) => {
        if (result.n > 0 || result.nModified > 0) {
            res.status(constant.HTML_STATUS_CODE.CREATED).json({ message: 'Brand deleted successfully.' });
        }
    }).catch((error) => {
        res.status(error.status || constant.HTML_STATUS_CODE.INTERNAL_ERROR).json(error);
    });
});

module.exports = route;
