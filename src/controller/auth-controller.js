const express = require('express');
const authService = require('../service/auth-service');
const route = express.Router();
const constant = require('../utils/constant');

// LOGIN 
route.post('/login', (req, res) => {
    authService.signIn(req.body).then((result) => {
        res.status(constant.HTML_STATUS_CODE.SUCCESS).json({ message: "Login successfull", Data: result });
    }).catch((error) => {
        res.status(error.status || constant.HTML_STATUS_CODE.INTERNAL_ERROR).json(error);
    })
});

// GENERATE OTP 
route.post('/generateOTP', (req, res) => {
    authService.generateOTP(req.body).then((result) => {
        res.status(constant.HTML_STATUS_CODE.SUCCESS).json(result);
    }).catch((error) => {
        res.status(error.status || constant.HTML_STATUS_CODE.INTERNAL_ERROR).json(error);
    })
});

module.exports = route;