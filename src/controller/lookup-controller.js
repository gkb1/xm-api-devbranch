const express = require('express');
const lookUpService = require('../service/lookup-service');
const route = express.Router();
const constant = require('../utils/constant');
const isAuthenticate = require('../service/token-service');
const multer = require('multer')
const upload = multer();
const multipartMiddleware = require('connect-multiparty')();

//FRAME SIZE
route.get('/frameSizes', isAuthenticate, (req, res) => {
    lookUpService.getFrameSizes().then((result) => {
        res.status(constant.HTML_STATUS_CODE.SUCCESS).json(result);
    }).catch((error) => {
        res.status(error.status || constant.HTML_STATUS_CODE.INTERNAL_ERROR).json(error);
    })
});

//FRAME PARENT COLOUR
route.get('/getAllParentColour', isAuthenticate, (req, res) => {
    lookUpService.getFrameParentColour().then((result) => {
        res.status(constant.HTML_STATUS_CODE.SUCCESS).json(result);
    }).catch((error) => {
        res.status(error.status || constant.HTML_STATUS_CODE.INTERNAL_ERROR).json(error);
    })
});

// FRAME MATERIAL

route.post('/frameMaterial/create', multipartMiddleware, isAuthenticate, (req, res) => {
    lookUpService.createFrameMaterial(req, req.user).then((result) => {
        res.status(constant.HTML_STATUS_CODE.SUCCESS).json(result);
    }).catch((error) => {
        let message;
        if (error.message.indexOf('E11000') > -1) {
            if (error.message.indexOf('name') > -1) {
                message = 'Frame material ' + req.body.name + ' already exists !!'
            }
            res.status(constant.HTML_STATUS_CODE.CONFLICT).json({ status: constant.HTML_STATUS_CODE.CONFLICT, message: message });
        } else {
            res.status(error.status || constant.HTML_STATUS_CODE.INTERNAL_ERROR).json(error);
        }
    })
});

route.get('/frameMaterial/:frameMatId', isAuthenticate, (req, res) => {
    lookUpService.getFrameMaterialById(req.params, req.user).then((result) => {
        res.status(constant.HTML_STATUS_CODE.SUCCESS).json(result);
    }).catch((error) => {
        res.status(error.status || constant.HTML_STATUS_CODE.INTERNAL_ERROR).json(error);
    })
});

route.post('/frameMaterial', isAuthenticate, (req, res) => {
    lookUpService.getAllFrameMaterials(req.body, req.user).then((result) => {
        res.status(constant.HTML_STATUS_CODE.SUCCESS).json(result);
    }).catch((error) => {
        res.status(error.status || constant.HTML_STATUS_CODE.INTERNAL_ERROR).json(error);
    })
});

route.put('/frameMaterial/:frameMatId', isAuthenticate, (req, res) => {
    lookUpService.updateFrameMaterial(req.params, req.body).then((result) => {
        if (result.n > 0 || result.nModified > 0) {
            res.status(constant.HTML_STATUS_CODE.SUCCESS).json({ message: 'Frame material updated successfully' });
        }
    }).catch((error) => {
        res.status(error.status || constant.HTML_STATUS_CODE.INTERNAL_ERROR).json(error);
    })
});

route.delete('/frameMaterial/:frameMatId', isAuthenticate, (req, res) => {
    lookUpService.deleteFrameMaterial(req.params, req.user).then((result) => {
        if (result.n > 0 || result.nModified > 0) {
            res.status(constant.HTML_STATUS_CODE.SUCCESS).json({ message: 'Frame material deleted successfully' });
        }
    }).catch((error) => {
        res.status(error.status || constant.HTML_STATUS_CODE.INTERNAL_ERROR).json(error);
    })
});


// FRAME SHAPE

route.post('/frameShape/create', multipartMiddleware, isAuthenticate, (req, res) => {
    lookUpService.createFrameShape(req, req.user).then((result) => {
        res.status(constant.HTML_STATUS_CODE.SUCCESS).json(result);
    }).catch((error) => {
        let message;
        if (error.message.indexOf('E11000') > -1) {
            if (error.message.indexOf('name') > -1) {
                message = 'Frame shape ' + req.body.name + ' already exists !!'
            }
            res.status(constant.HTML_STATUS_CODE.CONFLICT).json({ status: constant.HTML_STATUS_CODE.CONFLICT, message: message });
        } else {
            res.status(error.status || constant.HTML_STATUS_CODE.INTERNAL_ERROR).json(error);
        }
    })
});

route.get('/frameShape/:frameShapeId', isAuthenticate, (req, res) => {
    lookUpService.getFrameShapeById(req.params, req.user).then((result) => {
        res.status(constant.HTML_STATUS_CODE.SUCCESS).json(result);
    }).catch((error) => {
        res.status(error.status || constant.HTML_STATUS_CODE.INTERNAL_ERROR).json(error);
    })
});

route.post('/frameShape', isAuthenticate, (req, res) => {
    lookUpService.getAllFrameShapes(req.body, req.user).then((result) => {
        res.status(constant.HTML_STATUS_CODE.SUCCESS).json(result);
    }).catch((error) => {
        res.status(error.status || constant.HTML_STATUS_CODE.INTERNAL_ERROR).json(error);
    })
});

route.put('/frameShape/:frameShapeId', isAuthenticate, (req, res) => {
    lookUpService.updateFrameShape(req.params, req.body).then((result) => {
        if (result.n > 0 || result.nModified > 0) {
            res.status(constant.HTML_STATUS_CODE.SUCCESS).json({ message: 'Frame shape updated successfully' });
        }
    }).catch((error) => {
        res.status(error.status || constant.HTML_STATUS_CODE.INTERNAL_ERROR).json(error);
    })
});

route.delete('/frameShape/:frameShapeId', isAuthenticate, (req, res) => {
    lookUpService.deleteFrameShape(req.params, req.user).then((result) => {
        if (result.n > 0 || result.nModified > 0) {
            res.status(constant.HTML_STATUS_CODE.SUCCESS).json({ message: 'Frame shape deleted successfully' });
        }
    }).catch((error) => {
        res.status(error.status || constant.HTML_STATUS_CODE.INTERNAL_ERROR).json(error);
    })
});


// FRAME TYPE

route.post('/frameType/create', multipartMiddleware, isAuthenticate, (req, res) => {
    lookUpService.createFrameType(req, req.user).then((result) => {
        res.status(constant.HTML_STATUS_CODE.SUCCESS).json(result);
    }).catch((error) => {
        let message;
        if (error.message.indexOf('E11000') > -1) {
            if (error.message.indexOf('name') > -1) {
                message = 'Frame type ' + req.body.name + ' already exists !!'
            }
            res.status(constant.HTML_STATUS_CODE.CONFLICT).json({ status: constant.HTML_STATUS_CODE.CONFLICT, message: message });
        } else {
            res.status(error.status || constant.HTML_STATUS_CODE.INTERNAL_ERROR).json(error);
        }
    })
});

route.get('/frameType/:frameTypeId', isAuthenticate, (req, res) => {
    lookUpService.getFrameTypeById(req.params, req.user).then((result) => {
        res.status(constant.HTML_STATUS_CODE.SUCCESS).json(result);
    }).catch((error) => {
        res.status(error.status || constant.HTML_STATUS_CODE.INTERNAL_ERROR).json(error);
    })
});

route.post('/frameType', isAuthenticate, (req, res) => {
    lookUpService.getAllFrameTypes(req.body, req.user).then((result) => {
        res.status(constant.HTML_STATUS_CODE.SUCCESS).json(result);
    }).catch((error) => {
        res.status(error.status || constant.HTML_STATUS_CODE.INTERNAL_ERROR).json(error);
    })
});

route.put('/frameType/:frameTypeId', isAuthenticate, (req, res) => {
    lookUpService.updateFrameType(req.params, req.body).then((result) => {
        if (result.n > 0 || result.nModified > 0) {
            res.status(constant.HTML_STATUS_CODE.SUCCESS).json({ message: 'Frame type updated successfully' });
        }
    }).catch((error) => {
        res.status(error.status || constant.HTML_STATUS_CODE.INTERNAL_ERROR).json(error);
    })
});

route.delete('/frameType/:frameTypeId', isAuthenticate, (req, res) => {
    lookUpService.deleteFrameType(req.params, req.user).then((result) => {
        if (result.n > 0 || result.nModified > 0) {
            res.status(constant.HTML_STATUS_CODE.SUCCESS).json({ message: 'Frame type deleted successfully' });
        }
    }).catch((error) => {
        res.status(error.status || constant.HTML_STATUS_CODE.INTERNAL_ERROR).json(error);
    })
});

// FRAME COLOUR

route.post('/frameColour/create', isAuthenticate, (req, res) => {
    lookUpService.createFrameColour(req.body, req.user).then((result) => {
        res.status(constant.HTML_STATUS_CODE.SUCCESS).json(result);
    }).catch((error) => {
        let message;
        if (error.message.indexOf('E11000') > -1) {
            if (error.message.indexOf('name') > -1) {
                message = 'Frame colour ' + req.body.name + ' already exists !!'
            }
            res.status(constant.HTML_STATUS_CODE.CONFLICT).json({ status: constant.HTML_STATUS_CODE.CONFLICT, message: message });
        } else {
            res.status(error.status || constant.HTML_STATUS_CODE.INTERNAL_ERROR).json(error);
        }
    })
});

route.get('/frameColour/:frameColourId', isAuthenticate, (req, res) => {
    lookUpService.getFrameColourById(req.params, req.user).then((result) => {
        res.status(constant.HTML_STATUS_CODE.SUCCESS).json(result);
    }).catch((error) => {
        res.status(error.status || constant.HTML_STATUS_CODE.INTERNAL_ERROR).json(error);
    })
});

route.post('/frameColour', isAuthenticate, (req, res) => {
    lookUpService.getAllFrameColour(req.body, req.user).then((result) => {
        res.status(constant.HTML_STATUS_CODE.SUCCESS).json(result);
    }).catch((error) => {
        res.status(error.status || constant.HTML_STATUS_CODE.INTERNAL_ERROR).json(error);
    })
});

// FRAME MODEL

route.post('/frameModel/create', isAuthenticate, (req, res) => {
    lookUpService.creteFrameModel(req.body, req.user).then((result) => {
        res.status(constant.HTML_STATUS_CODE.SUCCESS).json(result);
    }).catch((error) => {
        let message;
        if (error.message.indexOf('E11000') > -1) {
            if (error.message.indexOf('name') > -1) {
                message = 'Frame model ' + req.body.name + ' already exists !!'
            }
            res.status(constant.HTML_STATUS_CODE.CONFLICT).json({ status: constant.HTML_STATUS_CODE.CONFLICT, message: message });
        } else {
            res.status(error.status || constant.HTML_STATUS_CODE.INTERNAL_ERROR).json(error);
        }
    })
});

route.get('/frameModel/:frameModelId', isAuthenticate, (req, res) => {
    lookUpService.getFrameModelById(req.params, req.user).then((result) => {
        res.status(constant.HTML_STATUS_CODE.SUCCESS).json(result);
    }).catch((error) => {
        res.status(error.status || constant.HTML_STATUS_CODE.INTERNAL_ERROR).json(error);
    })
});

route.post('/frameModel', isAuthenticate, (req, res) => {
    lookUpService.getAllFrameModel(req.body, req.user).then((result) => {
        res.status(constant.HTML_STATUS_CODE.SUCCESS).json(result);
    }).catch((error) => {
        res.status(error.status || constant.HTML_STATUS_CODE.INTERNAL_ERROR).json(error);
    })
});


module.exports = route;