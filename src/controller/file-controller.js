const express = require('express');
const constant = require('./../utils/constant');
const fileService = require('./../service/file-service');
const route = express.Router();
const multipart = require('connect-multiparty');
const multipartMiddleware = multipart();
const isAuthenticate = require('./../service/token-service');

const multer = require('multer')
const upload = multer()

route.post('/s3-upload', isAuthenticate, multipartMiddleware, async (req, res) => {
    try {
        let imageUrl = await fileService.uploadToS3(req.files)
        if (imageUrl) {
            res.status(constant.HTML_STATUS_CODE.CREATED).json(imageUrl);
        } else {
            console.log(error);
            res.status(error.status || constant.HTML_STATUS_CODE.INTERNAL_ERROR).json(error);
        }
    } catch (exception) {
        console.log(exception);
        res.status(exception.status || constant.HTML_STATUS_CODE.INTERNAL_ERROR).json(exception);
    }

});

route.post('/uploadKYCDocs', isAuthenticate, multipartMiddleware, async (req, res) => {
    try {
        let imageUrl = await fileService.uploadKYCDocs(req);
        if (imageUrl) {
            res.status(constant.HTML_STATUS_CODE.CREATED).json({ url: imageUrl.url });
        } else {
            res.status(error.status || constant.HTML_STATUS_CODE.INTERNAL_ERROR).json(error);
        }
    } catch (exception) {
        res.status(exception.status || constant.HTML_STATUS_CODE.INTERNAL_ERROR).json(exception);
    }

});

route.post('/upload', upload.single('media'), async (req, res) => {
    fileService.compressFile(req.file).then((data) => {
        return res.status(constant.HTML_STATUS_CODE.CREATED).json(data);
    }).catch((errorObj) => {
        return res.status(constant.HTML_STATUS_CODE.INTERNAL_ERROR).json(errorObj);
    });
});


route.post('/bucket/readS3Folder', isAuthenticate, async (req, res) =>  {
	fileService.readS3Folder(req.user, req.body).then((data) => {
        return res.status(constant.HTML_STATUS_CODE.CREATED).json(data);
    }).catch((errorObj) => {
        return res.status(constant.HTML_STATUS_CODE.INTERNAL_ERROR).json(errorObj);
    });
});

module.exports = route;