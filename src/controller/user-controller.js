const express = require('express');
const constant = require('./../utils/constant');
const userService = require('./../service/user-service');
const isAuthenticate = require('./../service/token-service');

const route = express.Router();


// Registration User
route.post('/signup', (req, res) => {
    userService.createUser(req.body).then((result) => {
        res.status(constant.HTML_STATUS_CODE.CREATED).json(result);
    }).catch((error) => {
        res.status(error.status || constant.HTML_STATUS_CODE.INTERNAL_ERROR).json(error);
    });
});

// Generate & send Otp by mobile
route.post('/generateAndSendOTP', (req, res) => {
    userService.generateAndSendOTP(req.body).then((result) => {
        res.status(constant.HTML_STATUS_CODE.SUCCESS).json(result);
    }).catch((error) => {
        res.status(error.status || constant.HTML_STATUS_CODE.INTERNAL_ERROR).json(error);
    });
});

// Verify User Otp by mob no & login
route.get('/verifyOtp/:mobile/:otpEntered', (req, res) => {
    userService.verifyOtp(req.params).then((result) => {
        res.status(constant.HTML_STATUS_CODE.SUCCESS).json(result);
    }).catch((error) => {
        res.status(error.status || constant.HTML_STATUS_CODE.INTERNAL_ERROR).json(error);
    });
});

// Get All User
route.post('/getList', isAuthenticate, (req, res) => {
    userService.getAll(req.body, req.user).then((result) => {
        res.status(constant.HTML_STATUS_CODE.SUCCESS).json(result);
    }).catch((error) => {
        res.status(error.status || constant.HTML_STATUS_CODE.INTERNAL_ERROR).json(error);
    });
});

// Get Sel User
route.post('/getSelUserByCond', isAuthenticate, (req, res) => {
    userService.getSelUserByCond(req.body, req.user).then((result) => {
        res.status(constant.HTML_STATUS_CODE.SUCCESS).json(result);
    }).catch((error) => {
        res.status(error.status || constant.HTML_STATUS_CODE.INTERNAL_ERROR).json(error);
    });
});

// Get By Id
route.get('/getById', isAuthenticate, (req, res) => {
    userService.getById(req.user).then((result) => {
        res.status(constant.HTML_STATUS_CODE.SUCCESS).json(result);
    }).catch((error) => {
        res.status(error.status || constant.HTML_STATUS_CODE.INTERNAL_ERROR).json(error);
    });
});

// update user by userId
route.put('/updateUser', isAuthenticate, (req, res) => {
    userService.updateUser(req.body, req.user).then((result) => {
        res.status(constant.HTML_STATUS_CODE.SUCCESS).json(result);
    }).catch((error) => {
        res.status(error.status || constant.HTML_STATUS_CODE.INTERNAL_ERROR).json(error);
    });
});


// update user Status activate/de-activate
route.put('/activateUser/:userId', isAuthenticate, (req, res) => {
    userService.activateUser(req.body, req.user, req.params.userId).then((result) => {
        res.status(constant.HTML_STATUS_CODE.SUCCESS).json(result);
    }).catch((error) => {
        res.status(error.status || constant.HTML_STATUS_CODE.INTERNAL_ERROR).json(error);
    });
});

// Get All User Notifications
route.post('/getUserNotificationList', isAuthenticate, (req, res) => {
    userService.getUserNotificationList(req.body, req.user).then((result) => {
        res.status(constant.HTML_STATUS_CODE.SUCCESS).json(result);
    }).catch((error) => {
        res.status(error.status || constant.HTML_STATUS_CODE.INTERNAL_ERROR).json(error);
    });
});

// Assign Store to Frame_collector
route.get('/stores', isAuthenticate, (req, res) => {
    userService.getAllStores(req.user).then((result) => {
        res.status(constant.HTML_STATUS_CODE.SUCCESS).json(result);
    }).catch((error) => {
        res.status(error.status || constant.HTML_STATUS_CODE.INTERNAL_ERROR).json(error);
    });
});

// Assign Store to Frame_collector
route.put('/assignStore', isAuthenticate, (req, res) => {
    userService.AssignStoreToFrameCollector(req.body, req.user).then((result) => {
        if (result.n > 0 || result.nModified > 0) {
            res.status(constant.HTML_STATUS_CODE.SUCCESS).json({ message: "Store assigned successfully!" });
        }
    }).catch((error) => {
        res.status(error.status || constant.HTML_STATUS_CODE.INTERNAL_ERROR).json(error);
    });
});

// Delete Store to Frame_collector
route.delete('/deleteStore/:cust_code/:frameCollectorUserId', isAuthenticate, (req, res) => {
    userService.deleteStoreFromframeCollector(req.params, req.user).then((result) => {
        if (result.n > 0 || result.nModified > 0) {
            res.status(constant.HTML_STATUS_CODE.SUCCESS).json({ message: "Store removed successfully!" });
        }
    }).catch((error) => {
        res.status(error.status || constant.HTML_STATUS_CODE.INTERNAL_ERROR).json(error);
    });
});

module.exports = route;