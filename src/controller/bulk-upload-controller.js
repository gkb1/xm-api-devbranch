const express = require('express');
const constant = require('./../utils/constant');
const bulkUpload = require('./../service/bulk-upload');
const isAuthenticate = require('./../service/token-service');
const route = express.Router();


// Registration Batch
route.post('/', (req, res) => {
    bulkUpload.uploadStudent(req.body).then((result) => {
        res.status(constant.HTML_STATUS_CODE.CREATED).json(result);
    }).catch((error) => {
        res.status(error.status || constant.HTML_STATUS_CODE.INTERNAL_ERROR).json(error);
    });
});

module.exports = route;