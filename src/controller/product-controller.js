const express = require('express');
const productService = require('../service/product-service');
const route = express.Router();
const constant = require('../utils/constant');
const isAuthenticate = require('./../service/token-service');
const multipartMiddleware = require('connect-multiparty')();


route.post('/add', multipartMiddleware, isAuthenticate, (req, res) => {
    productService.addProduct(req, req.user).then((result) => {
        res.status(constant.HTML_STATUS_CODE.SUCCESS).json(result);
    }).catch((error) => {
        let message;
        if (error.message.indexOf('E11000') > -1) {
            if (error.message.indexOf('frameId') > -1) {
                message = 'FRAME ID already exists !!'
            }
        res.status(constant.HTML_STATUS_CODE.CONFLICT).json({status: constant.HTML_STATUS_CODE.CONFLICT, message: message});
        } else {
            res.status(error.status || constant.HTML_STATUS_CODE.INTERNAL_ERROR).json(error);            
        }
    })
});
route.get('/trending', isAuthenticate, (req, res) => {
    productService.getTrendingProduct(req.user).then((result) => {
        res.status(constant.HTML_STATUS_CODE.SUCCESS).json({ message: "Trending Products!", data: result });
    }).catch((error) => {
        res.status(error.status || constant.HTML_STATUS_CODE.INTERNAL_ERROR).json(error);
    })
});

route.get('/getByPrdtId/:prodId', (req, res) => {
    productService.getProductById(req.params.prodId).then((result) => {
        res.status(constant.HTML_STATUS_CODE.SUCCESS).json({ message: "Product fetched successfully!", data: result });
    }).catch((error) => {
        res.status(error.status || constant.HTML_STATUS_CODE.INTERNAL_ERROR).json(error);
    })
});

route.get('/getByPrdtframeId/:frameId', (req, res) => {
    productService.getProductByFrameId(req.params).then((result) => {
        res.status(constant.HTML_STATUS_CODE.SUCCESS).json({ message: "Product fetched successfully!", data: result });
    }).catch((error) => {
        res.status(error.status || constant.HTML_STATUS_CODE.INTERNAL_ERROR).json(error);
    })
});

route.post('/getAllByCond', (req, res) => {
    productService.listAllProducts(req.body).then((result) => {
        res.status(constant.HTML_STATUS_CODE.SUCCESS).json(result);
    }).catch((error) => {
        res.status(error.status || constant.HTML_STATUS_CODE.INTERNAL_ERROR).json(error);
    })
});

route.post('/getAllPrdAttributes', (req, res) => {
    productService.listAllProductsAttributer(req.body).then((result) => {
        res.status(constant.HTML_STATUS_CODE.SUCCESS).json(result);
    }).catch((error) => {
        res.status(error.status || constant.HTML_STATUS_CODE.INTERNAL_ERROR).json(error);
    })
});


route.put('/update/:prodId', isAuthenticate, (req, res) => {
    productService.updateProduct(req.params.prodId, req.body).then((result) => {
        if (result.n > 0 || result.nModified > 0) {
            res.status(constant.HTML_STATUS_CODE.SUCCESS).json({ message: "Product updated successfully!" });
        }
    }).catch((error) => {
        res.status(error.status || constant.HTML_STATUS_CODE.INTERNAL_ERROR).json(error);
    })
});

route.put('/updateGKBPrice/:prodId', isAuthenticate, (req, res) => {
    productService.updateGKBPrice(req.params.prodId, req.body, req.user).then((result) => {
        if (result.n > 0 || result.nModified > 0) {
            res.status(constant.HTML_STATUS_CODE.SUCCESS).json({ message: "Product Price Updated successfully!" });
        }
    }).catch((error) => {
        res.status(error.status || constant.HTML_STATUS_CODE.INTERNAL_ERROR).json(error);
    })
});

route.post('/updateProductAttribute', isAuthenticate, (req, res) => {
    productService.updateProductAttribute(req.user, req.body).then((result) => {
        res.status(constant.HTML_STATUS_CODE.SUCCESS).json({ message: "Product updated successfully!" });
    }).catch((error) => {
        res.status(error.status || constant.HTML_STATUS_CODE.INTERNAL_ERROR).json(error);
    })
});

route.post('/removeStore', isAuthenticate, (req, res) => {
    productService.removeStore(req.user, req.body).then((result) => {
        res.status(constant.HTML_STATUS_CODE.SUCCESS).json({ message: "Store removed successfully!" });
    }).catch((error) => {
        res.status(error.status || constant.HTML_STATUS_CODE.INTERNAL_ERROR).json(error);
    })
});

route.delete('/delete/:prodId', isAuthenticate, (req, res) => {
    productService.deleteProduct(req.params.prodId, req.user).then((result) => {
        if (result.n > 0 || result.nModified > 0) {
            res.status(constant.HTML_STATUS_CODE.SUCCESS).json({ message: "Product deleted successfully!" });
        }
    }).catch((error) => {
        res.status(error.status || constant.HTML_STATUS_CODE.INTERNAL_ERROR).json(error);
    })
});



// product approval from admin
route.put('/approvOrReject/:prodId', isAuthenticate, (req, res) => {
    productService.approvOrReject(req.params.prodId, req.body, req.user).then((result) => {
        res.status(constant.HTML_STATUS_CODE.SUCCESS).json(result);
    }).catch((error) => {
        res.status(error.status || constant.HTML_STATUS_CODE.INTERNAL_ERROR).json(error);
    })
});

route.get('/searchByTagNo/:tagNo',  isAuthenticate, (req, res) => {
    productService.searchByTagNo(req.user, req.params).then((result) => {
        res.status(constant.HTML_STATUS_CODE.SUCCESS).json({ message: "Product fetched successfully!", data: result });
    }).catch((error) => {
        res.status(error.status || constant.HTML_STATUS_CODE.INTERNAL_ERROR).json(error);
    })
});

route.get('/generateSKU/:productId/:prdAttributeId/:storeId',  isAuthenticate, (req, res) => {
    productService.generateSKU(req.params).then((result) => {
        if(result.n >0 || result.nModified > 0) {
            res.status(constant.HTML_STATUS_CODE.SUCCESS).json({ message: "SKU generated successfully!" });
        }
    }).catch((error) => {
        res.status(error.status || constant.HTML_STATUS_CODE.INTERNAL_ERROR).json(error);
    })
});

route.post('/getAllTagNamesToVerifySKU', (req, res) => {
    productService.getAllTagNamesToVerifySKU(req.body).then((result) => {
        res.status(constant.HTML_STATUS_CODE.SUCCESS).json(result);
    }).catch((error) => {
        res.status(error.status || constant.HTML_STATUS_CODE.INTERNAL_ERROR).json(error);
    })
});



module.exports = route;