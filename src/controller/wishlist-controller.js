const express = require('express');
const wishlistService = require('../service/wishlist-service');
const route = express.Router();
const constant = require('../utils/constant');
const isAuthenticate = require('../service/token-service');


route.post('/create', isAuthenticate, (req, res) => {
    wishlistService.createWishlist(req, req.user).then((result) => {
        res.status(constant.HTML_STATUS_CODE.SUCCESS).json(result);
    }).catch((error) => {
        res.status(error.status || constant.HTML_STATUS_CODE.INTERNAL_ERROR).json(error);
    })
});

route.get('/:wishlistId', isAuthenticate, (req, res) => {
    wishlistService.getWishlistItemById(req.user, req.params.wishlistId).then((result) => {
        res.status(constant.HTML_STATUS_CODE.SUCCESS).json(result);
    }).catch((error) => {
        res.status(error.status || constant.HTML_STATUS_CODE.INTERNAL_ERROR).json(error);
    })
});

route.post('/getAllWishlistItemsByCond', (req, res) => {
    wishlistService.listAllWishlistItems(req.body).then((result) => {
        res.status(constant.HTML_STATUS_CODE.SUCCESS).json(result);
    }).catch((error) => {
        res.status(error.status || constant.HTML_STATUS_CODE.INTERNAL_ERROR).json(error);
    })
});

// route.put('/updateWishlist/:wishlistId', isAuthenticate, (req, res) => {
//     wishlistService.updateWishlist(req.params, req.body).then((result) => {
//         if (result.n > 0 || result.nModified > 0) {
//             res.status(constant.HTML_STATUS_CODE.SUCCESS).json({ message: 'Cart updated successfully!' });
//         } else {
//             res.status(constant.HTML_STATUS_CODE.TIME_OUT).json({message: 'Updation failed try again!'})
//         }
//     }).catch((error) => {
//         res.status(error.status || constant.HTML_STATUS_CODE.INTERNAL_ERROR).json(error);
//     })
// });

route.delete('/deleteWishlist/:wishlistId/:productId', isAuthenticate, (req, res) => {
    wishlistService.deleteWishlist(req.params, req.user).then((result) => {
        if (result.n > 0 || result.nModified > 0) {
            res.status(constant.HTML_STATUS_CODE.SUCCESS).json({ message: 'Wishlist deleted successfully!' });
        }
    }).catch((error) => {
        res.status(error.status || constant.HTML_STATUS_CODE.INTERNAL_ERROR).json(error);
    })
});

module.exports = route;