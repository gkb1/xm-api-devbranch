const express = require('express');
const frameIdService = require('../service/frameId-service');
const route = express.Router();
const constant = require('../utils/constant');
const isAuthenticate = require('../service/token-service');
const multipartMiddleware = require('connect-multiparty')();

route.post('/create',multipartMiddleware, isAuthenticate, (req, res) => {
    frameIdService.createFrameId(req, req.user).then((result) => {
        res.status(constant.HTML_STATUS_CODE.SUCCESS).json(result);
    }).catch((error) => {
        res.status(error.status || constant.HTML_STATUS_CODE.INTERNAL_ERROR).json(error);
    })
});

route.get('/:frameId', isAuthenticate, (req, res) => {
    frameIdService.getFrameById(req.user, req.params.frameId).then((result) => {
        res.status(constant.HTML_STATUS_CODE.SUCCESS).json(result);
    }).catch((error) => {
        res.status(error.status || constant.HTML_STATUS_CODE.INTERNAL_ERROR).json(error);
    })
});

route.post('/getAllFrames', (req, res) => {
    frameIdService.listAllFrameId(req.body).then((result) => {
        res.status(constant.HTML_STATUS_CODE.SUCCESS).json(result);
    }).catch((error) => {
        res.status(error.status || constant.HTML_STATUS_CODE.INTERNAL_ERROR).json(error);
    })
});

route.put('/updateFrame/:ecpUserId/:frameId', isAuthenticate, (req, res) => {
    frameIdService.updateFrameId(req.params, req.body).then((result) => {
        if (result.n > 0 || result.nModified > 0) {
            res.status(constant.HTML_STATUS_CODE.SUCCESS).json({ message: 'FrameId updated successfully!' });
        } else {
            res.status(constant.HTML_STATUS_CODE.TIME_OUT).json({message: 'Updation failed try again!'})
        }
    }).catch((error) => {
        res.status(error.status || constant.HTML_STATUS_CODE.INTERNAL_ERROR).json(error);
    })
});

route.put('/updateAllFrameStatusOfEcp/:ecpUserId', isAuthenticate, (req, res) => {
    frameIdService.updateAllFrameStatusOfEcp(req.user, req.params, req.body).then((result) => {
        if (result.n > 0 || result.nModified > 0) {
            res.status(constant.HTML_STATUS_CODE.SUCCESS).json({ message: 'Status updated successfully!' });
        } else {
            res.status(constant.HTML_STATUS_CODE.TIME_OUT).json({message: 'Unable to update, Please try again later!'})
        }
    }).catch((error) => {
        res.status(error.status || constant.HTML_STATUS_CODE.INTERNAL_ERROR).json(error);
    })
});

route.delete('/deleteFrame/:frameId', isAuthenticate, (req, res) => {
    frameIdService.deleteFrameId(req.params, req.user).then((result) => {
        if (result.n > 0 || result.nModified > 0) {
            res.status(constant.HTML_STATUS_CODE.SUCCESS).json({ message: 'FrameId deleted successfully!' });
        }
    }).catch((error) => {
        res.status(error.status || constant.HTML_STATUS_CODE.INTERNAL_ERROR).json(error);
    })
});

module.exports = route;