const express = require('express');
const constant = require('./../utils/constant');
const notificationService = require('./../service/notification-service');
const isAuthenticate = require('./../service/token-service');
var request = require('request');

const route = express.Router();

route.post('/sendNotificationToUsers', isAuthenticate, (req, res) => {
    notificationService.sendNotificationToUsers(req.body, req.user).then((result) => {
        res.status(constant.HTML_STATUS_CODE.SUCCESS).json(result);
    }).catch((error) => {
        res.status(error.status || constant.HTML_STATUS_CODE.INTERNAL_ERROR).json(error);
    });
});

module.exports = route;