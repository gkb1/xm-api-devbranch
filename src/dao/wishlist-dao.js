const _ = require('lodash');
const WishlistModel = require('../model/wishlist-schema');

exports.create = (wishlistDetail) => {
    return new WishlistModel({
        products: wishlistDetail.products,
        userId: wishlistDetail.userId,
        wishlistId: wishlistDetail.wishlistId
    }).save();

};

exports.update = (id, wishlistDetail) => {
    return WishlistModel.updateOne({ _id: id }, { $set: wishlistDetail });
};

exports.removeItemFormWishlist = (wishlist) => {
    return WishlistModel.update(
        { 'products.wishlistId': wishlist.wishlistId },
        { $pull: { products: { productId: wishlist.productId } } },
        { multi: true }
    )
};

exports.getOne = (conditions, deselectFields) => {
    if (deselectFields) {
        return WishlistModel.findOne(conditions, deselectFields);
    } else {
        return WishlistModel.findOne(conditions);
    }
};

exports.getAll = async (limit, page, sort, conditions) => {
    let bannerData = WishlistModel.find(conditions).limit(limit).skip(page).sort(sort ? sort : { 'auditFields.updatedAt': -1 })
    return bannerData;
}