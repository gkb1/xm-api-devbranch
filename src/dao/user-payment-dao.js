const UserPaymentModel = require('../model/user-payment-schema');
const baseDAO = require('./base-dao');

// For Create User 
exports.create = (userPaymentDetail) => {
    return new UserPaymentModel({
        paymentId: userPaymentDetail.paymentId,    
        user: userPaymentDetail.user,
        payment: userPaymentDetail.payment,
        payment_Msg: userPaymentDetail.payment_Msg,
        paymentDateAndTime: userPaymentDetail.paymentDateAndTime,
    }).save();
};

// For Update Sel User Order Detail
exports.updateByCondition = (condition, updateFields) => {
    return UserPaymentModel.updateOne(condition, { $set: updateFields });
};
exports.delete = (id, message) => {
    return UserOrderModel.updateOne({ '_id': id }, { $set: { 'auditFields.isDeleted': true, 'auditFields.isActive': false, 'auditFields.deleteMessage': message } });
};

exports.getOne = (conditions, deselectFields) => {
    if (deselectFields) {
        return UserPaymentModel.findOne(conditions, deselectFields);
    } else {
        return UserPaymentModel.findOne(conditions);
    }
};

exports.getAllUserPayments = async (limit, page, conditions) => {
    conditions = baseDAO.prepareConditionForDeletedData(conditions);
    let list = UserOrderModel.find(conditions);
    const count = UserPaymentModel.countDocuments(conditions);
    const result = await Promise.all([list, count]);
    return { userpayments: result[0], count: result[1] };
}

exports.getByConditions = (conditions = {}) => {
    return UserPaymentModel.find(conditions);
}



