const UserOrderModel = require('../model/order-schema');
const baseDAO = require('./base-dao');

// For Create User 
exports.create = (userOrderDetail) => {
    return new UserOrderModel({
        orderId: userOrderDetail.orderId,    
        order_no: userOrderDetail.order_no,
        user: userOrderDetail.user,
        order_type: userOrderDetail.user_type,
        cust_refno: userOrderDetail.cust_refno,
        lens: userOrderDetail.lens,
        status: userOrderDetail.status,
        orderDateTime: userOrderDetail.orderDateTime,
    }).save();
};

// For Update Sel User Order Detail
exports.updateByCondition = (condition, updateFields) => {
    return UserOrderModel.updateOne(condition, { $set: updateFields });
};
exports.delete = (id, message) => {
    return UserOrderModel.updateOne({ '_id': id }, { $set: { 'auditFields.isDeleted': true, 'auditFields.isActive': false, 'auditFields.deleteMessage': message } });
};

exports.getOne = (conditions, deselectFields) => {
    if (deselectFields) {
        return UserOrderModel.findOne(conditions, deselectFields);
    } else {
        return UserOrderModel.findOne(conditions);
    }
};

exports.getAllUserOrders = async (limit, page, conditions) => {
    conditions = baseDAO.prepareConditionForDeletedData(conditions);
    let list = UserOrderModel.find(conditions);
    const count = UserOrderModel.countDocuments(conditions);
    const result = await Promise.all([list, count]);
    return { userorders: result[0], count: result[1] };
}

exports.getByConditions = (conditions = {}) => {
    return UserOrderModels.find(conditions);
}



