const _ = require('lodash');
const frameShapeModel = require('../model/frame-shape-schema');

exports.create = (frameShapeDetail) => {
    return new frameShapeModel({
        name: frameShapeDetail.name,
        code: frameShapeDetail.code,
        // media: frameShapeDetail.media,
        createdAt: frameShapeDetail.createdAt
    }).save();

};

exports.update = (conditions, frameShapeDetail) => {
    return frameShapeModel.updateOne(conditions, { $set: frameShapeDetail });
};

exports.getOne = (conditions, deselectFields) => {
    if (deselectFields) {
        return frameShapeModel.findOne(conditions, deselectFields);
    } else {
        return frameShapeModel.findOne(conditions);
    }
};

exports.getPreviousCode = (conditions) => {
    return frameShapeModel.find(conditions).sort({ createdAt: -1 }).limit(1);
};

exports.getAll = async (limit, page, sort, conditions) => {
    let bannerData = frameShapeModel.find(conditions).limit(limit).skip(page).sort(sort ? sort : { 'auditFields.updatedAt': -1 })
    return bannerData;
}