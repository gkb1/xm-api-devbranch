const _ = require('lodash');
const frameTypeModel = require('../model/frame-type-schema');

exports.create = (frameTypeDetail) => {
    return new frameTypeModel({
        name: frameTypeDetail.name,
        code: frameTypeDetail.code,
        // media: frameTypeDetail.media,
        createdAt: frameTypeDetail.createdAt
    }).save();

};

exports.update = (conditions, frameTypeDetail) => {
    return frameTypeModel.updateOne(conditions, { $set: frameTypeDetail });
};

exports.getOne = (conditions, deselectFields) => {
    if (deselectFields) {
        return frameTypeModel.findOne(conditions, deselectFields);
    } else {
        return frameTypeModel.findOne(conditions);
    }
};
exports.getPreviousCode = (conditions) => {
    return frameTypeModel.find(conditions).sort({ createdAt: -1 }).limit(1);
};
exports.getAll = async (limit, page, sort, conditions) => {
    let bannerData = frameTypeModel.find(conditions).limit(limit).skip(page).sort(sort ? sort : { 'auditFields.updatedAt': -1 })
    return bannerData;
}