const _ = require('lodash');
const ProductModel = require('../model/product-schema');
const baseDAO = require('./base-dao');

// For Create product
exports.create = (prodDetail) => {
    return new ProductModel({
        name: prodDetail.name,
        description: prodDetail.description,
        productId: prodDetail.productId,
        createdEcpCustId: prodDetail.createdEcpCustId,
        createdFrameCollectorId: prodDetail.createdFrameCollectorId,
        SKU: prodDetail.SKU,
        pricing: prodDetail.pricing,
        model: prodDetail.model,
        brand: prodDetail.brand,
        product_info: prodDetail.product_info,
        category: prodDetail.category,
        frame_attributes: prodDetail.frame_attributes,
        prdt_attributes: prodDetail.prdt_attributes,
        badge: prodDetail.badge,
        tags: prodDetail.tags,
    }).save();
};

exports.findOnePrdAttribute = (filter) => {
    return ProductModel.findOne(filter, { 'prdt_attributes.$': 1 })
}

exports.update = (id, prodDetail) => {
    return ProductModel.updateOne({ productId: id }, { $set: prodDetail });
};

exports.updateByCondition = (condition, updateFields) => {
    return ProductModel.updateOne(condition, { $set: updateFields });
};

exports.updateByCond = (condition, updateFields) => {
    return ProductModel.updateOne(condition, updateFields);
};
exports.delete = (id) => {
    return ProductModel.deleteOne({ '_id': id });
};

exports.prepareConditionForDeletedData = function (condition) {
    if (_.isUndefined(condition) || _.isEmpty(condition)) {
        condition = {};
        condition['auditFields.isDeleted'] = false;
        return condition;
    }
    if (_.isUndefined(condition['auditFields.isDeleted'])) {
        condition['auditFields.isDeleted'] = false;
    }
    return condition;
}

exports.getById = (id) => {
    return ProductModel.findUser('_id', id).select('-auditFields');
};
exports.getTrendingProduct = (condition) => {
    return ProductModel.aggregate([
        { $project: { view_count: { $max: "$view_count" } } },
        { $sort: { view_count: -1 } }
    ])
};
exports.getByFrameId = (condition) => {
    return ProductModel.findOne({ prdt_attributes: { $elemMatch: { "colour.frameId": condition.frameId } } })
}

// Product attribute 
exports.updateSelProductsAttribute = async (condition, updateFields) => {
    return await ProductModel.updateOne(
        {
            productId: condition.productId,
            'prdt_attributes.prdAttributeId': condition.prdAttributeId,
            // prdt_attributes: {
            //     $elemMatch:
            //     {
            //         "prdAttributeId": condition.prdAttributeId,
            //         // "colour.prdColourId": condition.prdColorId
            //     }
            // }
        },
        {
            $push: {
                "prdt_attributes.$.colour.store": {
                    _id: updateFields.colour.store[0]._id,
                    cust_code: updateFields.colour.store[0].cust_code,
                    store_code: updateFields.colour.store[0].store_code,
                    tagName: updateFields.colour.store[0].tagName
                }
            }
        }
    )
}

exports.updateProdAttribute = (id, prodDetail) => {
    return ProductModel.updateOne({ productId: id }, { $addToSet: { 'prdt_attributes': prodDetail } });
};


exports.removeStore = (prodDetail) => {
    return ProductModel.updateOne(
        {
            productId: prodDetail.productId,
            prdt_attributes: {
                $elemMatch:
                {
                    "prdAttributeId": prodDetail.prdAttributeId,
                }
            }
        },
        { $pull: { "prdt_attributes.$.colour.store": { cust_code: prodDetail.cust_code } } },
        { multi: true }
    )
};


exports.findOneByCondition = (fieldName, value) => {
    return ProductModel.findUser(fieldName, value);
};

exports.getOne = (conditions, deselectFields) => {
    if (deselectFields) {
        return ProductModel.findOne(conditions, deselectFields);
    } else {
        return ProductModel.findOne(conditions);
    }
};

// exports.getAll = async (conditions) => {
//     return await ProductModel.find(conditions).select('-auditFields')
// }

exports.getAllPrdAttributes = async (conditions) => {
    const res = await ProductModel.aggregate([
        { $unwind: "$prdt_attributes" },
        { $project: { prdt_attributes: 1 } },
        {
            $match: {
                "prdt_attributes.prdAttributeId": conditions.prdAttributeId,
                "prdt_attributes.colour.prdColourId": conditions.prdColorId,
                "prdt_attributes.colour.size.prdSizeId": conditions.prdSizeId
            }
        }
    ])
    return res;
}

exports.getAll = async (limit, page, sort, conditions) => {
    // const queryObj = { ...conditions };
    // const excludedFields = ['page', 'sort', 'limit', 'fields'];
    // excludedFields.forEach(el => delete queryObj[el]);
    // let queryStr = JSON.stringify(queryObj);
    // // console.log(queryStr);
    // condition = conditions.name;
    // const page = conditions.page * 1 || 1;
    // const limit = conditions.limit * 1 || 100;
    // const skip = (page - 1) * limit;
    // conditions = await ProductModel.find(JSON.parse(queryStr)).skip(skip).limit(limit);
    // return conditions;
    let productRecord = ProductModel.find(conditions).limit(limit).skip(page).sort(sort ? sort : { 'auditFields.updatedAt': -1 }).populate({
        path: 'category._id',
        model: 'category',
        select: '_id name categoryId title path',
    }).populate({
        path: 'category.childCategories._id',
        model: 'category',
        select: '_id name categoryId title path',
    })
    const productCount = ProductModel.countDocuments(conditions);
    const result = await Promise.all([productRecord, productCount]);
    return { data: result[0], count: result[1] };
}

exports.getSelPrdtAttr = async (conditions) => {
    conditions = baseDAO.prepareConditionForDeletedData(conditions);
    return ProductModel.findOne(conditions, {
        name: 1, SKU: 1, productId: 1, brand: 1, model:1 ,description: 1,
        category: 1, 'prdt_attributes.$': 1, 'prdt_attributes.colour.store':1, pricing: 1, product_info: 1, frame_attributes: 1, gender: 1, badge: 1,
        tags: 1, view_count: 1, rating: 1, totalReviewCount: 1, isPremium: 1,
        isApproved: 1, isActivate: 1, isUnisex: 1, auditFields: 1
    });
}

exports.updateSKUToStore = async (condition, updateFields) => {
    return await ProductModel.updateOne(
        {
            "productId": condition.productId,
            "prdt_attributes.prdAttributeId": condition.prdAttributeId,
            'prdt_attributes.colour.store.store_code': condition.storeId,
        },
        {
            // { "$set": { "friends.$.u.name": "hello" } }
            $set: {
                "prdt_attributes.$.colour.store.$[].SKU": updateFields
            }
        }
    )
}

exports.getAllTagNamesToVerifySKU = async (limit, page, sort, conditions) => {
    const list = await ProductModel.aggregate([
        { $unwind: "$prdt_attributes" },
        { $unwind: "$prdt_attributes.colour.store"},
        { $match: conditions },
        { "$skip": page},
        { "$limit": limit },
        {
            $group: {
                _id: {
                    _id: '$_id',
                    name: '$name',
                    category: '$category',
                    productId: '$productId',
                    prdt_attributes: '$prdt_attributes'
                }
            }
        },
    ]);
     return list;
}