const _ = require('lodash');
const BannerModel = require('../model/banner-schema');
// For Create banner
exports.create = (bannerDetail) => {
    return new BannerModel({
        // image: bannerDetail.image,
        redirectUrl: bannerDetail.redirectUrl,
        startDate: bannerDetail.startDate,
        endDate: bannerDetail.endDate,
        bannerId: bannerDetail.bannerId
    }).save();

};

exports.update = (id, bannerDetail) => {
    return BannerModel.updateOne({ _id: id }, { $set: bannerDetail });
};

exports.updateByCondition = (condition, updateFields) => {
    return BannerModel.updateOne(condition, { $set: updateFields });
};

exports.updateByCond = (condition, updateFields) => {
    return BannerModel.updateOne(condition, updateFields);
};
exports.delete = (id) => {
    return BannerModel.deleteOne({ '_id': id });
};

exports.getOne = (conditions, deselectFields) => {
    if (deselectFields) {
        return BannerModel.findOne(conditions, deselectFields);
    } else {
        return BannerModel.findOne(conditions);
    }
};

exports.getAll = async (limit, page, sort, conditions) => {
    let bannerData = BannerModel.find(conditions).limit(limit).skip(page).sort(sort ? sort : { 'auditFields.updatedAt': -1 })
    return bannerData;
}

exports.prepareConditionForDeletedData = function (condition) {
    if (_.isUndefined(condition) || _.isEmpty(condition)) {
        condition = {};
        condition['auditFields.isDeleted'] = false;
        return condition;
    }
    if (_.isUndefined(condition['auditFields.isDeleted'])) {
        condition['auditFields.isDeleted'] = false;
    }
    return condition;
}

exports.getById = (id) => {
    return BannerModel.findOne({ '_id': id }).select('-auditFields');
};