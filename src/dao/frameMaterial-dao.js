const _ = require('lodash');
const frameMaterialModel = require('../model/frame-material-schema');

exports.create = (frameMaterialDetail) => {
    return new frameMaterialModel({
        name: frameMaterialDetail.name,
        code: frameMaterialDetail.code,
        createdAt: frameMaterialDetail.createdAt
    }).save();

};

exports.update = (conditions, frameMaterialDetail) => {
    return frameMaterialModel.updateOne(conditions, { $set: frameMaterialDetail });
};

exports.getOne = (conditions, deselectFields) => {
    if (deselectFields) {
        return frameMaterialModel.findOne(conditions, deselectFields);
    } else {
        return frameMaterialModel.findOne(conditions);
    }
};
exports.getPreviousCode = (conditions) => {
    return frameMaterialModel.find(conditions).sort( { createdAt: -1 } ).limit(1);
};

exports.getAll = async (limit, page, sort, conditions) => {
    let bannerData = frameMaterialModel.find(conditions).limit(limit).skip(page).sort(sort ? sort : { 'auditFields.updatedAt': -1 })
    return bannerData;
}