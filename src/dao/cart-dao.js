const _ = require('lodash');
const CartModel = require('../model/usercart-schema');
// For Add to cart
exports.create = (cartDetail) => {
    return new CartModel({
        products: cartDetail.products,
        userId: cartDetail.userId,
        cartId: cartDetail.cartId
    }).save();

};

exports.removeItemFormCart = (cartId) => {
    return CartModel.update(
        { 'products.productId': cartId },
        { $pull: { products: { productId: cartId } } },
        { multi: true }
    )
};


exports.updateCart = (condition, updateFields) => {
    if (updateFields.productAttr) {
        return CartModel.updateOne(
            { cartId: condition.cartId, "products.productId": condition.productId },
            { $set: { "products.$.quantity": updateFields.quantity, "products.$.productAttr": updateFields.productAttr } }
        )
    } else {
        return CartModel.updateOne(
            { cartId: condition.cartId, "products.productId": condition.productId },
            { $set: { "products.$.quantity": updateFields.quantity } }
        )
    }
}

exports.getOne = (conditions, deselectFields) => {
    if (deselectFields) {
        return CartModel.findOne(conditions, deselectFields);
    } else {
        return CartModel.findOne(conditions);
    }
};

exports.getAll = async (limit, page, sort, conditions) => {
    let bannerData = CartModel.find(conditions).limit(limit).skip(page).sort(sort ? sort : { 'auditFields.updatedAt': -1 })
    return bannerData;
}