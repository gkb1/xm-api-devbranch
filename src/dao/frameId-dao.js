const _ = require('lodash');
const frameModel = require('../model/frameID-schema');
const baseDAO = require('./base-dao');

exports.create = frameDetail => {
    return new frameModel({
        store: {
            ecpUserId: frameDetail.ecpUserId,
            cust_code: frameDetail.cust_code,
            store_code: frameDetail.store_code,
            frame: {
                frameCollectorId: frameDetail.frameCollectorId,
                frameID: frameDetail.frameId,
                tagName: frameDetail.tagName,
                media: frameDetail.media
            }
        }
    }).save();
};

exports.update = (conditions, frameDetail) => {
    return frameModel.updateOne(conditions, { $addToSet: frameDetail });
};

exports.updatemedia = async (condition, updateFields) => {
    return await frameModel.updateOne(
        {
            _id: condition._id
        },
        {
            $push: {
                "store.frame.media": updateFields
            }
        },
        { upsert: true }
    );
};

exports.updateFrameIdStatus = async (id, frameIdDetail) => {
    if (id.frameId) {
        return await frameModel.updateOne(
            {
                "store.ecpUserId": id.ecpUserId,
                "store.frame": { $elemMatch: { "frameID": id.frameId } }
            },
            { $set: { "store.frame.$.status.isApproved": frameIdDetail.isApproved } });
    } else {
        return await frameModel.update(
            {
                "store.ecpUserId": id.ecpUserId,
            },
            { $set: { "store.frame.$[].status.isApproved": frameIdDetail.isApproved } }, {$multi: true})
    }
    // const frameIdStatus = frameModel.aggregate([
    //     { $unwind: "$store.frame"},
    //     { $match: {  "store.frame.frameID": id.frameId  } },
    //     { $out : 'frameIdStatus'}
    // ])
    // console.log(frameIdStatus);
    // return frameIdStatus;
};

exports.updateFrameId = async function (conditions, brandDetail) {
    const result = await frameModel.updateOne(conditions, { $set: brandDetail });
    return result;
};

exports.getOne = (conditions, deselectFields) => {
    if (deselectFields) {
        return frameModel.findOne(conditions, deselectFields);
    } else {
        return frameModel.findOne(conditions);
    }
};
exports.getAllFrameIdStatusFilter = async conditions => {
    let bannerData = await frameModel.aggregate([
        // { $unwind: "$store.frame" },
        { $match: { "store.frame.status.isApproved": conditions.frameFilter } },
        {
            $project: {
                _id: 1,
                'store.frame': {
                    $filter: {
                        input: '$store.frame',
                        as: 'item',
                        cond: { $eq: ['$$item.status.isApproved', conditions.frameFilter] }
                    }
                },
                frameCollector: { name: 1, email: 1, mobile: 1, userid: 1, _id: 1 }
            }
        }
        // { $project: { _id: 1, store: { cust_code: 1, frame: { media: 1, status: 1, frameID: 1, tagName: 1 } }, 
        //             //   ecpUser: { name: 1, email: 1, mobile: 1, userid: 1, _id: 1 }, 
        //               frameCollector: { name: 1, email: 1, mobile: 1, userid: 1, _id: 1 } 
        //             } },                    
    ]);
    // const populateStoreRecord = frameModel.populate(bannerData, {
    //     path: 'store.ecpUserId',
    //     model: 'users',
    //     select: '_id name mobile userId email ecpCust',
    // });

    const populateFrameCollectorRecord = await frameModel.populate(bannerData, {
        path: 'store.frame.frameCollectorId',
        model: 'users',
        select: '_id name mobile userId email'
    });
    return populateFrameCollectorRecord;
};

exports.getOneWithPopulatedData = (conditions) => {
    return frameModel.findOne(conditions).populate({
        path: 'store.ecpUserId',
        model: 'users',
        select: '_id name mobile userId email ecpCust',
    }).populate({
        path: 'store.frame.frameCollectorId',
        model: 'users',
        select: '_id name mobile userId email'
    });
};


exports.getAll = async (limit, page, sort, conditions) => {
    let bannerData = frameModel
        .find(conditions)
        .limit(limit)
        .skip(page)
        .sort(sort ? sort : { "auditFields.updatedAt": -1 })
        .populate({
            path: 'store.ecpUserId',
            model: 'users',
            select: '_id name mobile userId email ecpCust',
        }).populate({
            path: "store.frame.frameCollectorId",
            model: "users",
            select: "_id name mobile userId email"
        }).populate({
            path: "store.ecpUserId",
            model: "users",
            select: "_id name mobile userId email ecpCust"
        })
    return bannerData;
}

exports.getSelFrameByFrameId = async (conditions) => {
    conditions = baseDAO.prepareConditionForDeletedData(conditions);
    return frameModel.findOne(conditions, { 'store.frame.$': 1 }).populate({
        path: 'store.ecpUserId',
        model: 'users',
        select: '_id name mobile userId email ecpCust store_code',
    }).populate({
        path: 'store.frame.frameCollectorId',
        model: 'users',
        select: '_id name mobile userId email'
    });
}
