const _ = require('lodash');
const frameColourModel = require('../model/frame-colour-schema');

exports.create = (frameColourdetails) => {
    return new frameColourModel({
        actualColour: frameColourdetails.actualColour,
        parentColour: frameColourdetails.parentColour,
        hexCode: frameColourdetails.hexCode,
        XMCode: frameColourdetails.XMCode,
        createdAt:   frameColourdetails.createdAt
    }).save();
};

exports.getPreviousCode = (conditions) => {
    return frameColourModel.find(conditions).sort({ createdAt: -1 }).limit(1);
};

exports.getOne = async (conditions, deselectFields) => {
    if (deselectFields) {
        return await frameColourModel.findOne(conditions, deselectFields);
    } else {
       return await frameColourModel.findOne(conditions);
    }
};

exports.getAll = async (limit, page, sort, conditions) => {
    let bannerData = frameColourModel.find(conditions).limit(limit).skip(page).sort(sort ? sort : { 'auditFields.updatedAt': -1 })
    return bannerData;
}