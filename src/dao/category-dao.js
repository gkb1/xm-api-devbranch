const CategoryModel = require('../model/category-schema');
const baseDAO = require('./base-dao');
const _ = require('lodash')
// For Create Category 
exports.createCategory = (categoryDetail) => {
    return new CategoryModel({
        name: categoryDetail.name,
        title: categoryDetail.title,
        offer_id: categoryDetail.offer_id,
        categoryId: categoryDetail.categoryId,
        bannerImages: categoryDetail.bannerImages,
        path: categoryDetail.path,
        isRoot: categoryDetail.isRoot,
        childCategories: categoryDetail.childCategories,
    }).save();
};

exports.getOne = function (condition, returnSingleObj, callback) {
    condition = baseDAO.prepareConditionForDeletedData(condition);
    if (returnSingleObj) {
        return CategoryModel.findOne(condition, returnSingleObj).populate({
            path: 'childCategories._id',
            model: 'category',
            select: '_id name categoryId title path bannerImages'
        });
    } else {
        return CategoryModel.findOne(condition).populate({
            path: 'childCategories._id',
            model: 'category',
            select: '_id name categoryId title path bannerImages'
        });
    }
}

exports.getOneWithPopulateddata = function (condition) {
    condition = baseDAO.prepareConditionForDeletedData(condition);
    return CategoryModel.findOne(condition).populate({
        path: 'childCategories._id',
        model: 'category',
        select: '_id name categoryId title path bannerImages'
    });
}

exports.getAllByPageAndCond = async function (conditions) {
    conditions = baseDAO.prepareConditionForDeletedData(conditions);
    let list = CategoryModel.find(conditions).populate({
        path: 'childCategories._id',
        model: 'category',
        select: '_id name categoryId title path bannerImages'
    });
    const count = CategoryModel.countDocuments(conditions);
    const result = await Promise.all([list, count]);
    return { data: result[0], count: result[1] };
}

exports.getAllCatByName = async function (conditions) {
    // conditions = baseDAO.prepareConditionForDeletedData(conditions);
    const reg = [];
    if (conditions) {
        for (let i = 0; i < conditions.length; i++) {
            conditions = { $regex: new RegExp(conditions[i], "i") }
            reg.push(conditions)
        }
    }
    // conditon['isRoot'] = true;
    console.log(reg);
    let list = await CategoryModel.find({ isRoot: true, name: { $in: reg } });
    return list;
}

exports.updateCategory = function (condition, updateFields) {
    return CategoryModel.updateOne(condition, updateFields);
}
