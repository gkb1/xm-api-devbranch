const _ = require('lodash');
const frameModel = require('../model/frame-model-schema');

exports.create = (frameModelDetails) => {
    return new frameModel({
        name: frameModelDetails.name,
        code: frameModelDetails.code,
        modelCode: frameModelDetails.modelCode,
        brand: frameModelDetails.brand,
        createdAt: frameModelDetails.createdAt
    }).save();

};

exports.getPreviousCode = (conditions) => {
    return frameModel.find(conditions).sort({ createdAt: -1 }).limit(1);
};

exports.getOne = (conditions, deselectFields) => {
    if (deselectFields) {
        return frameModel.findOne(conditions, deselectFields);
    } else {
        return frameModel.findOne(conditions);
    }
};

exports.getAll = async (limit, page, sort, conditions) => {
    return frameModel.find(conditions).limit(limit).skip(page).sort(sort ? sort : { 'auditFields.updatedAt': -1 })
}