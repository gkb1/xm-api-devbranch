const BrandModel = require('../model/brand-schema');
const baseDAO = require('./base-dao');

// For Create Brand 
exports.createBrand = (brandDetail) => {
    return new BrandModel({
        name: brandDetail.name,
        description: brandDetail.description,
        brandId: brandDetail.brandId,
        gender: brandDetail.gender,
        category: brandDetail.category,
        media: brandDetail.media,
        code: brandDetail.code,
        createdAt: brandDetail.createdAt
    }).save();
};

exports.getOne = function (condition, returnSingleObj, callback) {
    condition = baseDAO.prepareConditionForDeletedData(condition);
    if (returnSingleObj) {
        return BrandModel.findOne(condition, returnSingleObj)
    } else {
        return BrandModel.findOne(condition)
    }
}

exports.getBrandByNameAndId = async function (conditions) {
    return await BrandModel.findOne({ $and: [{ name: conditions.name }, { brandId: conditions.brandId }] });
}

exports.updateBrandByNameAndId = async function (conditions, brandDetail) {
    const result = await BrandModel.updateOne(conditions, { $set: brandDetail });
    return result;
}
exports.getPreviousCode = (conditions) => {
    return BrandModel.find(conditions).sort({ createdAt: -1 }).limit(1);
};
exports.deleteBrandByNameAndId = async function (conditions) {
    const result = await BrandModel.deleteOne(conditions);
    return result;
}

exports.getAllByPageAndCond = async function (limit, page, sort, conditions) {
    conditions = baseDAO.prepareConditionForDeletedData(conditions);
    let record = BrandModel.find(conditions).limit(limit).skip(page).sort(sort ? sort : { 'auditFields.updatedAt': -1 }).populate({
        path: 'category._id',
        model: 'category',
        select: '_id name categoryId title path auditFields'
    });
    const count = BrandModel.countDocuments(conditions);
    const result = await Promise.all([record, count]);
    return { data: result[0], count: result[1] };
}

exports.updateBrand = function (condition, updateFields) {
    return BrandModel.updateOne(condition, updateFields);
}
