const UserModel = require('../model/users-schema');
const baseDAO = require('./base-dao');
const deselectFields = { deselect: ['otp'] };
// const UserDAO = {

// For Create User 
exports.create = (userDetail) => {
    return new UserModel({
        _id: userDetail._id,
        name: userDetail.name,
        gender: userDetail.gender,
        userId: userDetail.userId,
        role: userDetail.role ? userDetail.role.toUpperCase() : 'CUSTOMER',
        email: userDetail.email,
        mobile: userDetail.mobile,
        ecpCust: userDetail.ecpCust,
        store_code: userDetail.store_code,
        auditFields: userDetail.auditFields,
        // otpExpiresIn: userDetail.otpExpiresIn,
        // isVerified: userDetail.isVerified
    }).save();
};

// For Update User Detail
exports.update = (id, userDetail) => {
    return UserModel.updateOne({ _id: id }, { $set: userDetail }, { upsert: true });
};

// For Update Sel User Detail
exports.updateByCondition = (condition, updateFields) => {
    return UserModel.updateOne(condition, { $set: updateFields });
};

exports.updateByCondn = (condition, updateFields) => {
    return UserModel.updateOne(condition, updateFields);
};
exports.updateStore = (id, updateFields) => {
    return UserModel.updateOne(id, { $addToSet: updateFields });
};

exports.delete = (id, message) => {
    return UserModel.updateOne({ '_id': id }, { $set: { 'auditFields.isDeleted': true, 'auditFields.isActive': false, 'auditFields.deleteMessage': message } });
};

exports.setFCM = (userId, _fcmId) => {
    return UserModel.updateOne({ '_id': userId }, { $set: { 'fcmId': _fcmId } });
};

exports.getById = async (id) => {
    return await UserModel.findUser('_id', id);
};
exports.getByToken = (token) => {
    return UserModel.findOne({ otp: token });
}

exports.getByEmail = (email) => {
    return UserModel.findUser('email', email);
};

exports.findOneByCondition = (fieldName, value) => {
    return UserModel.findUser(fieldName, value);
};

exports.getByPhone = (phone) => {
    return UserModel.findUser('phone', phone);
};

exports.getOne = (conditions, deselectFields) => {
    if (deselectFields) {
        return UserModel.findOne(conditions, deselectFields);
    } else {
        return UserModel.findOne(conditions);
    }
};

exports.getAllUsers = async (limit, page, bodyData, conditions) => {
    // conditions = baseDAO.prepareConditionForDeletedData(conditions);
    let list;
    if (bodyData && !bodyData.showOtp) {
        list = UserModel.find(conditions, { otp: 0 }).sort({ "auditFields.createdAt": -1 }).populate({
            path: 'mystores._id',
            model: 'users',
            select: '_id ecpCust'
        });;
    } else {
        list = UserModel.find(conditions).sort({ "auditFields.createdAt": -1 });
    }
    const count = UserModel.countDocuments(conditions);
    const result = await Promise.all([list, count]);
    return { data: result[0], count: result[1] };
}

exports.getAllUsersByCondn = async (conditions) => {
    conditions = baseDAO.prepareConditionForDeletedData(conditions);
    return UserModel.find(conditions);
}


exports.getStoresWithPopulatedData = async (conditions, deselectFields) => {
    conditions = baseDAO.prepareConditionForDeletedData(conditions);
    const selUser = UserModel.findOne(conditions, deselectFields).populate({
        path: 'mystores._id',
        model: 'users',
        select: '_id ecpCust'
    });
    return selUser;
}

exports.getAllUserNotification = async (limit, skip, conditions) => {
    conditions = baseDAO.prepareConditionForDeletedData(conditions);
    let list = await UserModel.aggregate([
        { "$unwind": "$notificationList" },
        { $match: conditions },
        { "$sort": { "dateTime": -1 } },
        { "$skip": skip },
        { "$limit": limit },
        {
            $group: {
                _id: 0,
                notificationList: { $addToSet: '$notificationList' }
            }
        },
    ]);

    let count = await UserModel.aggregate([
        { "$unwind": "$notificationList" },
        { $match: conditions },
        { "$sort": { "dateTime": -1 } },
        {
            $group: {
                _id: 0,
                "totalCount": { $sum: 1 },
            }
        },
    ]);

    const result = await Promise.all([list, count]);
    return { data: { notificationList: result[0][0].notificationList }, totalCount: result[1][0].totalCount };
}

exports.getByUserName = (userName) => {
    let conditions = { username: userName };
    conditions = baseDAO.prepareConditionForActiveData(conditions);
    return UserModel.findOne(conditions);
}
exports.updatePassword = (em, pass) => {
    return UserModel.updateOne({ email: em }, { $set: { password: pass } });
}

exports.activate = (id, flag) => {
    return OnlineTestResultModel.updateOne({ '_id': id }, { $set: { 'auditFields.isActive': flag } });
}

exports.getByConditions = (conditions = {}) => {
    return UserModel.find(conditions);
}
exports.removeOTP = (id) => {
    return UserModel.update({ _id: id }, {
        $unset: {
            otpExpiresIn: "",
            otp: ""
        }
    });
};

exports.removeStore = (storeData) => {
    return UserModel.updateOne(
        { 'mystores.cust_code': storeData.cust_code },
        { $pull: { mystores: { cust_code: storeData.cust_code } } },
        { multi: true }
    )
};

