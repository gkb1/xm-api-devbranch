const _ = require('lodash');
const mongoose = require('mongoose');
const baseSchema = require('./base-schema');
const constant = require('../utils/constant');

const modelName = 'frameID';
const Schema = mongoose.Schema;

const MediaSchema = new Schema({
    name: {
        type: String,
        default: '',
        required: true
    },
    url: {
        type: String,
        default: '',
        required: true
    },
    type: {
        enum: constant.lookup.MEDIA_TYPE,
        required: true,
        type: String
    },
});

const FrameSchema = new Schema({
    store: {
        ecpUserId: { type: Schema.Types.ObjectId, ref: 'users', required: true }, // ecpCustId
        cust_code: { type: String, required: true }, // ecpCustCode
        store_code: { type: String, required: true },
        frame: [{
            frameCollectorId: { type: Schema.Types.ObjectId, ref: 'users', required: true },
            frameID: { type: String, required: true },
            tagName: { type: String, required: true }, // frameID + '-' + cust_code 
            media: { type: MediaSchema },
            status: {
                    isApproved: { type: Boolean, default: false },
                message: { type: String }
            },
            isModelShoot: { type: Boolean, default: false }
        }]
    },
    auditFields: {
        type: baseSchema.AuditFieldSchema
    }
});

// This function will call before each save() so that we can set auditFields.
FrameSchema.pre('save', function (next) {
    // console.log(this.auditFields);
    // this.auditFields = { createdAt: new Date() };
    if (_.isNil(this.auditFields)) {
        this.auditFields = {};
    }
    this.auditFields.updatedAt = new Date();
    if (_.isNil(this.auditFields.createdAt)) {
        this.auditFields.createdAt = new Date();
    }
    if (_.isNil(this.auditFields.isActive)) {
        this.auditFields.isActive = true;
    }
    if (_.isNil(this.auditFields.isDeleted)) {
        this.auditFields.isDeleted = false;
    }
    next();
});


module.exports = mongoose.model(modelName, FrameSchema); //Compiling schema to model