const _ = require('lodash');
const mongoose = require('mongoose');
const baseSchema = require('./base-schema');
const validate = require('validator');

const modelName = 'wishlist';
const Schema = mongoose.Schema;
const ObjectId = mongoose.Schema.Types.ObjectId;
const constant = require('../utils/constant');

const WishlistSchema = new Schema({
    products: [{
        productId: { type: Schema.Types.ObjectId, ref: 'product', unique: true, required: true }
        // {
        //     type: Schema.Types.ObjectId,
        //     ref: 'product',
        //     unique: true,
        //     required: true
        // }
    }, { _id: false }],
    userId: {
        type: Schema.Types.ObjectId,
        ref: 'users',
        required: true
    },
    wishlistId: { type: String, unique: true },
    auditFields: {
        type: baseSchema.AuditFieldSchema
    }
});

// This function will call before each save() so that we can set auditFields.
WishlistSchema.pre('save', function (next) {
    // console.log(this.auditFields);
    // this.auditFields = { createdAt: new Date() };
    if (_.isNil(this.auditFields)) {
        this.auditFields = {};
    }
    this.auditFields.updatedAt = new Date();
    if (_.isNil(this.auditFields.createdAt)) {
        this.auditFields.createdAt = new Date();
    }
    if (_.isNil(this.auditFields.isActive)) {
        this.auditFields.isActive = true;
    }
    if (_.isNil(this.auditFields.isDeleted)) {
        this.auditFields.isDeleted = false;
    }
    next();
});


module.exports = mongoose.model(modelName, WishlistSchema); //Compiling schema to model