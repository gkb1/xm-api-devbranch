const _ = require('lodash');
const mongoose = require('mongoose');
const baseSchema = require('./base-schema');
const validate = require('validator');

const modelName = 'brand';
const Schema = mongoose.Schema;
const ObjectId = mongoose.Schema.Types.ObjectId;
const constant = require('../utils/constant');

const MediaSchema = new Schema({
    name: {
        type: String,
        default: '',
        required: true
    },
    url: {
        type: String,
        default: '',
        required: true
    },
    type: {
        enum: constant.lookup.MEDIA_TYPE,
        required: true,
        type: String
    },
}, { _id: false });

const BrandSchema = new Schema({
    name: {
        type: String,
        unique: true, trim: true, required: true
    },
    description: {
        type: String,
        trim: true
    },
    media: { type: MediaSchema },
    brandId: { type: String, unique: true },
    gender: {
        type: Array,
        default: 'Unisex'
    },
    code: { type: String , required: true, unique: true },
    // category: [{ 
    //     _id: {type: ObjectId, required: true,
    //         type: Schema.Types.ObjectId,
    //         ref: 'category'
    //     },
    //     categoryId: { type: String, required: true },
    // 	childCategories: {
    // 		_id: {type: ObjectId},
    // 	},
    // 	subCategory: {
    // 		_id: {type: ObjectId},
    // 	}
    // }],
    category: [{    // child categories
        _id: {
            type: ObjectId, required: true,
            type: Schema.Types.ObjectId,
            ref: 'category'
        }
    }],
    auditFields: {
        type: baseSchema.AuditFieldSchema
    },
    createdAt: Date
});

// This function will call before each save() so that we can set auditFields.
BrandSchema.pre('save', function (next) {
    // console.log(this.auditFields);
    // this.auditFields = { createdAt: new Date() };
    if (_.isNil(this.auditFields)) {
        this.auditFields = {};
    }
    this.auditFields.updatedAt = new Date();
    if (_.isNil(this.auditFields.createdAt)) {
        this.auditFields.createdAt = new Date();
    }
    if (_.isNil(this.auditFields.isActive)) {
        this.auditFields.isActive = true;
    }
    if (_.isNil(this.auditFields.isDeleted)) {
        this.auditFields.isDeleted = false;
    }
    next();
});


module.exports = mongoose.model(modelName, BrandSchema); //Compiling schema to model