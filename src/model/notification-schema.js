const _ = require('lodash');
const mongoose = require('mongoose');
const baseSchema = require('./base-schema');
const validate = require('validator');

const modelName = 'notification';
const Schema = mongoose.Schema;
const ObjectId = mongoose.Schema.Types.ObjectId;
const constant = require('../utils/constant');

const NotificationSchema = new Schema({
    userId: {
        type: Schema.Types.ObjectId,
        ref: 'users',
        required: true
    },
    notifications: [{
        nId: { type: String },
		title: { type: String },
		body: { type: String },
		icon: { type: String },
		url: { type: String },
		dateTime: { type: String }
    }],
    auditFields: {
        type: baseSchema.AuditFieldSchema
    }
});

// This function will call before each save() so that we can set auditFields.
NotificationSchema.pre('save', function (next) {
    // console.log(this.auditFields);
    // this.auditFields = { createdAt: new Date() };
    if (_.isNil(this.auditFields)) {
        this.auditFields = {};
    }
    this.auditFields.updatedAt = new Date();
    if (_.isNil(this.auditFields.createdAt)) {
        this.auditFields.createdAt = new Date();
    }
    if (_.isNil(this.auditFields.isActive)) {
        this.auditFields.isActive = true;
    }
    if (_.isNil(this.auditFields.isDeleted)) {
        this.auditFields.isDeleted = false;
    }
    next();
});


module.exports = mongoose.model(modelName, NotificationSchema); //Compiling schema to model