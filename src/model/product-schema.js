const _ = require('lodash');
const mongoose = require('mongoose');
const baseSchema = require('./base-schema');
const validate = require('validator');

const modelName = 'product';
const Schema = mongoose.Schema;
const ObjectId = mongoose.Schema.Types.ObjectId;
const constant = require('../utils/constant');


const MediaSchema = new Schema({
    name: {
        type: String,
        default: '',
       // required: true
    },
    url: {
        type: String,
        default: '',
        // required: true
    },
    type: {
        enum: constant.lookup.MEDIA_TYPE,
        // required: true,
        type: String
    },
}, { _id: false });

const PricingSchema = new Schema(
    {
        actualPrice: {
            type: Number,
            default: constant.NUMBER_ZERO
        },
        sellingPrice: {
            type: Number,
            default: constant.NUMBER_ZERO,
            required: true
        },
        gkbPrice: {
            type: Number,
            default: constant.NUMBER_ZERO
        },
        opticianPrice: {
            type: Number,
            default: constant.NUMBER_ZERO
        },
        discountPerc: {
            type: Number,
            default: constant.NUMBER_ZERO
        },
        afterDiscount: {
            type: Number,
            default: constant.NUMBER_ZERO
        },
    },
    { _id: false });


const ProductSchema = new Schema({
    name: {
        type: String,
        default: '',
        required: true
    },
    SKU: { type: String },
    productId: { type: String, unique: true },
    // cover_img: { type: MediaSchema 
    //     //required: true 
    // },
    // media: {  type: [ MediaSchema ] },
    brand: {
        // type: String,
        // ref: 'brand',
        // required: true,
        _id: { type: ObjectId, ref: 'brand' }, 
        name: { type: String},
        code: { type: String}
    },
    model: {
        _id: { type: ObjectId, ref: 'frameModel' }, 
        name: { type: String },
        code: { type: String }
    },
    description: { type: String, required: true },
    createdEcpCustId: {
        type: Schema.Types.ObjectId,
        ref: 'users',
        // required: true
    },
    createdFrameCollectorId: {
        type: Schema.Types.ObjectId,
        ref: 'users',
        // required: true
    },
    category: {
        _id: {
            type: ObjectId, required: true,
            type: Schema.Types.ObjectId,
            ref: 'category'
        },
        categoryId: { type: String, required: true },
        childCategories: {
            _id: {
                type: ObjectId, required: true,
                type: Schema.Types.ObjectId,
                ref: 'category', required: true
            },
            categoryId: { type: String, required: true },
        },
        // subCategory: {
        //     _id: {
        //         type: ObjectId,
        //         type: Schema.Types.ObjectId,
        //         ref: 'category'
        //     },
        //     categoryId: { type: String },
        // }
    },  
    pricing: {
        type: PricingSchema
    },
    // offers: [{ type: ObjectId, ref: 'ProductOfferModel'}],
    product_info: [{
        heading: { type: String },
        content: {
                key_name: { type: String }, key_value: { type: String }, media: { type: MediaSchema }
            }
    }],
    frame_attributes: [{
        key_name: { type: String }, key_value: { type: String }, digitCode: { type: String }
    }],
    // prdt_attributes: [{
    //     prdAttributeId: String,
    //     size: {
    //         prdSizeId: String,
    //         value: { type: String },
    //         SKU: { type: String },
    //         color: [{
    //             prdColorId : String,
    //             value: { type: String },
    //             quantity: { type: Number, default: 0 },
    //             SKU: { type: String },
    //             cover_img: { type: MediaSchema },
    //             media: { type: [MediaSchema] },
    //         }],
    //     },
    // },{_id:false}],
    prdt_attributes: [{
        prdAttributeId: String,
        colour: {            
            store: [{
                _id: { type: Schema.Types.ObjectId, ref: 'users', required: true },
                cust_code: { type: String, required: true },
                store_code: { type: String , required: true } ,
                tagName: { type: String }, // frameID + '-' + store_code  
                SKU: []               
            }],
            frameId: { type: String, trim: true, unique: true, required: true },
            prdColourId: String,
            _id: { type: Schema.Types.ObjectId, ref: 'frameColour', required: true },            
            name: { type: String, required: true },   // colour name
            value: { type: String , required: true}, // hexcode
            digitCode: { type: String, required: true }, // colour XMCode
            SKU: { type: String },
            isDefault: {type: Boolean, default: false},
            cover_img: { type: MediaSchema },
            media: { 
                type: [MediaSchema],
                validate: [productColourMediaLimit, '{PATH} exceeds the limit of 4']
            } ,
            size: [{
                digitCode: { type: String },
                prdSizeId : { type: String },
                value: { type: String },
                quantity: { type: Number, default: 0 },
                SKU: { type: String },
                url: { type: String },
                sizeValue: { type: String },
                isDefault: {type: Boolean, default: false},
                isSelected: {type: Boolean, default: false}
            }],
        },
    },{_id:false}],
    gender: {
        type: String,
        enum: constant.lookup.PRODUCT_GENDER_LOOKUP
    },
    badge: [{ label: { type: String }, url: { type: String } }],
    tags: [{ type: String }],
    view_count: { type: Number, default: 0 },
    rating: {
        totalCount: { type: Number, default: 0 },
        avgRating: { type: Number, default: 0.0 },
        oneStarCount: { type: Number, default: 0 },
        twoStarCount: { type: Number, default: 0 },
        threeStarCount: { type: Number, default: 0 },
        fourStarCount: { type: Number, default: 0 },
        fiveStarCount: { type: Number, default: 0 },
    },
    totalReviewCount: { type: Number, default: 0 },
    isPremium: { type: Boolean, default: false },
    isApproved: { type: Boolean, default: false },
    isActivate: { type: Boolean, default: true },
    isUnisex: { type: Boolean, default: false },
    auditFields: {
        type: baseSchema.AuditFieldSchema
    },
    permission:{
        isAllowedToCreatePrdt: { type: Boolean, default: false },
    }
});

function productColourMediaLimit(val) {
    return val.length <= 4;
}

ProductSchema.index({ tags: 'text', name: 'text', description: 'text' });

// Find one user by specific field
ProductSchema.statics.findUser = function (findBy, value) {
    return this.model(modelName).findOne({ [findBy]: value });
};

// This function will call before each save() so that we can set auditFields.
ProductSchema.pre('save', function (next) {
    // this.auditFields = { createdAt: new Date() };
    if (_.isNil(this.auditFields)) {
        this.auditFields = {};
    }
    this.auditFields.updatedAt = new Date();
    if (_.isNil(this.auditFields.createdAt)) {
        this.auditFields.createdAt = new Date();
    }
    if (_.isNil(this.auditFields.isActive)) {
        this.auditFields.isActive = true;
    }
    if (_.isNil(this.auditFields.isDeleted)) {
        this.auditFields.isDeleted = false;
    }
    next();
});


module.exports = mongoose.model(modelName, ProductSchema); //Compiling schema to model