const _ = require('lodash');
const mongoose = require('mongoose');
const baseSchema = require('./base-schema');
const validate = require('validator');

const constant = require('../utils/constant');
const modelName = 'users';
const Schema = mongoose.Schema;

// Create Schema objects and set validations
const userSchema = new Schema({
    name: { type: String, required: true, trim: true },
    mobile: { type: String, required: true, unique: true, trim: true },
    email: {
        type: String,
        unique: true,
        trim: true,
        required: true,
        validate: {
            validator: validate.isEmail,
            message: '{VALUE} is not a valid email'
        }
    },
    role: {
        type: String,
        required: true,
        enum: constant.lookup.ROLE,
        uppercase: true
    },
    otp: String,   // otp validity
    otpExpiresIn: Date,
    platform: { type: String, trim: true },  // IOS / ANDROID / WEB
    gender: { type: String, enum: constant.lookup.GENDER_LOOKUP },
    dob: Date,
    userId: { type: String, unique: true },
    address: { type: [baseSchema.AddressSchema] },
    media: {
        profile_img: String
    },
    fcmToken: String,
    ecpCust: { // from gkb DB for ECP_CUSTOMER ROLE
        _id: { type: Schema.Types.ObjectId },
        userId: { type: String }, 
        cust_code: { type: String },
        store: { type: Object }
    },
    store_code: { type: String, unique: true },
    // mystores: [{    // FRAME COLLECTOR ROLE
    //     _id: { type: Schema.Types.ObjectId, ref: 'users', unique: true },
    //     cust_code: { type: String, required: true },
    // }], 
    auditFields: {
        type: baseSchema.auditFields
    }
});

// Defining custom methods to schema

// Find one user by specific field
userSchema.statics.findUser = function (findBy, value) {
    return this.model(modelName).findOne({ [findBy]: value });
};

// Find user by specific field
userSchema.statics.findUsers = function (condition, limit, page, sortBy) {
    return this.model(modelName).find().exec();
};

// This function will call before each save() so that we can set auditFields.
userSchema.pre('save', function (next) {
    // console.log(this.auditFields);
    // this.auditFields = { createdAt: new Date() };
    if (_.isNil(this.auditFields)) {
        this.auditFields = {};
    }
    this.auditFields.updatedAt = new Date();
    if (_.isNil(this.auditFields.createdAt)) {
        this.auditFields.createdAt = new Date();
    }
    if (_.isNil(this.auditFields.isActive)) {
        this.auditFields.isActive = true;
    }
    if (_.isNil(this.auditFields.isDeleted)) {
        this.auditFields.isDeleted = false;
    }
    next();
});

module.exports = mongoose.model(modelName, userSchema); //Compiling schema to model