const _ = require('lodash');
const mongoose = require('mongoose');
const baseSchema = require('./base-schema');

const modelName = 'category';
const Schema = mongoose.Schema;
const constant = require('../utils/constant');


const MediaSchema = new Schema({
    name: {
        type: String,
        default: '',
        required: true
    },
    url: {
        type: String,
        default: '',
        required: true
    },
    type: {
        enum: constant.lookup.MEDIA_TYPE,
        required: true,
        type: String
    },
});

const CategorySchema = new Schema({
    name: { type: String, trim: true, required: true },
    title: { type: String },
    categoryId: { type: String},
    path: {type: String,trim: true},
    isRoot: {
        type: Boolean,
        default: false
    },
    code: { type: String, unique: true },
    childCategories: [
        { 
            _id: {
                type: Schema.Types.ObjectId,
                ref: 'Category'
            },
            parentRootCategory: {
                _id: { type: Schema.Types.ObjectId, ref: 'Category' },
                categoryId: { type: String },
                name:{ type: String },
                path: {type: String,trim: true},
            },
        } 
    ],
    bannerImages: { type: MediaSchema },
    auditFields: {
        type: baseSchema.AuditFieldSchema
    }
});

// This function will call before each save() so that we can set auditFields.
CategorySchema.pre('save', function (next) {
    // console.log(this.auditFields);
    // this.auditFields = { createdAt: new Date() };
    if (_.isNil(this.auditFields)) {
        this.auditFields = {};
    }
    this.auditFields.updatedAt = new Date();
    if (_.isNil(this.auditFields.createdAt)) {
        this.auditFields.createdAt = new Date();
    }
    if (_.isNil(this.auditFields.isActive)) {
        this.auditFields.isActive = true;
    }
    if (_.isNil(this.auditFields.isDeleted)) {
        this.auditFields.isDeleted = false;
    }
    next();
});


module.exports = mongoose.model(modelName, CategorySchema); //Compiling schema to model
