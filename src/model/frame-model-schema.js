const _ = require('lodash');
const mongoose = require('mongoose');
const baseSchema = require('./base-schema');
const constant = require('../utils/constant');

const modelName = 'frameModel';
const Schema = mongoose.Schema;

const frameModel = new Schema({
    name: { type: String, required: true, unique: true },
    modelCode: { type: String, required: true, unique: true },
    code: { type: String, required: true, unique: true },
    brand: {
        _id: { type: String, ref: 'brand', required: true },
        brandId: { type: String, required: true }
    },
    auditFields: {
        type: baseSchema.AuditFieldSchema
    },
    createdAt: Date
});

// This function will call before each save() so that we can set auditFields.
frameModel.pre('save', function (next) {
    // console.log(this.auditFields);
    // this.auditFields = { createdAt: new Date() };
    if (_.isNil(this.auditFields)) {
        this.auditFields = {};
    }
    this.auditFields.updatedAt = new Date();
    if (_.isNil(this.auditFields.createdAt)) {
        this.auditFields.createdAt = new Date();
    }
    if (_.isNil(this.auditFields.isActive)) {
        this.auditFields.isActive = true;
    }
    if (_.isNil(this.auditFields.isDeleted)) {
        this.auditFields.isDeleted = false;
    }
    next();
});


module.exports = mongoose.model(modelName, frameModel); //Compiling schema to model