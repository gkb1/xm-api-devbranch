const _ = require('lodash');
const mongoose = require('mongoose');
const baseSchema = require('./base-schema');
const validate = require('validator');

const modelName = 'cart';
const Schema = mongoose.Schema;
const ObjectId = mongoose.Schema.Types.ObjectId;
const constant = require('../utils/constant');

const MediaSchema = new Schema({
    name: {
        type: String,
        default: '',
        required: true
    },
    url: {
        type: String,
        default: '',
        required: true
    },
    type: {
        enum: constant.lookup.MEDIA_TYPE,
        required: true,
        type: String
    },
}, { _id: false });

// const CartSchema = new Schema({
//     products: [{
//         productId: { type: Schema.Types.ObjectId, ref: 'product', required: true, unique: true },
//         productAttr: { type: String, required: true },
//         quantity: { type: Number, required: true, default: 1 },
//     }, { _id: false }],
//     // products: [
//     //     {
//     //         type: Schema.Types.ObjectId,
//     //         ref: 'product',
//     //         unique: true,
//     //         required: true
//     //     },
//     //     { productAttr: { type: String, required: true} },
//     //     { quantity: {type: Number, required: true, default: 1} },
//     // ],
//     userId: {
//         type: Schema.Types.ObjectId,
//         ref: 'users',
//         required: true
//     },
//     cartId: { type: String, unique: true },
//     auditFields: {
//         type: baseSchema.AuditFieldSchema
//     }
// });


const UserCartSchema = new Schema({
    userId: {
        type: Schema.Types.ObjectId,
        ref: 'users',
        required: true
    },
    cartId: { type: String, unique: true },
    auditFields: {
        type: baseSchema.AuditFieldSchema
    },
    products: [{
        productId: { type: Schema.Types.ObjectId, ref: 'product', required: true },
        prdtAttr: {
            prdtAttrId: { type: String, required: true },
            prdColourId: { type: String, required: true }, 
            prdSizeId: { type: String, required: true }, 
        },
        quantity: { type: Number, required: true, default: 1 },
        storeId: { type: Schema.Types.ObjectId, ref: 'users', required: true },
        voucher: {
            code: { type: String, required: true },
            validity: {type: Date },
            mediia: { type: MediaSchema }
        }
    }, { _id: false }]
});

// This function will call before each save() so that we can set auditFields.
UserCartSchema.pre('save', function (next) {
    // console.log(this.auditFields);
    // this.auditFields = { createdAt: new Date() };
    if (_.isNil(this.auditFields)) {
        this.auditFields = {};
    }
    this.auditFields.updatedAt = new Date();
    if (_.isNil(this.auditFields.createdAt)) {
        this.auditFields.createdAt = new Date();
    }
    if (_.isNil(this.auditFields.isActive)) {
        this.auditFields.isActive = true;
    }
    if (_.isNil(this.auditFields.isDeleted)) {
        this.auditFields.isDeleted = false;
    }
    next();
});


module.exports = mongoose.model(modelName, UserCartSchema); //Compiling schema to model