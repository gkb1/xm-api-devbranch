const _ = require('lodash');
const mongoose = require('mongoose');
const baseSchema = require('./base-schema');
const constant = require('../utils/constant');

const modelName = 'frameColour';
const Schema = mongoose.Schema;

const frameColour = new Schema({
    actualColour: { type: String, required: true, trim: true, unique: true},
    parentColour: { type: String,
        required: true,
        enum: constant.lookup.COLOUR_LOOKUP,
    },
    hexCode: { type: String , required: true, unique: true },
    XMCode: { type: String , required: true, unique: true },
    auditFields: {
        type: baseSchema.AuditFieldSchema
    },
    createdAt: Date
});

// This function will call before each save() so that we can set auditFields.
frameColour.pre('save', function (next) {
    // console.log(this.auditFields);
    // this.auditFields = { createdAt: new Date() };
    if (_.isNil(this.auditFields)) {
        this.auditFields = {};
    }
    this.auditFields.updatedAt = new Date();
    if (_.isNil(this.auditFields.createdAt)) {
        this.auditFields.createdAt = new Date();
    }
    if (_.isNil(this.auditFields.isActive)) {
        this.auditFields.isActive = true;
    }
    if (_.isNil(this.auditFields.isDeleted)) {
        this.auditFields.isDeleted = false;
    }
    next();
});


module.exports = mongoose.model(modelName, frameColour); //Compiling schema to model