const _ = require('lodash');
const mongoose = require('mongoose');
const baseSchema = require('./base-schema');
const constant = require('../utils/constant');

const modelName = 'frameType';
const Schema = mongoose.Schema;

const MediaSchema = new Schema({
    name: {
        type: String,
        default: '',
        required: true
    },
    url: {
        type: String,
        default: '',
        required: true
    },
    type: {
        enum: constant.lookup.MEDIA_TYPE,
        required: true,
        type: String
    },
});

const frameType = new Schema({
    name: { type: String, required: true, trim: true, unique: true},
    code: { type: String , required: true , unique: true},
    media: { type: MediaSchema },
    auditFields: {
        type: baseSchema.AuditFieldSchema
    },
    createdAt: Date
});

// This function will call before each save() so that we can set auditFields.
frameType.pre('save', function (next) {
    // console.log(this.auditFields);
    // this.auditFields = { createdAt: new Date() };
    if (_.isNil(this.auditFields)) {
        this.auditFields = {};
    }
    this.auditFields.updatedAt = new Date();
    if (_.isNil(this.auditFields.createdAt)) {
        this.auditFields.createdAt = new Date();
    }
    if (_.isNil(this.auditFields.isActive)) {
        this.auditFields.isActive = true;
    }
    if (_.isNil(this.auditFields.isDeleted)) {
        this.auditFields.isDeleted = false;
    }
    next();
});


module.exports = mongoose.model(modelName, frameType); //Compiling schema to model