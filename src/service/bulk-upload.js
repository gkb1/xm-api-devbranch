const _ = require('lodash');
const mongo = require('mongodb');
const constant = require('./../utils/constant');
const res = require('../utils/custom-response');
const studentDAO = require('../dao/student-dao');
const userDAO = require('../dao/user-dao');
const QRCode = require('qrcode');
const path = require('path');
const communication = require('../utils/communication')
const fs = require("fs");
const pdf = require('html-pdf');
const handlebars = require('handlebars');
// var html = fs.readFileSync(path.join(__dirname + '/../views/abcQr.html'), 'utf8');
const studentService = require('./student-service');
const utils = require('./../utils/utilities');
const utility = require('util');

// const communicationHelper = require('./../utils/communication_helper');
// const communicationUtility = require('./../utils/communication');

const bulkUploadService = {


    async uploadStudent(usersData) {
        try {
            const results = [];
            for (const userObj of usersData) {
                const ss = {};
                if (userObj.user.role === '5c50727ad6031c58edc6a6af') {
                    const _user = {
                        name: userObj.user.name,
                        phone: userObj.user.phone,
                        username: userObj.user.username,
                        password: userObj.user.password.passwordHash,
                        location: userObj.user.location._id,
                        role: 'STUDENT',
                        email: userObj.user.email
                    };
                    const savedUser = await userDAO.create(_user);
                    ss['user'] = savedUser;
                    let _student = {
                        batch: userObj.batch._id,
                        user: savedUser._id,
                        invoiceNumber: userObj.invoiceNumber,
                        profileImage: '/files/' + userObj.profileImage.filename,
                        aadharCard: userObj.files[1] ? '/files/' + userObj.files[1].filename : '',
                        resume: userObj.files[0] ? '/files/' + userObj.files[0].filename : ''
                    };
                    _student = Object.assign(_student, userObj.details);
                    const savedStudent = await studentDAO.addStudentDetails(_student);
                    const updateStudent = await studentService.setYOPAndGAPOfStudent(savedStudent._id);
                    ss['student'] = savedStudent;
                }
                results.push(ss);
            }
            return Promise.resolve(results);
        } catch (error) {
            return Promise.reject(res.error(constant.HTML_STATUS_CODE.INTERNAL_ERROR, error.message, error.stack));
        }

    }

};


module.exports = bulkUploadService;