
const _ = require('lodash');
const constant = require('../utils/constant');
const res = require('../utils/custom-response');
const utils = require('../utils/utilities');
const wishlistDAO = require('../dao/wishlist-dao');

const wishlistService = {
    async createWishlist(req, user) {
        try {
            if (req == null) {
                return Promise.reject(res.error(constant.HTML_STATUS_CODE.INVALID_url, 'Invalid url!.'));
            }
            req.body['wishlistId'] = utils.getRandomString(6);
            req.body['userId'] = user._id;
            let checkUnique = await wishlistDAO.getOne({ "wishlistId": req['wishlistId'] });
            while (checkUnique) {
                req.body['wishlistId'] = utils.getRandomString(6);
                checkUnique = await wishlistDAO.getOne({ "wishlistId": req['wishlistId'] });
            }
            const result = await wishlistDAO.create(req.body);
            return Promise.resolve(result);
        } catch (error) {
            return Promise.reject(res.error(constant.HTML_STATUS_CODE.INTERNAL_ERROR, error.message, error.stack));
        }
    },
    async getWishlistItemById(loggedInUser, wishlistId) {
        try {
            if (loggedInUser == null || wishlistId == null) {
                return Promise.reject(res.error(constant.HTML_STATUS_CODE.INVALID_url, 'Invalid url!.'));
            }
            const result = await wishlistDAO.getOne({ wishlistId: wishlistId });
            if (result == null) {
                return Promise.reject(res.error(constant.HTML_STATUS_CODE.INVALID_url, `No wishlist found.`));
            }
            return result;
        } catch (error) {
            return Promise.reject(res.error(constant.HTML_STATUS_CODE.INTERNAL_ERROR, error.message, error.stack));
        }
    },
    async listAllWishlistItems(wishlistData) {
        try {
            if (wishlistData == null) {
                return Promise.reject(res.error(constant.HTML_STATUS_CODE.INVALID_url, 'Invalid url!.'));
            }
            let limit;
            let skip;
            let page;
            let pagination = wishlistData.pagination;
            if (pagination) {
                limit = isNaN(Number(pagination.limit)) ? 10 : Number(pagination.limit);
                page = Number(pagination.page) || 1;
                skip = (pagination.page ? pagination.limit * (pagination.page - 1) : 0);
            } else {
                limit = 30; // default
                skip = 0;
            }
            wishlistData.conditions['auditFields.isDeleted'] = false;
            const result = await wishlistDAO.getAll(limit, skip, wishlistData.sort, wishlistData.conditions);
            return result;
        } catch (error) {
            return Promise.reject(res.error(constant.HTML_STATUS_CODE.INTERNAL_ERROR, error.message, error.stack));
        }
    },
    // async updateWishlist(params, prodDetails) {
    //     try {
    //         if (prodDetails == null || params == null) {
    //             return Promise.reject(res.error(constant.HTML_STATUS_CODE.INVALID_DATA, 'Invalid url!.'));
    //         }
    //         const objId = await wishlistDAO.getOne({ wishlistId: params.wishlistId });
    //         if (objId == null) {
    //             return Promise.reject(res.error(constant.HTML_STATUS_CODE.INVALID_DATA, `No wishlist found.`));
    //         }
    //         const result = await wishlistDAO.update({ wishlistId: params.wishlistId, wishlistId: params.wishlistId }, prodDetails);
    //         return result;
    //     } catch (error) {
    //         return Promise.reject(res.error(constant.HTML_STATUS_CODE.INTERNAL_ERROR, error.message, error.stack));
    //     }
    // },

    async deleteWishlist(wishlistData, user) {
        try {
            if (wishlistData == null) {
                return Promise.reject(res.error(constant.HTML_STATUS_CODE.INVALID_DATA, 'Invalid url!.'));
            }
            const objId = await wishlistDAO.getOne({ wishlistId: wishlistData.wishlistId });
            if (objId == null) {
                return Promise.reject(res.error(constant.HTML_STATUS_CODE.INVALID_DATA, `No wishlist found.`));
            }
            const result = await wishlistDAO.removeItemFormWishlist(wishlistData);
            return result;
        } catch (error) {
            return Promise.reject(res.error(constant.HTML_STATUS_CODE.INTERNAL_ERROR, error.message, error.stack));
        }
    },
};


module.exports = wishlistService;