const constant = require('./../utils/constant');
const jwt = require('jsonwebtoken');
const userDAO = require('./../dao/user-dao');
const customResponse = require('./../utils/custom-response');

async function isAuthenticate(req, res, next) {
    try {        
        if(!req.headers.authorization) {
            throw customResponse.error(constant.HTML_STATUS_CODE.UNAUTHORIZED, 'Access denied!');
        }
        const token = req.headers.authorization.replace("Bearer ", "");
        if (token == null) {
            throw customResponse.error(constant.HTML_STATUS_CODE.UNAUTHORIZED, 'Token not found in request');
        }
        // Verify token with APP_SECRET
        const userDetail = jwt.verify(token, constant.APP_SECRETE);
        if (userDetail == null) {
            throw customResponse.error(constant.HTML_STATUS_CODE.UNAUTHORIZED, 'Invalid Token');
        }
        // check user data with database if user is valid or not...
        // We can use redis db to check the user data...
        let user;
        user = await userDAO.getById(userDetail._id);
        if (user == null) {
            throw customResponse.error(constant.HTML_STATUS_CODE.UNAUTHORIZED, 'Invalid Token');
        }
        // Add userDetail in req.user so that we can use user detail in future
        req.user = user;
        next();
    } catch (error) {
        if (error.name && error.name.indexOf('TokenExpiredError') > -1) {
            res.status(constant.HTML_STATUS_CODE.UNAUTHORIZED).json(error);   
        } else {
            res.status(error.status || constant.HTML_STATUS_CODE.INTERNAL_ERROR).json(error);
        }
    }
}

module.exports = isAuthenticate;