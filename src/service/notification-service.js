const _ = require('lodash');
const constant = require('./../utils/constant');
const res = require('../utils/custom-response');
const utils = require('./../utils/utilities');
const request = require('request');
const userDAO = require('../dao/user-dao');
var shortid = require('shortid');
const FCMNotificationService = require('./fcm-notification-service');
const moment = require ('moment');

const NotificationService = {
    async sendNotificationToUsers(bodyData, user) {
        try {
            if (!user) {
                return Promise.reject(res.error(constant.HTML_STATUS_CODE.INVALID_DATA, 'Invalid User'));
            }
            if (user.role !== constant.lookup.ADMIN_ROLE && user.role !== constant.lookup.CUST_SUPPORT_ROLE) {
                return Promise.reject(res.error(constant.HTML_STATUS_CODE.UNAUTHORIZED, 'Unauthorized Access.'));
            }
            if (!bodyData.title || !bodyData.body) {
                return Promise.reject(res.error(constant.HTML_STATUS_CODE.INVALID_DATA, 'Invalid Payload'));
            }
            if (!bodyData.conditions) {
                return Promise.reject(res.error(constant.HTML_STATUS_CODE.INVALID_DATA, 'Invalid Conditions'));
            }
            bodyData.conditions['fcmToken'] = {$exists: true, $ne: null };
            const usersList = await userDAO.getAllUsersByCondn(bodyData.conditions);
            if (usersList && usersList.length > 0) {
                usersList.forEach(async user => {
                    if (user.fcmToken) {
                        var today = new Date();
                        var _dateTime = moment(today).format('DD-MMM-YYYY hh:mm');
                        var notification = {
                            nId: shortid.generate(),
                            // icon: Constants.system.server.url2 + '/assets/common/logo.png',
                            title: bodyData.title ? bodyData.title : '',
                            body:  bodyData.body ? bodyData.body : '',
                            dateTime: _dateTime,
                            url: bodyData.url ? bodyData.url : ''
                        }
                        const fcmPayload = {
                            payload: {
                                notification: notification
                            },
                            options: {
                                priority: "high",
                            },
                            token: user.fcmToken
                        };
                        FCMNotificationService.sendNotification(fcmPayload).then(async (res, err) => {
                            if (err) {
                                console.log('Unable to send notification !!', err);
                            }else {
                                if (res.failureCount > 0) {
                                    console.log('Unable to send notification for '+user.email+'!!', res);
                                }
                                if (res.successCount > 0) {
                                    const updateUser = await userDAO.updateByCondn({userId: user.userId },{$addToSet : { notificationList : notification}});                    
                                    if (updateUser.nModified !== 1) {  //PENDING delete
                                    }   
                                }
                            }
                        });              
                    }
                });
                return Promise.resolve(true); 
            } else {
                return Promise.reject(res.error(constant.HTML_STATUS_CODE.INTERNAL_ERROR, 'No Records Found !!'));
            } 
        } catch (error) {
            return Promise.reject(res.error(constant.HTML_STATUS_CODE.INTERNAL_ERROR, error.message, error.stack));
        }
    },
}
module.exports = NotificationService;