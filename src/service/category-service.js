const _ = require('lodash');
const constant = require('./../utils/constant');
const res = require('../utils/custom-response');
const utils = require('./../utils/utilities');
const request = require('request');
const categoryDAO = require('../dao/category-dao');
var shortid = require('shortid');
const moment = require('moment');
const async = require('async');

const CategoryService = {
    async createCategory(bodyData, user) {
        try {
            if (!user) {
                return Promise.reject(res.error(constant.HTML_STATUS_CODE.INVALID_DATA, 'Invalid User'));
            }
            if (user.role !== constant.lookup.SUPERADMIN_ROLE && user.role !== constant.lookup.ADMIN_ROLE) {
                return Promise.reject(res.error(constant.HTML_STATUS_CODE.UNAUTHORIZED, 'Unauthorized Access.'));
            }
            if (!bodyData.name) {
                return Promise.reject(res.error(constant.HTML_STATUS_CODE.INVALID_DATA, 'Invalid Payload'));
            }
            if (!bodyData.childCategories || bodyData.childCategories.length <= 0) {
                return Promise.reject(res.error(constant.HTML_STATUS_CODE.INVALID_DATA, 'Please add atleast one child category !!'));
            }
            const catExist = await categoryDAO.getOne({ 'name': bodyData.name, 'isRoot': true });
            if (catExist) {
                return Promise.reject(res.error(constant.HTML_STATUS_CODE.CONFLICT, 'Category ' + bodyData.name + ` already Exists !!`));
            }
            // var childSubCatPayload = [];
            // _.forEach(createCategory.childCategories, async childCategory => {
            //     // update all child categories parent with  main parent ID, name
            //     childCategory.path = bodyData.name.toLowerCase()+'_'+childCategory.name.toLowerCase();
            //     childCategory.parentRootCategory = { '_id' : createCategory._id,'name' : createCategory.name};
            //     // update all sub child categories parent id with child parent id
            //     var childCatList = childCategory;
            //     if (childCatList.hasOwnProperty('subCategory') && childCatList.subCategory.length > 0) {
            //         _.forEach(childCatList.subCategory, childSubCat => {
            //             childSubCat.parentRootCategory = { '_id' : createCategory._id,'name' : createCategory.name}; 
            //             childSubCat.path = bodyData.name.toLowerCase()+'_'+childCategory.name.toLowerCase()+'_'+childSubCat.name.toLowerCase();                       
            //         });                               
            //     }
            //     const updateChildCat = await categoryDAO.updateCategory({'categoryId': createCategory.categoryId, 'childCategories._id':childCategory._id  }, {$set: {'childCategories.$' : childCategory}}); 
            // });     

            var childCatIdList = [];
            return new Promise((resolve, reject) => {
                var iteratorFcn = async function (childCategory) {
                    childCategory['categoryId'] = utils.getRandomString(6);
                    let checkUnique = await categoryDAO.getOne({ "childCategory.categoryId": childCategory['categoryId'] });
                    while (checkUnique) {
                        childCategory['categoryId'] = utils.getRandomString(6);
                        checkUnique = await categoryDAO.getOne({ "childCategory.categoryId": childCategory['categoryId'] });
                    }
                    childCategory.path = bodyData.name.toLowerCase() + '_' + childCategory.name.toLowerCase();

                    const checkIfChildCategoryExist = await categoryDAO.getOne({ 'path': childCategory.path, 'childCategories.name': childCategory.name });
                    if (checkIfChildCategoryExist) {
                        return reject(res.error(constant.HTML_STATUS_CODE.CONFLICT, 'Child Category ' + childCategory.name + ` already Exists !!`));
                    }
                    const createChildCategory = await categoryDAO.createCategory(childCategory);
                    if (createChildCategory) {
                        childCatIdList.push({ _id: createChildCategory._id });
                    }
                };
                var doneIteratingFcn = async function (err, output) {
                    if (err) {
                        return reject(res.error(constant.HTML_STATUS_CODE.INTERNAL_ERROR, err));
                    }
                    if (childCatIdList.length > 0) {
                        bodyData['childCategories'] = childCatIdList;
                        bodyData['isRoot'] = true;
                        bodyData['categoryId'] = utils.getRandomString(6);
                        let checkUnique = await categoryDAO.getOne({ "categoryId": bodyData['categoryId'] });
                        while (checkUnique) {
                            bodyData['categoryId'] = utils.getRandomString(6);
                            checkUnique = await categoryDAO.getOne({ "categoryId": bodyData['categoryId'] });
                        }
                        bodyData['path'] = bodyData.name.toLowerCase();
                        const createCategory = await categoryDAO.createCategory(bodyData);
                        if (createCategory == null) {
                            return reject(res.error(constant.HTML_STATUS_CODE.INTERNAL_ERROR, 'Something went wrong with Creating Category !!'));
                        }
                    }
                    return resolve(true);
                }
                async.forEach(bodyData.childCategories, iteratorFcn, doneIteratingFcn);
            });
            return Promise.resolve(true)
        } catch (error) {
            return Promise.reject(res.error(constant.HTML_STATUS_CODE.INTERNAL_ERROR, error.message, error.stack));
        }
    },
    async createChildCat(bodyData, user) {
        try {
            if (!user) {
                return Promise.reject(res.error(constant.HTML_STATUS_CODE.INVALID_DATA, 'Invalid User'));
            }
            if (user.role !== constant.lookup.SUPERADMIN_ROLE && user.role !== constant.lookup.ADMIN_ROLE) {
                return Promise.reject(res.error(constant.HTML_STATUS_CODE.UNAUTHORIZED, 'Unauthorized Access.'));
            }
            if (!bodyData.categoryId) {
                return Promise.reject(res.error(constant.HTML_STATUS_CODE.INVALID_DATA, 'Invalid Payload'));
            }
            if (!bodyData.childCategories || bodyData.childCategories.length <= 0) {
                return Promise.reject(res.error(constant.HTML_STATUS_CODE.INVALID_DATA, 'Please add atleast one child category !!'));
            }
            const getCategory = await categoryDAO.getOne({ isRoot: true, 'categoryId': bodyData.categoryId });
            if (!getCategory) {
                return Promise.reject(res.error(constant.HTML_STATUS_CODE.INVALID_DATA, 'Invalid Category'));
            }
            var childCatIdList = [];
            return new Promise((resolve, reject) => {
                var iteratorFcn = async function (childCategory) {
                    childCategory['categoryId'] = utils.getRandomString(6);
                    let checkUnique = await categoryDAO.getOne({ "childCategory.categoryId": childCategory['categoryId'] });
                    while (checkUnique) {
                        childCategory['categoryId'] = utils.getRandomString(6);
                        checkUnique = await categoryDAO.getOne({ "childCategory.categoryId": childCategory['categoryId'] });
                    }
                    childCategory.path = getCategory.name.toLowerCase() + '_' + childCategory.name.toLowerCase();

                    const checkIfChildCategoryExist = await categoryDAO.getOne({ 'path': childCategory.path, 'name': childCategory.name });
                    if (checkIfChildCategoryExist) {
                        return reject(res.error(constant.HTML_STATUS_CODE.CONFLICT, 'Child Category ' + childCategory.name + ` already Exists !!`));
                    }
                    const createChildCategory = await categoryDAO.createCategory(childCategory);
                    if (createChildCategory) {
                        childCatIdList.push({ _id: createChildCategory._id });
                    }
                };
                var doneIteratingFcn = async function (err, output) {
                    if (err) {
                        return reject(res.error(constant.HTML_STATUS_CODE.INTERNAL_ERROR, err));
                    }
                    if (childCatIdList.length > 0) {
                        const updateChildCat = await categoryDAO.updateCategory({ 'categoryId': bodyData.categoryId }, { $addToSet: { 'childCategories': childCatIdList } });
                        if (!updateChildCat || updateChildCat.nModified !== 1) {
                            return reject(res.error(constant.HTML_STATUS_CODE.INVALID_DATA, 'Unable to create child category !!'));
                        }
                    }
                    return resolve(true);
                }
                async.forEach(bodyData.childCategories, iteratorFcn, doneIteratingFcn);
            });
            return Promise.resolve(true);
        } catch (error) {
            return Promise.reject(res.error(constant.HTML_STATUS_CODE.INTERNAL_ERROR, error.message, error.stack));
        }
    },
    async getAllCategories(bodyData, user) {
        try {
            if (!bodyData.conditions) {
                bodyData.conditions = {};
            }
            if (bodyData.conditions.name) {
                bodyData.conditions.name = {
                    $regex: new RegExp("^" + bodyData.conditions.name + "$", "i")
                };
            }
            if (bodyData.conditions.catNameList) {
                bodyData.conditions.name = bodyData.conditions.catNameList;
                delete bodyData.conditions.catNameList;
            }
            bodyData.conditions['isRoot'] = true; // root category with child categories
            const data = await categoryDAO.getAllByPageAndCond(bodyData.conditions);
            return Promise.resolve(Object.assign(data));
        } catch (error) {
            return Promise.reject(res.error(constant.HTML_STATUS_CODE.INTERNAL_ERROR, error.message, error.stack));
        }
    },
    async getAllCategoriesWithTrendingCat(bodyData, user) {
        try {
            if (!bodyData.conditions) {
                bodyData.conditions = {};
            }
            if (bodyData.conditions.name) {
                bodyData.conditions.name = {
                    $regex: new RegExp("^" + bodyData.conditions.name + "$", "i")
                };
            }
            if (bodyData.conditions.catNameList) {
                bodyData.conditions.name = bodyData.conditions.catNameList;
                delete bodyData.conditions.catNameList;
            }
            bodyData.conditions['isRoot'] = true; // root category with child categories
            const result = await categoryDAO.getAllByPageAndCond(bodyData.conditions);
            if (result && result.data.length > 0) {
                _.forEach(result.data, cat=> {
                    cat.childCategories.push({
                        '_id':{
                            path: cat.name.toLowerCase() + '_' + 'trending',
                            name: 'Trending',
                            bannerImages: {
                                "name": "trending.png",
                                "type": "Image",
                                "url": "https://xm-customer.s3.ap-south-1.amazonaws.com/images/1575463655300Rgwumusc.png"
                            }
                        } 
                    });
                });
            }
            return Promise.resolve(Object.assign(result));
        } catch (error) {
            return Promise.reject(res.error(constant.HTML_STATUS_CODE.INTERNAL_ERROR, error.message, error.stack));
        }
    },

    getByCategoryId(categoryId, user) {
        try {
            if (!categoryId) {
                return Promise.reject(res.error(constant.HTML_STATUS_CODE.INVALID_DATA, 'Invalid Category Id'));
            }
            // if (!user) {
            //     return Promise.reject(res.error(constant.HTML_STATUS_CODE.INVALID_DATA, 'Invalid User'));
            // }
            return Promise.resolve(categoryDAO.getOne({ categoryId: categoryId }));
        } catch (error) {
            return Promise.reject(res.error(constant.HTML_STATUS_CODE.INTERNAL_ERROR, error.message, error.stack));
        }
    },
    getByCategoryName(categoryName, user) {
        try {
            if (!categoryName) {
                return Promise.reject(res.error(constant.HTML_STATUS_CODE.INVALID_DATA, 'Invalid Category Name'));
            }
            // if (!user) {
            //     return Promise.reject(res.error(constant.HTML_STATUS_CODE.INVALID_DATA, 'Invalid User'));
            // }
            return Promise.resolve(categoryDAO.getOne({ name: { $regex: new RegExp(categoryName, "ig") } }));
        } catch (error) {
            return Promise.reject(res.error(constant.HTML_STATUS_CODE.INTERNAL_ERROR, error.message, error.stack));
        }
    },
    async updateCategory(categoryId, updateDetails, user) {
        try {
            if (!user) {
                return Promise.reject(res.error(constant.HTML_STATUS_CODE.INVALID_DATA, 'Invalid User'));
            }
            if (user.role !== constant.lookup.SUPERADMIN_ROLE && user.role !== constant.lookup.ADMIN_ROLE) {
                return Promise.reject(res.error(constant.HTML_STATUS_CODE.UNAUTHORIZED, 'Unauthorized Access.'));
            }
            if (!categoryId) {
                return Promise.reject(res.error(constant.HTML_STATUS_CODE.INVALID_DATA, 'Invalid Category'));
            }
            const getCategory = await categoryDAO.getOne({isRoot: true, 'categoryId': categoryId });
            if (!getCategory) {
                return Promise.reject(res.error(constant.HTML_STATUS_CODE.INVALID_DATA, 'Invalid Category'));
            }
            let childCatIdList = [];
            if (updateDetails.name) { // update Root Cat Name
                const updateRootChildCat = await categoryDAO.updateCategory({ 'categoryId': categoryId }, { name: updateDetails.name  });
                if (!updateRootChildCat || updateRootChildCat.nModified !== 1) {
                    return reject(res.error(constant.HTML_STATUS_CODE.INVALID_DATA, 'Unable to create child category !!'));
                }
            }
            // insert or update childCategories
            if (updateDetails.childCategories && updateDetails.childCategories.length > 0) {
                return new Promise((resolve, reject) => {
                    var iteratorFcn = async function (childCategory) {
                        if (childCategory._id) { // update
                            const updateChildCat = await categoryDAO.updateCategory({ '_id': childCategory._id }, { $set: { 'name': childCategory.name } });
                        } else { //create
                            childCategory['categoryId'] = utils.getRandomString(6);
                            let checkUnique = await categoryDAO.getOne({ "childCategory.categoryId": childCategory['categoryId'] });
                            while (checkUnique) {
                                childCategory['categoryId'] = utils.getRandomString(6);
                                checkUnique = await categoryDAO.getOne({ "childCategory.categoryId": childCategory['categoryId'] });
                            }
                            childCategory.path = getCategory.name.toLowerCase() + '_' + childCategory.name.toLowerCase();
                            const checkIfChildCategoryExist = await categoryDAO.getOne({ _id: childCategory._id });
                            if (checkIfChildCategoryExist) {
                                return reject(res.error(constant.HTML_STATUS_CODE.CONFLICT, 'Child Category ' + childCategory.name + ` already Exists !!`));
                            } 
                            const createChildCategory = await categoryDAO.createCategory(childCategory);
                            if (createChildCategory) {
                                childCatIdList.push({ _id: createChildCategory._id });
                            }  
                        }
                    };
                    var doneIteratingFcn = async function (err, output) {
                        if (err) {
                            return reject(res.error(constant.HTML_STATUS_CODE.INTERNAL_ERROR, err));
                        }
                        if (childCatIdList.length > 0) {
                            const updateRootChildCat = await categoryDAO.updateCategory({ 'categoryId': categoryId }, { $push: { 'childCategories': childCatIdList } });
                            if (!updateRootChildCat || updateRootChildCat.nModified !== 1) {
                                return reject(res.error(constant.HTML_STATUS_CODE.INVALID_DATA, 'Unable to create child category !!'));
                            }
                        }
                        return resolve(true);
                    }
                    async.forEach(updateDetails.childCategories, iteratorFcn, doneIteratingFcn);
                });
            }
            return Promise.resolve(true);
        } catch (error) {
            return Promise.reject(res.error(constant.HTML_STATUS_CODE.INTERNAL_ERROR, error.message, error.stack));
        }
    },
    // getSelCategoryByCond(bodyData, user) {
    //     try {
    //         if (!bodyData.categoryId && !bodyData.catName) {
    //             return Promise.reject(res.error(constant.HTML_STATUS_CODE.INVALID_DATA, 'Invalid Category'));
    //         }
    //         // if (!user) {
    //         //     return Promise.reject(res.error(constant.HTML_STATUS_CODE.INVALID_DATA, 'Invalid User'));
    //         // }
    //         const singleObjReturn = {};
    //         let conditions = {};
    //         if (bodyData.categoryId) {
    //             conditions['categoryId'] = bodyData.categoryId;
    //         }
    //         if (bodyData.catName) {
    //             conditions['name'] = {$regex: new RegExp(bodyData.catName, "ig") };
    //         }
    //         if (bodyData.childCatName) {
    //             conditions['childCategories.name'] = {$regex: new RegExp(bodyData.childCatName, "ig") };
    //             singleObjReturn['childCategories.$'] = 1;
    //         }
    //         if (bodyData.childSubCatName) {
    //             conditions['childCategories.subCategory.name'] =  {$regex: new RegExp(bodyData.childSubCatName, "ig") };
    //             singleObjReturn['childCategories.subCategory.$'] = 1;
    //         }
    //         if (bodyData.childCatId) {
    //             conditions['childCategories._id'] = bodyData.childCatId;
    //             singleObjReturn['childCategories.$'] = 1;
    //         }
    //         if (bodyData.childSubCatId) {
    //             conditions['childCategories.subCategory.name'] =  bodyData.childSubCatId;
    //             singleObjReturn['childCategories.subCategory.$'] = 1;
    //         }
    //         return Promise.resolve(categoryDAO.getOne(conditions,singleObjReturn ));
    //     } catch (error) {
    //         return Promise.reject(res.error(constant.HTML_STATUS_CODE.INTERNAL_ERROR, error.message, error.stack));
    //     }
    // },
}
module.exports = CategoryService;
