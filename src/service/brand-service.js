const _ = require('lodash');
const constant = require('./../utils/constant');
const res = require('../utils/custom-response');
const utils = require('./../utils/utilities');
const brandDAO = require('../dao/brand-dao');
const categoryDAO = require('../dao/category-dao');
const alphanuminc = require('alphanum-increment');
const increment = alphanuminc.increment;

const BrandService = {
    async createBrand(bodyData, user) {
        try {
            if (!user) {
                return Promise.reject(res.error(constant.HTML_STATUS_CODE.INVALID_DATA, 'Invalid User'));
            }
            if (user.role !== constant.lookup.FRAME_COLLECTOR_ROLE && user.role !== constant.lookup.SUPERADMIN_ROLE && user.role !== constant.lookup.ADMIN_ROLE && user.role !== constant.lookup.ECP_CUSTOMER_ROLE) {
                return Promise.reject(res.error(constant.HTML_STATUS_CODE.UNAUTHORIZED, 'Unauthorized Access.'));
            }
            if (!bodyData.name) {
                return Promise.reject(res.error(constant.HTML_STATUS_CODE.INVALID_DATA, 'Invalid Payload'));
            }
            if (!bodyData.category || bodyData.category.length <= 0) {
                return Promise.reject(res.error(constant.HTML_STATUS_CODE.INVALID_DATA, 'Please add atleast one child category !!'));
            }
            const brandExist = await brandDAO.getOne({ 'name': bodyData.name });
            if (brandExist) {
                return Promise.reject(res.error(constant.HTML_STATUS_CODE.CONFLICT, 'Brand ' + bodyData.name + ` already Exists !!`));
            }
            bodyData['brandId'] = utils.getRandomString(6);
            if (!bodyData['media'] || bodyData['media'] === '') {
                delete bodyData['media'];
            }
            let checkUnique = await brandDAO.getOne({ "brandId": bodyData['brandId'] });
            while (checkUnique) {
                bodyData['brandId'] = utils.getRandomString(6);
                checkUnique = await brandDAO.getOne({ "brandId": bodyData['brandId'] });
            }
            // BRAND CODE
            const code = await brandDAO.getPreviousCode({});
            if (!code || code.length <= 0) { bodyData['code'] = '01'; }
            if (code.length > 0) {
                const genCode = await utils.getCodes(code[0].code);
                bodyData['code'] = genCode;
            }
            bodyData['createdAt'] = Date.now();
            const createBrand = await brandDAO.createBrand(bodyData);
            if (createBrand == null) {
                return Promise.reject(res.error(constant.HTML_STATUS_CODE.INTERNAL_ERROR, 'Something went wrong with Creating Brand !!'));
            }
            return Promise.resolve(true)
        } catch (error) {
            return Promise.reject(res.error(constant.HTML_STATUS_CODE.INTERNAL_ERROR, error.message, error.stack));
        }
    },

    async getBrandByNameAndId(bodyData, user) {
        try {
            if (!bodyData) {
                return Promise.reject(res.error(constant.HTML_STATUS_CODE.INVALID_DATA, 'Invalid Payload'));
            }
            const data = await brandDAO.getBrandByNameAndId(bodyData);
            return data
            // return Promise.resolve(true);
        } catch (error) {
            return Promise.reject(res.error(constant.HTML_STATUS_CODE.INTERNAL_ERROR, error.message, error.stack));
        }
    },
    async getAllBrandsByCond(bodyData, user) {
        try {
            let limit;
            let skip;
            let page;
            let pagination = bodyData.pagination;
            if (pagination) {
                limit = isNaN(Number(pagination.limit)) ? 10 : Number(pagination.limit);
                page = Number(pagination.page) || 1;
                skip = (pagination.page ? pagination.limit * (pagination.page - 1) : 0);
            } else {
                limit = 30; // default
                skip = 0;
            }
            if (bodyData.conditions.searchTerm) {
                bodyData.conditions.$or = [
                    { name: { $regex: new RegExp(bodyData.conditions.searchTerm, "ig") } },
                    { code: { $regex: new RegExp(bodyData.conditions.searchTerm, "ig") } }
                ];
                delete bodyData.conditions.searchTerm;
            }
            let catdetails;
            if (bodyData.conditions.catName) { // brands by child category
                catdetails = await categoryDAO.getOneWithPopulateddata({ path: bodyData.conditions.catName });
                if (catdetails) {
                    bodyData.conditions['category._id'] = catdetails._id;
                }
                delete bodyData.conditions.catName;
            }
            console.log(bodyData.conditions);
            const data = await brandDAO.getAllByPageAndCond(limit, skip, bodyData.sort, bodyData.conditions);
            return Promise.resolve(Object.assign(data));
        } catch (error) {
            return Promise.reject(res.error(constant.HTML_STATUS_CODE.INTERNAL_ERROR, error.message, error.stack));
        }
    },

    async updateBrandByNameAndId(conditions, data) {
        try {
            if (!conditions || !data) {
                return Promise.reject(res.error(constant.HTML_STATUS_CODE.INVALID_DATA, 'Invalid Payload'));
            }
            const dataAvialble = await brandDAO.getOne(conditions);
            if (!dataAvialble) {
                return Promise.reject(res.error(constant.HTML_STATUS_CODE.BAD_REQUEST, 'No data found!.'));
            }
            const result = await brandDAO.updateBrandByNameAndId(conditions, data);
            return result
            // return Promise.resolve(true);
        } catch (error) {
            return Promise.reject(res.error(constant.HTML_STATUS_CODE.INTERNAL_ERROR, error.message, error.stack));
        }
    },

    async deleteBrandByNameAndId(conditions, user) {
        try {
            if (!conditions || !user) {
                return Promise.reject(res.error(constant.HTML_STATUS_CODE.INVALID_DATA, 'Invalid Payload'));
            }
            const dataAvialble = await brandDAO.getOne(conditions);
            if (!dataAvialble) {
                return Promise.reject(res.error(constant.HTML_STATUS_CODE.BAD_REQUEST, 'No data found!.'));
            }
            const deleteProductCondition = {}
            deleteProductCondition['auditFields.isDeleted'] = true;
            deleteProductCondition['auditFields.deletedBy'] = user.id;
            const result = await brandDAO.updateBrandByNameAndId(conditions, deleteProductCondition);
            return result
            // return Promise.resolve(true);
        } catch (error) {
            return Promise.reject(res.error(constant.HTML_STATUS_CODE.INTERNAL_ERROR, error.message, error.stack));
        }
    },
}
module.exports = BrandService;
