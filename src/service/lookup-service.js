
const _ = require('lodash');
const constant = require('../utils/constant');
const utils = require('./../utils/utilities');
const res = require('../utils/custom-response');
const fileService = require('../service/file-service');
const frameMaterialDAO = require('../dao/frameMaterial-dao');
const frameShapeDAO = require('../dao/frameShape-dao');
const frameTypeDAO = require('../dao/frameType-dao');
const frameColourDAO = require('../dao/frameColour-dao');
const frameModelDAO = require('../dao/frameModel-dao');
const alphanuminc = require('alphanum-increment');
const increment = alphanuminc.increment;


const lookUpService = {
    // FRAME SIZES
    async getFrameSizes() {
        try {
            return await constant.lookup.SIZE;
        } catch (error) {
            return Promise.reject(res.error(constant.HTML_STATUS_CODE.INTERNAL_ERROR, error.message, error.stack));
        }
    },

    // FRAME PARENT COLOUR
    async getFrameParentColour() {
        try {
            return await constant.lookup.COLOUR_LOOKUP;
        } catch (error) {
            return Promise.reject(res.error(constant.HTML_STATUS_CODE.INTERNAL_ERROR, error.message, error.stack));
        }
    },

    // FRAME MATERIAL
    async createFrameMaterial(frameMatData, user) {
        try {
            console.log(frameMatData.body, frameMatData.files)
            if (frameMatData.body == null && frameMatData.files == null) {
                return Promise.reject(res.error(constant.HTML_STATUS_CODE.INVALID_DATA, 'Invalid url!.'));
            }
            const code = await frameMaterialDAO.getPreviousCode({});
            if (!code.length) { frameMatData.body['code'] = 0; }
            if (code.length) {
                if (parseInt(code[0].code) < 9) {
                    frameMatData.body['code'] = increment((code[0].code));
                } else if (parseInt(code[0].code) == 9) {
                    frameMatData.body['code'] = 'A';
                } else {
                    frameMatData.body['code'] = increment(code[0].code).toUpperCase();
                }
            }
            frameMatData.body['createdAt'] = Date.now();
            const result = await frameMaterialDAO.create(frameMatData.body);
            if (result == null) {
                return Promise.reject(res.error(constant.HTML_STATUS_CODE.INVALID_DATA, `No frameMaterial found.`));
            }
            // if (frameMatData.files) {
            //     // const media = await fileService.compressFile(frameMatData.files);
            //     const media = await fileService.uploadS3(frameMatData);
            //     const med = await frameMaterialDAO.update({ _id: result._id }, { media: media });
            //     result['media'] = media;
            // }
            return Promise.resolve(result);
        } catch (error) {
            return Promise.reject(res.error(constant.HTML_STATUS_CODE.INTERNAL_ERROR, error.message, error.stack));
        }
    },
    async getFrameMaterialById(frameMatparams) {
        try {
            if (frameMatparams == null) {
                return Promise.reject(res.error(constant.HTML_STATUS_CODE.INVALID_DATA, 'Invalid url!.'));
            }
            const result = await frameMaterialDAO.getOne({ _id: frameMatparams.frameMatId });
            if (result == null) {
                return Promise.reject(res.error(constant.HTML_STATUS_CODE.INVALID_DATA, `No frameMaterial found.`));
            }
            return result;
        } catch (error) {
            return Promise.reject(res.error(constant.HTML_STATUS_CODE.INTERNAL_ERROR, error.message, error.stack));
        }
    },
    async getAllFrameMaterials(framematerialData) {
        try {
            if (framematerialData == null) {
                return Promise.reject(res.error(constant.HTML_STATUS_CODE.INVALID_DATA, 'Invalid url!.'));
            }
            let limit;
            let skip;
            let page;
            let pagination = framematerialData.pagination;
            if (pagination) {
                limit = isNaN(Number(pagination.limit)) ? 10 : Number(pagination.limit);
                page = Number(pagination.page) || 1;
                skip = (pagination.page ? pagination.limit * (pagination.page - 1) : 0);
            } else {
                limit = 30; // default
                skip = 0;
            }
            if (framematerialData.conditions.searchTerm) {
                framematerialData.conditions.$or = [
                    { name: { $regex: new RegExp(framematerialData.conditions.searchTerm, "ig") } },
                    { code: { $regex: new RegExp(framematerialData.conditions.searchTerm, "ig") } }
                ];
                delete framematerialData.conditions.searchTerm;
            }
            framematerialData.conditions['auditFields.isDeleted'] = false;
            const result = await frameMaterialDAO.getAll(limit, skip, framematerialData.sort, framematerialData.conditions);
            if (!result.length) {
                return Promise.reject(res.error(constant.HTML_STATUS_CODE.NOT_FOUND, 'No data!.'))
            }
            return result;
        } catch (error) {
            return Promise.reject(res.error(constant.HTML_STATUS_CODE.INTERNAL_ERROR, error.message, error.stack));
        }
    },
    async updateFrameMaterial(frameMatparams, prodDetails) {
        try {
            if (prodDetails == null || frameMatparams == null) {
                return Promise.reject(res.error(constant.HTML_STATUS_CODE.INVALID_DATA, 'Invalid url!.'));
            }
            const objId = await frameMaterialDAO.getOne({ _id: frameMatparams.frameMatId });
            if (objId == null) {
                return Promise.reject(res.error(constant.HTML_STATUS_CODE.INVALID_DATA, `No frameMaterial found.`));
            }
            const result = await frameMaterialDAO.update({ _id: frameMatparams.frameMatId }, prodDetails);
            return result;
        } catch (error) {
            return Promise.reject(res.error(constant.HTML_STATUS_CODE.INTERNAL_ERROR, error.message, error.stack));
        }
    },

    async deleteFrameMaterial(frameMatparams, user) {
        try {
            if (frameMatparams == null) {
                return Promise.reject(res.error(constant.HTML_STATUS_CODE.INVALID_DATA, 'Invalid url!.'));
            }
            const objId = await frameMaterialDAO.getOne({ _id: frameMatparams.frameMatId });
            if (objId == null) {
                return Promise.reject(res.error(constant.HTML_STATUS_CODE.INVALID_DATA, `No frameMaterial found.`));
            }
            const deleteFrameCondition = {}
            deleteFrameCondition['auditFields.isDeleted'] = true;
            deleteFrameCondition['auditFields.deletedBy'] = user.id;
            const result = await frameMaterialDAO.update({ _id: frameMatparams.frameMatId }, deleteFrameCondition);
            return result;
        } catch (error) {
            return Promise.reject(res.error(constant.HTML_STATUS_CODE.INTERNAL_ERROR, error.message, error.stack));
        }
    },

    // FRAME SHAPE

    async createFrameShape(frameShapeData, user) {
        try {
            if (frameShapeData.body == null && frameShapeData.files == null) {
                return Promise.reject(res.error(constant.HTML_STATUS_CODE.INVALID_DATA, 'Invalid url!.'));
            }
            const code = await frameShapeDAO.getPreviousCode({});
            if (!code.length) { frameShapeData.body['code'] = 0; }
            if (code.length) {
                if (parseInt(code[0].code) < 9) {
                    frameShapeData.body['code'] = increment((code[0].code));
                } else if (parseInt(code[0].code) == 9) {
                    frameShapeData.body['code'] = 'A';
                } else {
                    frameShapeData.body['code'] = increment(code[0].code).toUpperCase();
                }
            }
            frameShapeData.body['createdAt'] = Date.now();
            const result = await frameShapeDAO.create(frameShapeData.body);
            if (result == null) {
                return Promise.reject(res.error(constant.HTML_STATUS_CODE.INVALID_DATA, `No frame shape found.`));
            }
            if (frameShapeData.files) {
                // const media = await fileService.compressFile(frameShapeData.file);
                const media = await fileService.uploadS3(frameShapeData);
                const med = await frameShapeDAO.update({ _id: result._id }, { media: media });
                result['media'] = media;
            }
            return (result);
        } catch (error) {
            return Promise.reject(res.error(constant.HTML_STATUS_CODE.INTERNAL_ERROR, error.message, error.stack));
        }
    },
    async getFrameShapeById(frameShapeParams) {
        try {
            if (frameShapeParams == null) {
                return Promise.reject(res.error(constant.HTML_STATUS_CODE.INVALID_DATA, 'Invalid url!.'));
            }
            const result = await frameShapeDAO.getOne({ _id: frameShapeParams.frameShapeId });
            if (result == null) {
                return Promise.reject(res.error(constant.HTML_STATUS_CODE.INVALID_DATA, `No frameShape found.`));
            }
            return result;
        } catch (error) {
            return Promise.reject(res.error(constant.HTML_STATUS_CODE.INTERNAL_ERROR, error.message, error.stack));
        }
    },
    async getAllFrameShapes(frameShapeData) {
        try {
            if (frameShapeData == null) {
                return Promise.reject(res.error(constant.HTML_STATUS_CODE.INVALID_DATA, 'Invalid url!.'));
            }
            let limit;
            let skip;
            let page;
            let pagination = frameShapeData.pagination;
            if (pagination) {
                limit = isNaN(Number(pagination.limit)) ? 10 : Number(pagination.limit);
                page = Number(pagination.page) || 1;
                skip = (pagination.page ? pagination.limit * (pagination.page - 1) : 0);
            } else {
                limit = 30; // default
                skip = 0;
            }
            if (frameShapeData.conditions.searchTerm) {
                frameShapeData.conditions.$or = [
                    { name: { $regex: new RegExp(frameShapeData.conditions.searchTerm, "ig") } },
                    { code: { $regex: new RegExp(frameShapeData.conditions.searchTerm, "ig") } }
                ];
                delete frameShapeData.conditions.searchTerm;
            }
            frameShapeData.conditions['auditFields.isDeleted'] = false;
            const result = await frameShapeDAO.getAll(limit, skip, frameShapeData.sort, frameShapeData.conditions);
            if (!result.length) {
                return Promise.reject(res.error(constant.HTML_STATUS_CODE.NOT_FOUND, 'No data!.'))
            }
            return result;
        } catch (error) {
            return Promise.reject(res.error(constant.HTML_STATUS_CODE.INTERNAL_ERROR, error.message, error.stack));
        }
    },
    async updateFrameShape(frameShapeParams, prodDetails) {
        try {
            if (prodDetails == null || frameShapeParams == null) {
                return Promise.reject(res.error(constant.HTML_STATUS_CODE.INVALID_DATA, 'Invalid url!.'));
            }
            const objId = await frameShapeDAO.getOne({ _id: frameShapeParams.frameShapeId });
            if (objId == null) {
                return Promise.reject(res.error(constant.HTML_STATUS_CODE.INVALID_DATA, `No frameShape found.`));
            }
            const result = await frameShapeDAO.update({ _id: frameShapeParams.frameShapeId }, prodDetails);
            return result;
        } catch (error) {
            return Promise.reject(res.error(constant.HTML_STATUS_CODE.INTERNAL_ERROR, error.message, error.stack));
        }
    },

    async deleteFrameShape(frameShapeParams, user) {
        try {
            if (frameShapeParams == null) {
                return Promise.reject(res.error(constant.HTML_STATUS_CODE.INVALID_DATA, 'Invalid url!.'));
            }
            const objId = await frameShapeDAO.getOne({ _id: frameShapeParams.frameShapeId });
            if (objId == null) {
                return Promise.reject(res.error(constant.HTML_STATUS_CODE.INVALID_DATA, `No frameShape found.`));
            }
            const deleteFrameCondition = {}
            deleteFrameCondition['auditFields.isDeleted'] = true;
            deleteFrameCondition['auditFields.deletedBy'] = user.id;
            const result = await frameShapeDAO.update({ _id: frameShapeParams.frameShapeId }, deleteFrameCondition);
            return result;
        } catch (error) {
            return Promise.reject(res.error(constant.HTML_STATUS_CODE.INTERNAL_ERROR, error.message, error.stack));
        }
    },


    // FRAME TYPE

    async createFrameType(frameTypeData, user) {
        try {
            if (frameTypeData.body == null || frameTypeData.files == null) {
                return Promise.reject(res.error(constant.HTML_STATUS_CODE.INVALID_DATA, 'Invalid url!.'));
            }
            const code = await frameTypeDAO.getPreviousCode({});
            if (!code.length) { frameTypeData.body['code'] = 0; }
            if (code.length) {
                if (parseInt(code[0].code) < 9) {
                    frameTypeData.body['code'] = increment((code[0].code));
                } else if (parseInt(code[0].code) == 9) {
                    frameTypeData.body['code'] = 'A';
                } else {
                    frameTypeData.body['code'] = increment(code[0].code).toUpperCase();
                }
            }
            frameTypeData.body['createdAt'] = Date.now();
            const result = await frameTypeDAO.create(frameTypeData.body);
            if (result == null) {
                return Promise.reject(res.error(constant.HTML_STATUS_CODE.INVALID_DATA, `No frame type found.`));
            }
            if (frameTypeData.files) {
                // const media = await fileService.compressFile(frameTypeData.file);
                const media = await fileService.uploadS3(frameTypeData);
                const med = await frameTypeDAO.update({ _id: result._id }, { media: media });
                result['media'] = media;
            }
            return Promise.resolve(result);
        } catch (error) {
            return Promise.reject(res.error(constant.HTML_STATUS_CODE.INTERNAL_ERROR, error.message, error.stack));
        }
    },
    async getFrameTypeById(frameTypeparams) {
        try {
            if (frameTypeparams == null) {
                return Promise.reject(res.error(constant.HTML_STATUS_CODE.INVALID_DATA, 'Invalid url!.'));
            }
            const result = await frameTypeDAO.getOne({ _id: frameTypeparams.frameTypeId });
            if (result == null) {
                return Promise.reject(res.error(constant.HTML_STATUS_CODE.INVALID_DATA, `No frameShape found.`));
            }
            return result;
        } catch (error) {
            return Promise.reject(res.error(constant.HTML_STATUS_CODE.INTERNAL_ERROR, error.message, error.stack));
        }
    },
    async getAllFrameTypes(frameTypeData) {
        try {
            if (frameTypeData == null) {
                return Promise.reject(res.error(constant.HTML_STATUS_CODE.INVALID_DATA, 'Invalid url!.'));
            }
            let limit;
            let skip;
            let page;
            let pagination = frameTypeData.pagination;
            if (pagination) {
                limit = isNaN(Number(pagination.limit)) ? 10 : Number(pagination.limit);
                page = Number(pagination.page) || 1;
                skip = (pagination.page ? pagination.limit * (pagination.page - 1) : 0);
            } else {
                limit = 30; // default
                skip = 0;
            }
            if (frameTypeData.conditions.searchTerm) {
                frameTypeData.conditions.$or = [
                    { name: { $regex: new RegExp(frameTypeData.conditions.searchTerm, "ig") } },
                    { code: { $regex: new RegExp(frameTypeData.conditions.searchTerm, "ig") } }
                ];
                delete frameTypeData.conditions.searchTerm;
            }
            frameTypeData.conditions['auditFields.isDeleted'] = false;
            const result = await frameTypeDAO.getAll(limit, skip, frameTypeData.sort, frameTypeData.conditions);
            if (!result.length) {
                return Promise.reject(res.error(constant.HTML_STATUS_CODE.NOT_FOUND, 'No data!.'))
            }
            return result;
        } catch (error) {
            return Promise.reject(res.error(constant.HTML_STATUS_CODE.INTERNAL_ERROR, error.message, error.stack));
        }
    },
    async updateFrameType(frameShapeParams, prodDetails) {
        try {
            if (prodDetails == null || frameShapeParams == null) {
                return Promise.reject(res.error(constant.HTML_STATUS_CODE.INVALID_DATA, 'Invalid url!.'));
            }
            const objId = await frameTypeDAO.getOne({ _id: frameShapeParams.frameTypeId });
            if (objId == null) {
                return Promise.reject(res.error(constant.HTML_STATUS_CODE.INVALID_DATA, `No frameShape found.`));
            }
            const result = await frameTypeDAO.update({ _id: frameShapeParams.frameTypeId }, prodDetails);
            return result;
        } catch (error) {
            return Promise.reject(res.error(constant.HTML_STATUS_CODE.INTERNAL_ERROR, error.message, error.stack));
        }
    },

    async deleteFrameType(frameShapeParams, user) {
        try {
            if (frameShapeParams == null) {
                return Promise.reject(res.error(constant.HTML_STATUS_CODE.INVALID_DATA, 'Invalid url!.'));
            }
            const objId = await frameTypeDAO.getOne({ _id: frameShapeParams.frameTypeId });
            if (objId == null) {
                return Promise.reject(res.error(constant.HTML_STATUS_CODE.INVALID_DATA, `No frameShape found.`));
            }
            const deleteFrameCondition = {}
            deleteFrameCondition['auditFields.isDeleted'] = true;
            deleteFrameCondition['auditFields.deletedBy'] = user.id;
            const result = await frameTypeDAO.update({ _id: frameShapeParams.frameTypeId }, deleteFrameCondition);
            return result;
        } catch (error) {
            return Promise.reject(res.error(constant.HTML_STATUS_CODE.INTERNAL_ERROR, error.message, error.stack));
        }
    },



    // FRAME COLOUR

    async createFrameColour(frameColourData, user) {
        try {
            if (frameColourData == null) {
                return Promise.reject(res.error(constant.HTML_STATUS_CODE.INVALID_DATA, 'Invalid url!.'));
            }
            // FRAME COLOUR CODE
            const code = await frameColourDAO.getPreviousCode({});
            if (!code.length) { frameColourData['XMCode'] = '01'; }
            if (code.length) {
                const genCode = await utils.getCodes(code[0].XMCode);
                frameColourData['XMCode'] = genCode;
            }
            frameColourData['createdAt'] = Date.now();
            const result = await frameColourDAO.create(frameColourData);
            if (result == null) {
                return Promise.reject(res.error(constant.HTML_STATUS_CODE.INVALID_DATA, `No frame type found.`));
            }
            return Promise.resolve(result);
        } catch (error) {
            return Promise.reject(res.error(constant.HTML_STATUS_CODE.INTERNAL_ERROR, error.message, error.stack));
        }
    },
    async getFrameColourById(frameColourData) {
        try {
            if (frameColourData == null) {
                return Promise.reject(res.error(constant.HTML_STATUS_CODE.INVALID_DATA, 'Invalid url!.'));
            }
            const result = await frameColourDAO.getOne({ _id: frameColourData.frameColourId });
            if (result == null) {
                return Promise.reject(res.error(constant.HTML_STATUS_CODE.INVALID_DATA, `No Frame colour found.`));
            }
            return result;
        } catch (error) {
            return Promise.reject(res.error(constant.HTML_STATUS_CODE.INTERNAL_ERROR, error.message, error.stack));
        }
    },
    async getAllFrameColour(frameColourData) {
        try {
            if (frameColourData == null) {
                return Promise.reject(res.error(constant.HTML_STATUS_CODE.INVALID_DATA, 'Invalid url!.'));
            }
            let limit;
            let skip;
            let page;
            let pagination = frameColourData.pagination;
            if (pagination) {
                limit = isNaN(Number(pagination.limit)) ? 10 : Number(pagination.limit);
                page = Number(pagination.page) || 1;
                skip = (pagination.page ? pagination.limit * (pagination.page - 1) : 0);
            } else {
                limit = 30; // default
                skip = 0;
            }
            if (frameColourData.conditions.searchTerm) {
                frameColourData.conditions.$or = [
                    { actualColour: { $regex: new RegExp(frameColourData.conditions.searchTerm, "ig") } },
                    { parentColour: { $regex: new RegExp(frameColourData.conditions.searchTerm, "ig") } },
                    { hexCode: { $regex: new RegExp(frameColourData.conditions.searchTerm, "ig") } },
                    { XMCode: { $regex: new RegExp(frameColourData.conditions.searchTerm, "ig") } }
                ];
                delete frameColourData.conditions.searchTerm;
            }
            frameColourData.conditions['auditFields.isDeleted'] = false;
            const result = await frameColourDAO.getAll(limit, skip, frameColourData.sort, frameColourData.conditions);
            if (!result.length) {
                return Promise.reject(res.error(constant.HTML_STATUS_CODE.NOT_FOUND, 'No data!.'))
            }
            return result;
        } catch (error) {
            return Promise.reject(res.error(constant.HTML_STATUS_CODE.INTERNAL_ERROR, error.message, error.stack));
        }
    },



    // FRAME MODEL

    async creteFrameModel(frameModelData, user) {
        try {
            if (frameModelData == null) {
                return Promise.reject(res.error(constant.HTML_STATUS_CODE.INVALID_DATA, 'Invalid url!.'));
            }
            let checkUnique = frameModelData['code'] = utils.getRandomNumber(2);
            while (checkUnique) {
                frameModelData['code'] = utils.getRandomNumber(3);
                checkUnique = await frameModelDAO.getOne({ code: frameModelData['code'] });
            }
            // FRAME MODEL CODE
            const code = await frameModelDAO.getPreviousCode({});
            if (!code.length) { frameModelData['code'] = '001'; }
            if (code.length) {
                const genCode = await utils.getCodes(code[0].code);
                frameModelData['code'] = genCode;
            }
            frameModelData['createdAt'] = Date.now();
            if (!frameModelData['modelCode']) {
                frameModelData['modelCode'] = utils.getRandomString(3);
            }

            const result = await frameModelDAO.create(frameModelData);
            if (result == null) {
                return Promise.reject(res.error(constant.HTML_STATUS_CODE.INVALID_DATA, `No frame type found.`));
            }
            return Promise.resolve(result);
        } catch (error) {
            return Promise.reject(res.error(constant.HTML_STATUS_CODE.INTERNAL_ERROR, error.message, error.stack));
        }
    },
    async getFrameModelById(frameTypeparams) {
        try {
            if (frameTypeparams == null) {
                return Promise.reject(res.error(constant.HTML_STATUS_CODE.INVALID_DATA, 'Invalid url!.'));
            }
            const result = await frameModelDAO.getOne({ _id: frameTypeparams.frameModelId });
            if (result == null) {
                return Promise.reject(res.error(constant.HTML_STATUS_CODE.INVALID_DATA, `No Frame model found.`));
            }
            return result;
        } catch (error) {
            return Promise.reject(res.error(constant.HTML_STATUS_CODE.INTERNAL_ERROR, error.message, error.stack));
        }
    },
    async getAllFrameModel(frameModelData) {
        try {
            if (frameModelData == null) {
                return Promise.reject(res.error(constant.HTML_STATUS_CODE.INVALID_DATA, 'Invalid url!.'));
            }
            let limit;
            let skip;
            let page;
            let pagination = frameModelData.pagination;
            if (frameModelData.conditions.brandId) {
                frameModelData.conditions['brand.brandId'] = frameModelData.conditions.brandId;
                delete frameModelData.conditions.brandId;
            }
            if (pagination) {
                limit = isNaN(Number(pagination.limit)) ? 10 : Number(pagination.limit);
                page = Number(pagination.page) || 1;
                skip = (pagination.page ? pagination.limit * (pagination.page - 1) : 0);
            } else {
                limit = 30; // default
                skip = 0;
            }
            if (frameModelData.conditions.searchTerm) {
                frameModelData.conditions.$or = [
                    { name: { $regex: new RegExp(frameModelData.conditions.searchTerm, "ig") } },
                    { modelCode: { $regex: new RegExp(frameModelData.conditions.searchTerm, "ig") } },
                    { code: { $regex: new RegExp(frameModelData.conditions.searchTerm, "ig") } },
                    // { brand: { $regex: new RegExp(frameModelData.conditions.searchTerm, "ig") } },
                ];
                delete frameModelData.conditions.searchTerm;
            }
            frameModelData.conditions['auditFields.isDeleted'] = false;
            const result = await frameModelDAO.getAll(limit, skip, frameModelData.sort, frameModelData.conditions);
            if (!result.length) {
                return Promise.reject(res.error(constant.HTML_STATUS_CODE.NOT_FOUND, 'No data!.'))
            }
            return result;
        } catch (error) {
            return Promise.reject(res.error(constant.HTML_STATUS_CODE.INTERNAL_ERROR, error.message, error.stack));
        }
    }



};

module.exports = lookUpService;