
const _ = require('lodash');
const constant = require('../utils/constant');
const res = require('../utils/custom-response');
const utils = require('../utils/utilities');
const bannerDAO = require('../dao/banner-dao');
const fs = require('fs');
const s3Config = require('../utils/s3_util/Config');
const S3Manager = require('../utils/s3_util/S3Manager');

const BannerService = {
    async createBanner(req) {
        try {
            if (req == null) {
                return Promise.reject(res.error(constant.HTML_STATUS_CODE.INVALID_url, 'Invalid url!.'));
            }
            req.body['bannerId'] = utils.getRandomString(6);
            let checkUnique = await bannerDAO.getOne({ "bannerId": req['bannerId'] });
            while (checkUnique) {
                req.body['bannerId'] = utils.getRandomString(6);
                checkUnique = await bannerDAO.getOne({ "bannerId": req['bannerId'] });
            }
            const result = await bannerDAO.create(req.body);
            if (req.files.image) {
                const imgUrl = await S3Manager.uploadS3({
                    Bucket: s3Config.XM_CUST_Bucket_Name,
                    Key: `${S3Manager.generateUrlKey("images/", req.files.image.name)}`,
                    ACL: s3Config.ACL,
                    Body: fs.createReadStream(req.files.image.path)
                }, async (err, img) => {
                    if (err) {
                        reject(err);
                    } else {
                        if (img.url) {
                            let updateBanner = await bannerDAO.updateByCond({ bannerId: result.bannerId }, { image: img.url });
                            if (updateBanner.nModified !== 1) {
                                return Promise.resolve({ status: constant.HTML_STATUS_CODE.SUCCESS, message: 'Unable to upload image!!' });
                            }
                        }
                    }
                });
            }
            return Promise.resolve(true);
        } catch (error) {
            return Promise.reject(res.error(constant.HTML_STATUS_CODE.INTERNAL_ERROR, error.message, error.stack));
        }
    },
    async getBannerById(loggedInUser, bannerId) {
        try {
            if (loggedInUser == null || bannerId == null) {
                return Promise.reject(res.error(constant.HTML_STATUS_CODE.INVALID_url, 'Invalid url!.'));
            }
            const result = await bannerDAO.getOne({bannerId: bannerId});
            if (result == null) {
                return Promise.reject(res.error(constant.HTML_STATUS_CODE.INVALID_url, `No banner found.`));
            }
            return result;
        } catch (error) {
            return Promise.reject(res.error(constant.HTML_STATUS_CODE.INTERNAL_ERROR, error.message, error.stack));
        }
    },
    async listAllBanners(bannerData) {
        try {
            if (bannerData == null) {
                return Promise.reject(res.error(constant.HTML_STATUS_CODE.INVALID_url, 'Invalid url!.'));
            }
            let limit;
            let skip;
            let page;
            let pagination = bannerData.pagination;
            if (pagination) {
                limit = isNaN(Number(pagination.limit)) ? 10 : Number(pagination.limit);
                page = Number(pagination.page) || 1;
                skip = (pagination.page ? pagination.limit * (pagination.page - 1) : 0);
            } else {
                limit = 30; // default
                skip = 0;
            }
            bannerData.conditions['auditFields.isDeleted'] = false;
            const result = await bannerDAO.getAll(limit, skip, bannerData.sort, bannerData.conditions);
            return result;
        } catch (error) {
            return Promise.reject(res.error(constant.HTML_STATUS_CODE.INTERNAL_ERROR, error.message, error.stack));
        }
    },
    async listAllbannersAt(bannerData) {
        try {
            if (bannerData == null) {
                return Promise.reject(res.error(constant.HTML_STATUS_CODE.INVALID_url, 'Invalid url!.'));
            }
            const result = await bannerDAO.getAllPrdAttributes(bannerData);
            return result;
        } catch (error) {
            return Promise.reject(res.error(constant.HTML_STATUS_CODE.INTERNAL_ERROR, error.message, error.stack));
        }
    },
    async updateBanner(bannerId, prodDetails) {
        try {
            if (prodDetails == null || bannerId == null) {
                return Promise.reject(res.error(constant.HTML_STATUS_CODE.INVALID_DATA, 'Invalid url!.'));
            }
            const objId = await bannerDAO.getOne({bannerId: bannerId});
            if (objId == null) {
                return Promise.reject(res.error(constant.HTML_STATUS_CODE.INVALID_DATA, `No banner found.`));
            }
            const result = await bannerDAO.updateByCondition({bannerId: bannerId}, prodDetails);
            return result;
        } catch (error) {
            return Promise.reject(res.error(constant.HTML_STATUS_CODE.INTERNAL_ERROR, error.message, error.stack));
        }
    },

    async deleteBanner(bannerId, user) {
        try {
            if (bannerId == null) {
                return Promise.reject(res.error(constant.HTML_STATUS_CODE.INVALID_DATA, 'Invalid url!.'));
            }
            const objId = await bannerDAO.getOne({bannerId: bannerId});
            if (objId == null) {
                return Promise.reject(res.error(constant.HTML_STATUS_CODE.INVALID_DATA, `No banner found.`));
            }
            const deletebannerCondition = {}
            deletebannerCondition['auditFields.isDeleted'] = true;
            deletebannerCondition['auditFields.deletedBy'] = user.id;
            const result = await bannerDAO.updateByCondition({ bannerId: bannerId }, deletebannerCondition);
            return result;
        } catch (error) {
            return Promise.reject(res.error(constant.HTML_STATUS_CODE.INTERNAL_ERROR, error.message, error.stack));
        }
    },
};


module.exports = BannerService;