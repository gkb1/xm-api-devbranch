const bcrypt = require('bcrypt');
const _ = require('lodash');
const fs = require('fs');
const s3Config = require('../utils/s3_util/Config');
const constant = require('../utils/constant');
const res = require('../utils/custom-response');
const S3Manager = require('../utils/s3_util/S3Manager');
const path = require('path');
var multer = require('multer');
var fileUpload = false;
var fileFilter = false;
const userDAO = require('../dao/user-dao');
const async = require('async');

const compressor = require('../utils/compressor');
const ProductService = require('../service/product-service');
const productDAO = require('../dao/product-dao');
const frameDAO = require('../dao/frameId-dao');

const promiseRequest = function (methodName, url, json) {
    const options = {
        method: methodName,
        headers: {
            "content-type": "application/json",
        },
        url: constant.GKB_PHP_BACKEND_URL + url,
        json: json,
    };
    return new Promise((resolve, reject) => {
        request(options, (error, response, body) => {
            if (error) {
                return reject(error);
            }
            return resolve(body);
        });
    });
};



var storage = multer.diskStorage({
    destination: function (req, file, cb) {
        if (constant.FIELD_TYPE.indexOf(file.fieldname) !== -1) {
            cb(null, path.resolve('./uploads'));
        }
    },
    filename: function (req, file, cb) {
        cb(null, Date.now() + "__" + file.fieldname + "__" + path.extname(file.originalname));
    }
});

var fileFilter = function (req, file, cb) {
    fileUpload = true;
    if (constant.FILE_TYPE.indexOf(file.mimetype) !== -1) {
        cb(null, true);
    } else {
        fileFilter = true;
        cb(null, false);
    }
};

var uploads = multer({
    storage: storage,
    fileFilter: fileFilter,
    limits: {
        fileSize: 1024 * 1024 * 1024
    }
});

// var upload = uploads.fields(constant.FIELD_TYPE.map((o) => {
//     return { name: o }
// }));
// var uploadImage = uploads.fields([{ name: 'attach' }]);


const FileService = {

    // upload(req) {
    //     return new Promise(function (resolve, reject) {
    //         try {
    //             upload(req, {}, function (err) {
    //                 if (err) {
    //                     throw err;
    //                 }
    //                 if (req.files == null) {
    //                     return reject(res.error(constant.HTML_STATUS_CODE.INVALID_DATA, 'No file to upload'));
    //                 }
    //                 resolve(constant.SERVER_FILE_URL + req.files[Object.keys(req.files)[0]][0].filename);
    //             });
    //         } catch (error) {
    //             return reject(res.error(constant.HTML_STATUS_CODE.INTERNAL_ERROR, error.message, error.stack));
    //         }
    //     });
    // },
    async uploadKYCDocs(req) {
        try {
            if (!req.user) {
                return Promise.reject(res.error(constant.HTML_STATUS_CODE.INVALID_DATA, 'Invalid User'));
            }
            if (req.user.role !== constant.lookup.ECP_CUSTOMER_ROLE) {
                return Promise.reject(res.error(constant.HTML_STATUS_CODE.UNAUTHORIZED, 'Unauthorized Access.'));
            }
            if (!req.files.doc) {
                return Promise.reject(res.error(constant.HTML_STATUS_CODE.INVALID_DATA, 'Invalid File'));
            }
            if (!req.body.idNo || !req.body.idType || !req.body.idProofType) {
                return Promise.reject(res.error(constant.HTML_STATUS_CODE.INVALID_DATA, 'Invalid Payload'));
            }
            return new Promise((resolve, reject) => {
                S3Manager.uploadS3({
                    Bucket: s3Config.ECP_Bucket_Name,
                    Key: `${S3Manager.generateUrlKey("images/", req.files.doc.name)}`,
                    ACL: s3Config.ACL,
                    Body: fs.createReadStream(req.files.doc.path)
                }, async (err, result) => {
                    if (err) {
                        return Promise.reject(res.error(constant.HTML_STATUS_CODE.INTERNAL_ERROR, error.message, error.stack));
                    } else {
                        let updateFields = {};
                        updateFields = {
                            idNo: req.body.idNo,
                            [req.body.idProofType]: result['url'],
                        }
                        // update doc url for user
                        let fieldName = 'documents' + '.' + req.body.idType;
                        let updateUser = await userDAO.updateByCondition({ userId: req.user.userId }, { [fieldName]: updateFields });
                        console.log(updateUser);
                        if (updateUser.nModified === 1) { // save uploaded image url
                            // upload base64 url to drishti
                            var url = 'getFileUpload.php';
                            var response = await promiseRequest('POST', url, req.body.drishti);
                            if (body['httpResponseCode'] === 200) {
                                return Promise.resolve(response);
                            } else { // PENDING delete uploaded s3 image
                                let resetDocUploaded = await userDAO.updateByCondition({ userId: req.user.userId }, { [fieldName]: {} });
                                S3Manager.deleteObject({
                                    Bucket: s3Config.ECP_Bucket_Name,
                                    Key: `${S3Manager.generateUrlKey("images/", req.files.doc.name)}`,
                                }, async (err, result) => {
                                    if (err) {
                                        console.log(err.message, err.stack)
                                    }
                                    return Promise.reject(res.error(response['httpResponseCode'] ? response['httpResponseCode'] : constant.HTML_STATUS_CODE.INVALID_DATA, response.message));
                                });
                            }
                        } else { // PENDING delete uploaded s3 image
                            S3Manager.deleteObject({
                                Bucket: s3Config.ECP_Bucket_Name,
                                Key: `${S3Manager.generateUrlKey("images/", req.files.doc.name)}`,
                            }, async (err, result) => {
                                if (err) {
                                    console.log(err.message, err.stack)
                                }
                                return Promise.reject(res.error(constant.HTML_STATUS_CODE.INVALID_DATA, 'Unable to upload !!'));
                            });
                        }
                    }
                });
            })
        } catch (error) {
            return Promise.reject(res.error(constant.HTML_STATUS_CODE.INTERNAL_ERROR, error.message, error.stack));
        }
    },
    async uploadS3(req) {
        console.log(req);
        return new Promise((resolve, reject) => {
            S3Manager.uploadS3({
                Bucket: s3Config.ECP_Bucket_Name,
                Key: `${S3Manager.generateUrlKey("images/", (req.files.media ? req.files.media.name : req.files.content.name))}`,
                ACL: s3Config.ACL,
                Body: fs.createReadStream((req.files.media ? req.files.media.path : req.files.path))
            }, (err, result) => {
                console.log(err, result);
                if (err) {
                    reject(err);
                } else resolve(result);
            });
        })
    },

    async deleteObjectsFromBucket(fileArray, folderName) {
        return new Promise((resolve, reject) => {
            var deleteParam = {
                Bucket: s3Config.bucketName,
                Delete: {
                    Objects: fileArray
                }
            };
            S3Manager.deleteObjects(deleteParam, function (err, data) {
                console.log(err, data);
                if (err) {
                    reject(err);
                } else {
                    resolve();
                }
            });
        });
    },

    async uploadToS3(file) {
        const data = {};
        data['name'] = file.content ? file.content.name : file.name;
        if (file.type ? file.type.indexOf('image/') > -1 : file.content.type.indexOf('image/') > -1) {
            data['type'] = constant.lookup.MEDIA_TYPE[0];
        } else if (file.type ? file.type.indexOf('video/') > -1 : file.content.type.indexOf('video/') > -1) {
            data['type'] = constant.lookup.MEDIA_TYPE[1];
        }
        return new Promise((resolve, reject) => {
            S3Manager.uploadS3({
                Bucket: s3Config.XM_CUST_Bucket_Name,
                Key: `${S3Manager.generateUrlKey("images/", file.content ? file.content.name : file.name)}`,
                ACL: s3Config.ACL,
                Body: fs.createReadStream(file.content ? file.content.path : file.path)
            }, (err, result) => {
                if (err) {
                    reject(err);
                } else {
                    data['url'] = result.url;
                    resolve(data);
                }
            });
        })
    },

    // COMPRESS IMAGE
    async compressFile(file) {
        return new Promise((resolve, reject) => {
            console.log(`${file.originalname} is now uploading...`);
            if (_.isUndefined(file) || _.isEmpty(file)) {
                Promise.reject();
            }
            compressor.compressImageInWebP(file).then((compressedFile) => {
                const data = {};
                data['name'] = file.originalname;
                if (file.mimetype.indexOf('image/') > -1) {
                    data['type'] = constant.lookup.MEDIA_TYPE[0];
                } else if (file.mimetype.indexOf('video/') > -1) {
                    data['type'] = constant.lookup.MEDIA_TYPE[1];
                }
                // return new Promise((resolve, reject) => {
                S3Manager.uploadS3({
                    Bucket: s3Config.XM_CUST_Bucket_Name,
                    Key: `${S3Manager.generateUrlKey("images/", compressedFile.originalname)}`,
                    ACL: s3Config.ACL,
                    Body: (compressedFile.buffer)
                }, (err, result) => {
                    if (result) {
                        data['url'] = result.url;
                        return resolve(data);
                    } else {
                        return reject(err);
                    }
                });
                // })
            }).catch((error) => {
                return reject(error);
            });
        });
    },

    async readS3Folder(user, body) {
        try {
            if (!user) {
                return Promise.reject(res.error(constant.HTML_STATUS_CODE.INVALID_DATA, 'Invalid User'));
            }
            if (user.role !== constant.lookup.SUPERADMIN_ROLE && user.role !== constant.lookup.ADMIN_ROLE) {
                return Promise.reject(res.error(constant.HTML_STATUS_CODE.UNAUTHORIZED, 'Unauthorized Access.'));
            }
            if (!body.bucketName) {
                body.bucketName = s3Config.XM_PHOTOGRAPER_Bucket_Name;
            }
            if (!body.folderName) {
                body.folderName = s3Config.WORKING_DIR;
            }
            let params, tagFolderName;
            if (body.tagFolderName) {
                params = {
                    Bucket: body.bucketName,
                    Delimiter: '/', // directories                
                    Prefix: body.folderName + '/' + body.tagFolderName + '/', // folder name + subfolderName
                };
            } else {
                params = {
                    Bucket: body.bucketName,
                    Delimiter: '/', // directories                
                    Prefix: body.folderName + '/', // folder name
                };
            }
            return new Promise(async (resolve, reject) => {
                await S3Manager.listObjectsV2(params, async (err, output) => {
                    if (err) {
                        return reject(res.error(constant.HTML_STATUS_CODE.INTERNAL_ERROR, err.message, err.stack));
                    }
                    // sub folder 
                    if (body.tagFolderName) {
                        const frameResult = await frameDAO.getSelFrameByFrameId({ 'store.frame.tagName': body.tagFolderName });
                        if (!frameResult || !frameResult.store || frameResult.store.frame.length <= 0) {
                            return reject(res.error(constant.HTML_STATUS_CODE.INVALID_DATA, `Invalid Tag No !!`));
                        }
                        if (frameResult.store.frame[0]) {
                            if (!frameResult.store.frame[0].status.isApproved) {
                                return reject(res.error(constant.HTML_STATUS_CODE.INVALID_DATA, 'Tag No ' + body.tagFolderName + ' is not yet approved!!'));
                            }
                        }
                        // if (frameResult && frameResult.store && frameResult.store.frame.length <= 0 && frameResult.store.frame[0] && frameResult.store.frame[0].status.isApproved) {
                        const result = await productDAO.getSelPrdtAttr({ 'prdt_attributes.colour.frameId': frameResult.store.frame[0].frameID });
                        const getSelProductResult = Object.assign({ product: result }, { store: frameResult.store });
                        if (!getSelProductResult || !getSelProductResult.product) {
                            //  return reject(res.error(constant.HTML_STATUS_CODE.INVALID_DATA, 'Product with tag name '+tagFolderName+' doesnt exists !!'));
                            console.log('Not found !!' + tagFolderName);
                        }
                        if (!getSelProductResult.product.prdt_attributes || getSelProductResult.product.prdt_attributes <= 0) {
                            // return reject(res.error(constant.HTML_STATUS_CODE.INVALID_DATA, 'Product with tag name '+tagFolderName+' doesnt exists !!'));
                            console.log('Not found !!' + tagFolderName);
                        }
                        if (output && output.Contents && output.Contents.length > 1) {
                            return getSubFolderDetails(body, getSelProductResult, output, body.tagFolderName, output.Prefix, resolve, reject);
                        } else {
                            return resolve({ status: constant.HTML_STATUS_CODE.INVALID_DATA, message: 'No Files Present !!' });
                        }
                        // }
                    } else { // folder
                        if (!_.isUndefined(output.CommonPrefixes) && output.CommonPrefixes.length > 0) { // get subfolder details
                            output.CommonPrefixes.forEach(async function (commonPrefixes) {
                                tagFolderName = commonPrefixes.Prefix.split('/')[1];
                                const frameResult = await frameDAO.getSelFrameByFrameId({ 'store.frame.tagName': tagFolderName });
                                if (!frameResult || !frameResult.store || frameResult.store.frame.length <= 0) {
                                    return reject(res.error(constant.HTML_STATUS_CODE.INVALID_DATA, `Invalid Tag No !!`));
                                }
                                if (frameResult.store.frame[0]) {
                                    if (!frameResult.store.frame[0].status.isApproved) {
                                        return reject(res.error(constant.HTML_STATUS_CODE.INVALID_DATA, 'Tag No ' + tagFolderName + ' is not yet approved!!'));
                                    }
                                }
                                const result = await productDAO.getSelPrdtAttr({ 'prdt_attributes.colour.frameId': frameResult.store.frame[0].frameID });
                                const getSelProductResult = Object.assign({ product: result }, { store: frameResult.store });
                                if (!getSelProductResult || !getSelProductResult.product) {
                                    //  return reject(res.error(constant.HTML_STATUS_CODE.INVALID_DATA, 'Product with tag name '+tagFolderName+' doesnt exists !!'));
                                    console.log('Not found !!' + tagFolderName);
                                }
                                if (!getSelProductResult.product.prdt_attributes || getSelProductResult.product.prdt_attributes <= 0) {
                                    // return reject(res.error(constant.HTML_STATUS_CODE.INVALID_DATA, 'Product with tag name '+tagFolderName+' doesnt exists !!'));
                                    console.log('Not found !!' + tagFolderName);
                                }
                                // get subfolder details
                                await S3Manager.listObjectsV2({ Bucket: body.bucketName, Prefix: commonPrefixes.Prefix }, (errP, result) => {
                                    if (errP) {
                                        return reject(res.error(constant.HTML_STATUS_CODE.INTERNAL_ERROR, errP.message, errP.stack));
                                    }
                                    if (!_.isUndefined(result.Contents) && result.Contents.length > 0) {
                                        return getSubFolderDetails(body, getSelProductResult, result, tagFolderName, commonPrefixes.Prefix, resolve, reject);
                                    }
                                });
                            });
                        } else {
                            return resolve({ status: constant.HTML_STATUS_CODE.INVALID_DATA, message: 'No Folders !!' });
                        }
                    }
                });
            });
        } catch (error) {
            return Promise.reject(res.error(constant.HTML_STATUS_CODE.INTERNAL_ERROR, error.message, error.stack));
        }
    },
};

var getSubFolderDetails = async function (body, getSelProductResult, result, tagFolderName, prefix, resolve, reject) {
    let media = [];
    let coverImg;
    // return new Promise(async (resolve, reject) => { 
    var iteratorFcn = async function (contents, index, done) {
        media = [];
        if (contents.Size > 0) { // file exists
            const fileName = tagFolderName + '_' + contents.Key.substring(contents.Key.lastIndexOf("/") + 1);
            const filePath = tagFolderName.split('-')[1] + '/' + contents.Key.substring(contents.Key.lastIndexOf("/") + 1);
            const isCopied = await moveFile(body.bucketName, s3Config.XM_CUST_Bucket_Name, contents.Key, filePath, 'images');
            if (isCopied) {
                // save url to prdt by tagNo
                const data = {};
                data['name'] = fileName;
                data['type'] = constant.lookup.MEDIA_TYPE[0];
                data.url = 'https://' + s3Config.XM_CUST_Bucket_Name + '.s3.ap-south-1.amazonaws.com/' + 'images/' + filePath;
                // update product image
                if (index === 1) { // cover_img
                    coverImg = data;
                }
                if (index !== 1) { // media images
                    media.push(data);
                }
            }
        }
        done;
    }
    var doneIteratingFcn = async function (err, outputPath) {
        if (err) {
            console.log('readS3Folder | ERROR', err.message, err.stack)
        }
        let updateFields = {
            'prdt_attributes.$.colour.cover_img': coverImg
        };
        if (media && media.length > 0) {
            updateFields['prdt_attributes.$.colour.media'] = media;
        }
        const updateProduct = await productDAO.updateByCond({ productId: getSelProductResult.product.productId, 'prdt_attributes.prdAttributeId': getSelProductResult.product.prdt_attributes[0].prdAttributeId }, { $set: updateFields });
        if (updateProduct.n > 0 || updateProduct.nModified > 0) { // updated
            // delete from working dir, move to final directory
            await S3Manager.listObjectsV2({ Bucket: body.bucketName, Prefix: prefix }, function (err, data) {
                const newPrefix = prefix.replace(body.folderName, s3Config.FINISH_DIR);
                var copyIteratorFcn = async function (contents, index, done) {
                    var params = {
                        Bucket: body.bucketName,
                        CopySource: body.bucketName + '/' + contents.Key,
                        Key: contents.Key.replace(prefix, newPrefix)
                    };
                    await S3Manager.copyObject(params, async function (copyErr, copyData) {
                        if (copyErr) {
                            console.log(err);
                        } else {
                            console.log('Copied to final dir: ', params.Key);
                            await S3Manager.deleteObject({ Bucket: body.bucketName, Key: contents.Key }, function (deleteErr, deleteData) {
                                if (deleteErr) {
                                    console.log(err);
                                } else {
                                    console.log('Deleted from working dir : ', params.Key);
                                }
                            });
                        }
                        done;
                    });
                }
                var doneCopy = function (err, data) {
                    if (err) {
                        console.log('doneCopy', err);
                        return resolve({ status: constant.HTML_STATUS_CODE.SUCCESS, message: 'Images Uploaded Successfully, Unable to move to final directory!!' });
                    }
                    return resolve({ status: constant.HTML_STATUS_CODE.SUCCESS, message: 'Images Uploaded Successfully, Moved to final directory!!' });
                }
                if (data && data.Contents.length > 0) {
                    async.forEachOf(data.Contents, copyIteratorFcn, doneCopy);
                }
            });
        } else {
            return resolve({ status: constant.HTML_STATUS_CODE.INVALID_DATA, message: 'Unable to Upload Image, Please try again later !!' });
        }
    }
    // iteratorFcn
    async.forEachOf(result.Contents, iteratorFcn, doneIteratingFcn);
    // });
}
var moveFile = async function (bucketName, targetBucketName, filePath, fileName, targetfolder) {
    return new Promise(async (resolve, reject) => {
        const copyparams = {
            Bucket: targetBucketName,
            ACL: 'public-read',
            CopySource: bucketName + "/" + filePath,
            Key: targetfolder + "/" + fileName
        };
        await S3Manager.copyObject(copyparams, (err, result) => {
            if (err) {
                return resolve(false);
            } else {
                return resolve(true);
            }
        });
    });
}


module.exports = FileService;