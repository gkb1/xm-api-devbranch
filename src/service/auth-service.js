const jwt = require('jsonwebtoken');
const bcrypt = require('bcrypt');
const _ = require('lodash');
const constant = require('./../utils/constant');
const res = require('../utils/custom-response');
const userDAO = require('../dao/user-dao');
const utils = require('./../utils/utilities');
const communicationHelper = require('./../utils/communication_helper');
const communicationUtility = require('./../utils/communication');
const request = require('request');



// CHECKING USER EXISTENCE IN GKBPAY 
const promiseRequest = function (methodName, url, json) {
    const options = {
        method: methodName,
        headers: {
            "content-type": "application/json",
        },
        url: constant.GKB_PAY_BACKEND_URL + url,
        json: json,
    };
    return new Promise((resolve, reject) => {
        request(options, (error, response, body) => {
            if (error) {
                return reject(error);
            }
            return resolve(body);
        });
    });
};


const AuthService = {
    saltRounds: 10,
    async signIn(userCredentials) {
        try {
            if (userCredentials == null) {
                return Promise.reject(res.error(constant.HTML_STATUS_CODE.UNAUTHORIZED, 'Invalid User Data.'));
            }
            let userData;
            switch (userCredentials.mobile) {
                case userCredentials.mobile:
                    userData = await userDAO.getOne({mobile: userCredentials.mobile});
                    if (userData == null) {
                        return Promise.reject(res.error(constant.HTML_STATUS_CODE.INVALID_DATA, 'Mobile number does not exists'));
                    }
                    // //CHECKING UNIQUENESS OF USERS FROM GKBPAY DB  
                    // const payload = { mobile: userCredentials.mobile }
                    // const resp = await promiseRequest('POST', 'users/checkIfUserRegistered', payload)
                    // if (resp) {
                    //     return Promise.reject(res.error(constant.HTML_STATUS_CODE.CONFLICT, `Mobile number doesnot exists`));
                    // }
                    if (userData.auditFields.isActive === false) {
                        return Promise.reject(res.error(constant.HTML_STATUS_CODE.INVALID_DATA, 'To activate your account. please contact system admin!'));
                    }
                    if (!userData.otp || (utils.decrypt(userData.otp) !== (userCredentials.otp.toString()))) {
                        return Promise.reject(res.error(constant.HTML_STATUS_CODE.INVALID_DATA, 'Invalid OTP.'));
                    }
                    if (!userData.otpExpiresIn || userData.otpExpiresIn < new Date()) {
                        await userDAO.removeOTP(userData._id);
                        return Promise.reject(res.error(constant.HTML_STATUS_CODE.INVALID_DATA, 'OTP Expired!'));
                    }
                    break; 
            }
            const token = jwt.sign(_.pick(userData, ['_id', 'name', 'gender', 'mobile', 'email', 'role']), constant.APP_SECRETE, {
                expiresIn: constant.TOKEN_TIMEOUT
            });
            return { token, name: userData.name, mobile: userData.mobile, email: userData.email, _id: userData._id, role: userData.role, gender: userData.gender };
        } catch (error) {
            return Promise.reject(res.error(error.message, error.stack));
        }
    },
    async generateOTP(userDetails) {
        try {
            if (userDetails == null) {
                return Promise.reject(res.error(constant.HTML_STATUS_CODE.INVALID_DATA, 'Invalid User Data.'));
            }
            const userData = await userDAO.getOne({mobile: userDetails.mobile}, null);
            if (userData == null) {
                return Promise.reject(res.error(constant.HTML_STATUS_CODE.INVALID_DATA, 'User does not exist.'));
            }
            if (userData.auditFields.isActive === false) {
                return Promise.reject(res.error(constant.HTML_STATUS_CODE.INVALID_DATA, 'Account is not activated !! Please contact system admin!'));
            }
            const randomNo = Math.floor(100000 + Math.random() * 900000);
            const otp = utils.encrypt(randomNo.toString());
            const otpExpiresIn = utils.otpExpireTime();
            console.log('OTP', randomNo);
            const createdUser = await userDAO.update(userData._id, { otp: otp, otpExpiresIn: otpExpiresIn });
            let smsBody = communicationHelper.sendOTPSMS(userData.name, randomNo);
            await communicationUtility.sendSMS(userData.mobile, smsBody, 'OTP');
            // var str = userData.mobile.toString().replace(/.(?=.{4})/g, '*');
            return Promise.resolve({ message: "OTP Sent to your mobile" });
        } catch (error) {
            return Promise.reject(res.error(constant.HTML_STATUS_CODE.INTERNAL_ERROR, error.message, error.stack));
        }
    },

};
module.exports = AuthService;