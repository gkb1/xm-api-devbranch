const _ = require('lodash');
const constant = require('./../utils/constant');
const res = require('../utils/custom-response');
const userDAO = require('../dao/user-dao');
const communicationHelper = require('./../utils/communication_helper');
const communicationUtility = require('./../utils/communication');
const utils = require('./../utils/utilities');
const jwt = require('jsonwebtoken');
const request = require('request');


String.prototype.lpad = function (padString, length) {
    let str = this;
    while (str.length < length)
        str = padString + str;
    return str;
};

// CHECKING USER EXISTENCE IN GKBPAY 
const promiseRequest = function (methodName, url, json) {
    const options = {
        method: methodName,
        headers: {
            "content-type": "application/json",
        },
        url: constant.GKB_PAY_BACKEND_URL + url,
        json: json,
    };
    return new Promise((resolve, reject) => {
        request(options, (error, response, body) => {
            if (error) {
                return reject(error);
            }
            return resolve(body);
        });
    });
};

const AuthService = {
    saltRounds: 10,
    /**
     * Create User, Check validation for required fields
     * params request body with User's data in JSON (email, username, password, role)
     * return user data if user data is saved.
     */
    async createUser(userDetail) {
        try {
            const checkIfUserEmailAlreadyExist = await userDAO.findOneByCondition('email', userDetail.email);
            if (checkIfUserEmailAlreadyExist) {
                return Promise.reject(res.error(constant.HTML_STATUS_CODE.CONFLICT, 'User already exist with same Email id.'));
            }
            const checkIfUserMobileAlreadyExist = await userDAO.findOneByCondition('mobile', userDetail.mobile);
            if (checkIfUserMobileAlreadyExist) {
                return Promise.reject(res.error(constant.HTML_STATUS_CODE.CONFLICT, 'Mobile number is already registered !!'));
            }

            //CHECKING UNIQUENESS OF USERS FROM GKBPAY DB  
            const payload = { email: userDetail.email, mobile: userDetail.mobile }
            const resp = await promiseRequest('POST', 'users/checkIfUserRegistered', payload)
            if (resp) {
                return Promise.reject(res.error(constant.HTML_STATUS_CODE.CONFLICT, 'Already registered.'));
            }
            // ISACTIVATE FALSE FOR FRAME COLLECTOR & MODERATOR
            if (userDetail.role === constant.lookup.FRAME_MODERATOR || userDetail.role === constant.lookup.FRAME_COLLECTOR_ROLE) {
                userDetail.auditFields = {};
                userDetail.auditFields['isActive'] = false
            }

            // FROM GKB_PAY DB FOR ECP_CUSTOMER ROLE
            if (userDetail.role === constant.lookup.ECP_CUSTOMER_ROLE) {
                const payload = {};
                payload['conditions'] = { role: userDetail.role, mobile: userDetail.mobile }
                const ecpUser = await promiseRequest('POST', 'users/getSelUserDetailsByCond', payload)
                if (ecpUser) {
                    const ecpCust = {
                        _id: ecpUser._id,
                        userId: ecpUser.userId,
                        cust_code: ecpUser.cust_code,
                        store: ecpUser.store,
                    }
                    userDetail['ecpCust'] = ecpCust;
                    userDetail.store_code = utils.getRandomNumber(3);
                }
            }
            // PENDING create cust code
            if (!userDetail['userId']) {
                userDetail['userId'] = utils.getRandomString(6);
                let checkUnique = await userDAO.getOne({ "userId": userDetail['userId'] });
                while (checkUnique) {
                    userDetail['userId'] = utils.getRandomString(6);
                    checkUnique = await userDAO.getOne({ "userId": userDetail['userId'] });
                }
            }
            if (!userDetail.role) { // default CUSTOMER ROLE
                userDetail.role = constant.lookup.CUSTOMER_ROLE;
            }
            // OTP SECTION
            // randomNo = Math.floor(100000 + Math.random() * 900000);
            // userDetail['otp'] = utils.encrypt(randomNo.toString());
            // userDetail['otpExpiresIn'] = utils.otpExpireTime();
            const createdUser = await userDAO.create(userDetail);
            if (createdUser == null) {
                return Promise.reject(res.error(constant.HTML_STATUS_CODE.INTERNAL_ERROR, 'Something went wrong with Creating user'));
            }
            // let smsBody = communicationHelper.sendOTPSMS(createdUser.name, randomNo);
            // await communicationUtility.sendSMS(createdUser.mobile, smsBody, 'OTP');
            // Sending created user detail with specific details
            return _.pick(createdUser, ['_id', 'userId', 'role', 'name', 'email', 'mobile', 'gender']);
        } catch (error) {
            return Promise.reject(res.error(constant.HTML_STATUS_CODE.INTERNAL_ERROR, error.message, error.stack));
        }
    },
    async generateAndSendOTP(body) {
        try {
            if (!body || !body.mobile) {
                return Promise.reject(res.error(constant.HTML_STATUS_CODE.INVALID_DATA, 'Invalid Mobile No'));
            }
            const userObj = await userDAO.getOne({ mobile: body.mobile });
            if (userObj == null) {
                return Promise.reject(res.error(constant.HTML_STATUS_CODE.INVALID_DATA, 'Invalid Mobile No'));
            }
            if (!body.supportRole && userObj.role === constant.lookup.CUST_SUPPORT_ROLE) {
                return Promise.reject(res.error(constant.HTML_STATUS_CODE.INVALID_DATA, 'Unauthorized Access !!'));
            }
            if (userObj.role === constant.lookup.ECP_CUSTOMER_ROLE) {
                if (userObj.isVerified.accountVerified === 0) { // not eligible to login
                    return Promise.reject(res.error(constant.HTML_STATUS_CODE.INVALID_DATA, 'Account Not Yet Verified !!'));
                }
            }
            const randomNo = Math.floor(100000 + Math.random() * 900000);
            const hashedOTP = utils.encrypt(randomNo.toString());
            if (hashedOTP == null) {
                return Promise.reject(res.error(constant.HTML_STATUS_CODE.INTERNAL_ERROR, 'Something went wrong !!'));
            }
            // Update User otp with hashed otp ...
            await userDAO.updateByCondition({ userId: userObj.userId }, { otp: hashedOTP, fcmToken: body.fcmToken ? body.fcmToken : '' });
            console.log(randomNo)
            let smsBody = communicationHelper.sendOTPSMS(userObj.name, randomNo);
            await communicationUtility.sendSMS(userObj.mobile, smsBody, 'OTP');

            // let emailOptions = communicationHelper.sendOTPEmail(userObj.email, userObj.name, otp);
            // await communicationUtility.sendMail(emailOptions);
            return Promise.resolve({ status: constant.HTML_STATUS_CODE.SUCCESS, message: 'OTP sent successfully !!' });

        } catch (error) {
            return Promise.reject(res.error(constant.HTML_STATUS_CODE.INTERNAL_ERROR, error.message, error.stack));
        }
    },
    async verifyOtp(params) {
        try {

            if (!params) {
                return Promise.reject(res.error(constant.HTML_STATUS_CODE.INVALID_DATA, 'Invalid Payload'));
            }
            if (!params.mobile) {
                return Promise.reject(res.error(constant.HTML_STATUS_CODE.INVALID_DATA, 'Invalid Mobile No !!'));
            }
            if (!params.otpEntered) {
                return Promise.reject(res.error(constant.HTML_STATUS_CODE.INVALID_DATA, 'Invalid OTP !!'));
            }
            const userObj = await userDAO.getOne({ mobile: params.mobile });
            if (userObj == null) {
                return Promise.reject(res.error(constant.HTML_STATUS_CODE.INVALID_DATA, 'Invalid User'));
            }
            if (userObj.auditFields.isActive === false) {
                return Promise.reject(res.error(constant.HTML_STATUS_CODE.INVALID_DATA, 'Account is not activated !! Please contact system admin!'));
            }
            if (utils.decrypt(userObj.otp) !== params.otpEntered.trim()) {
                return Promise.reject(res.error(constant.HTML_STATUS_CODE.INVALID_DATA, 'Please enter valid OTP and try again !!'));
            }
            let token;
            token = jwt.sign(_.pick(userObj, ['_id', 'userId', 'role', 'name', 'email', 'phone', 'mobile', 'cust_code', 'address', 'store', 'isVerified', 'fcmToken']), constant.APP_SECRETE, {
                expiresIn: userObj.role === constant.lookup.ECP_CUSTOMER_ROLE ? '30d' : constant.TOKEN_TIMEOUT
            });
            return {
                token, user: {
                    name: (userObj.name) ? userObj.name : '', mobile: userObj.mobile, phone: (userObj.phone) ? userObj.phone : '',
                    email: userObj.email, _id: userObj._id, role: userObj.role, userId: userObj.userId,
                    isVerified: userObj.isVerified, address: userObj.address,
                    cust_code: (userObj.cust_code) ? userObj.cust_code : '',
                    store: (userObj.store) ? userObj.store : '',
                    fcmToken: (userObj.fcmToken) ? userObj.fcmToken : ''
                }
            };
        } catch (error) {
            return Promise.reject(res.error(constant.HTML_STATUS_CODE.INTERNAL_ERROR, error.message, error.stack));
        }
    },
    // async updateUser(userDetail, user) {
    //     try {

    //         if (userDetail == null || userDetail.userId == null) {
    //             return Promise.reject(res.error(constant.HTML_STATUS_CODE.INVALID_DATA, 'Invalid User Data'));
    //         }
    //         let userObj = {
    //             name: userDetail.name,
    //             email: userDetail.email,
    //             mobile: userDetail.mobile,
    //             address: userDetail.address
    //         }
    //         const updatedUser = await userDAO.update(user.id, userObj);
    //         return Promise.resolve(updatedUser);
    //     } catch (error) {
    //         return Promise.reject(res.error(constant.HTML_STATUS_CODE.INTERNAL_ERROR, error.message, error.stack));
    //     }
    // },

    deleteUser(id, user) {
        return new Promise((resolve, reject) => {
            try {
                if (user.role !== constant.ROLE.ADMIN) {
                    return reject(res.error(constant.HTML_STATUS_CODE.UNAUTHORIZED, 'Unauthorized access. Only admin can delete users'));
                }
                if (id == null) {
                    return reject(res.error(constant.HTML_STATUS_CODE.INVALID_DATA, 'User id is required'));
                }
                if (id === user._id) {
                    return reject(res.error(constant.HTML_STATUS_CODE.UNAUTHORIZED, 'Unauthorized access'));
                }
                userDAO.delete(id).then((res) => {
                    resolve(true);
                }).catch((e) => {
                    throw e;
                });
            } catch (error) {
                return reject(res.error(constant.HTML_STATUS_CODE.INTERNAL_ERROR, error.message, error.stack));
            }
        });

    },
    async getAll(bodyData, user) {
        // return new Promise((resolve, reject) => {
        try {
            let limit;
            let skip;
            let page;
            // let condition = bodyData.condition;
            let pagination = bodyData.pagination;
            if (pagination) {
                limit = isNaN(Number(pagination.limit)) ? 10 : Number(pagination.limit);
                page = Number(pagination.page) || 1;
                skip = (pagination.page ? pagination.limit * (pagination.page - 1) : 0);
            } else {
                limit = 0;
                skip = 0;
            }
            if (!bodyData.conditions) {
                bodyData.conditions = {};
            }
            const users = await userDAO.getAllUsers(limit, skip, bodyData, bodyData.conditions);
            if (bodyData.showOtp) { // if otp
                if (users.users.length > 0) {
                    for (let i = 0; i < users.users.length; i++) {
                        users.users[i].otp = utils.decrypt(users.users[i].otp);
                    }
                }
            }
            if (!users.data.length && users.count == 0) {
                return Promise.reject(res.error(constant.HTML_STATUS_CODE.NOT_FOUND, 'user doesnot exist!'));
            }
            return Promise.resolve(Object.assign(users, { limit, page }));
        } catch (error) {
            return Promise.reject(res.error(constant.HTML_STATUS_CODE.INTERNAL_ERROR, error.message, error.stack));
        }
        // });
    },
    async getSelUserByCond(bodyData, user) {
        try {
            if (!user) {
                return Promise.reject(res.error(constant.HTML_STATUS_CODE.INVALID_DATA, 'Invalid user data'));
            }
            if (!bodyData || !bodyData.conditions) {
                return Promise.reject(res.error(constant.HTML_STATUS_CODE.INVALID_DATA, 'Invalid Payload'));
            }
            const userData = await userDAO.getOne(bodyData.conditions, { otp: 0, otpExpiresIn: 0 })
            if (!userData) {
                return Promise.reject(res.error(constant.HTML_STATUS_CODE.INVALID_DATA, 'user doesnot exist!'));
            }
            return userData;
        } catch (error) {
            return Promise.reject(res.error(constant.HTML_STATUS_CODE.INTERNAL_ERROR, error.message, error.stack));
        }
    },
    async getById(user) {
        try {
            if (!user) {
                return Promise.reject(res.error(constant.HTML_STATUS_CODE.INVALID_DATA, 'Invalid user data'));
            }
            const userData = await userDAO.getOne({ _id: user.id }, { otp: 0, otpExpiresIn: 0 })
            if (!userData) {
                return Promise.reject(res.error(constant.HTML_STATUS_CODE.INVALID_DATA, 'user doesnot exist!'));
            }
            return userData;
        } catch (error) {
            return Promise.reject(res.error(constant.HTML_STATUS_CODE.INTERNAL_ERROR, error.message, error.stack));
        }
    },

    getAllUser(limit, page, conditions) {
        return new Promise((resolve, reject) => {
            try {
                limit = isNaN(Number(limit)) ? 10 : Number(limit);
                page = Number(page) || 1;
                const skip = (page ? limit * (page - 1) : 0);
                // Restrict for student
                conditions = { 'role': { $ne: 'STUDENT' } };
                userDAO.getAllUsers(limit, skip, conditions).then((users) => {
                    resolve(Object.assign(users, { limit, page }));
                }).catch((error) => {
                    throw error;
                });
            } catch (error) {
                return reject(res.error(constant.HTML_STATUS_CODE.INTERNAL_ERROR, error.message, error.stack));
            }
        });
    },

    getUser(user) {
        return new Promise((resolve, reject) => {
            try {
                userDAO.getByEmail(user.email).then((user) => {
                    if (user && user.role === constant.ROLE.TRAINER) {
                        trainerService.getByUser(user._id).then((data) => {
                            const ss = _.pick(user, ['_id', 'username', 'role', 'name', 'email', 'phone', 'auditFields']);
                            ss['trainerDetail'] = data._doc;
                            ss['userId'] = {};
                            ss['userId'] = Object.assign({}, ss._id);
                            ss._id = data._doc._id;
                            // resolve(Object.assign(ss, data._doc));
                            resolve(ss);
                            // resolve(_.pick(user, ['_id', 'username', 'role', 'name', 'email', 'auditFields']));
                        }).catch((error) => {
                            throw error;
                        })
                    } else if (user && user.role === constant.ROLE.STUDENT) {
                        studentService.getByUser(user._id).then((data) => {
                            const ss = _.pick(user, ['_id', 'username', 'role', 'name', 'email', 'phone', 'auditFields']);
                            ss['studentDetail'] = data._doc;
                            ss['userId'] = {};
                            ss['userId'] = Object.assign({}, ss._id);
                            ss._id = data._doc._id;
                            resolve(ss);
                            // resolve(_.pick(user, ['_id', 'username', 'role', 'name', 'email', 'auditFields']));
                        }).catch((error) => {
                            throw error;
                        })
                    } else {
                        resolve(_.pick(user, ['_id', 'username', 'role', 'name', 'email', 'phone', 'auditFields']));
                    }
                }).catch((error) => {
                    throw error;
                });
            } catch (error) {
                return reject(res.error(constant.HTML_STATUS_CODE.INTERNAL_ERROR, error.message, error.stack));
            }
        });
    },
    async updateUser(userUpdateDetail, user) {
        try {

            if (!user) {
                return Promise.reject(res.error(constant.HTML_STATUS_CODE.INVALID_DATA, 'Invalid User'));
            }
            const userObj = await userDAO.getOne({ _id: user.id });
            if (userObj === null) {
                return Promise.reject(res.error(constant.HTML_STATUS_CODE.INVALID_DATA, 'Invalid User'));
            }
            // non-update fields
            if (userUpdateDetail.otp) {
                delete userUpdateDetail.otp;
            }
            if (userUpdateDetail.userId) {
                delete userUpdateDetail.userId;
            }
            if (userUpdateDetail._id) {
                delete userUpdateDetail._id;
            }
            if (userUpdateDetail.role) {
                delete userUpdateDetail.role;
            }
            const updateUser = await userDAO.updateByCondition({ _id: user.id }, userUpdateDetail);
            return Promise.resolve({ status: constant.HTML_STATUS_CODE.SUCCESS, message: 'Updated successfully !!' });
        } catch (error) {
            return Promise.reject(res.error(constant.HTML_STATUS_CODE.INTERNAL_ERROR, error.message, error.stack));
        }
    },
    async activateUser(userUpdateDetail, user, userId) {
        try {
            if (user.role === constant.lookup.ADMIN_ROLE || user.role === constant.lookup.SUPERADMIN_ROLE) {
                const user = await userDAO.getOne({ userId: userId });
                if (user === null) {
                    return Promise.reject(res.error(constant.HTML_STATUS_CODE.INVALID_DATA, 'Invalid User'));
                }
                const updateUser = await userDAO.updateByCondition({ userId: userId }, userUpdateDetail);
                if (updateUser.n > 0 || updateUser.nModified > 0) {
                    if (userUpdateDetail.auditFields.isActive) {
                        return Promise.resolve({ status: constant.HTML_STATUS_CODE.SUCCESS, message: 'User activated successfully!!' });
                    } else {
                        return Promise.resolve({ status: constant.HTML_STATUS_CODE.SUCCESS, message: 'User de-activated successfully!!' });
                    }
                }
            }
        } catch (error) {
            return Promise.reject(res.error(constant.HTML_STATUS_CODE.INTERNAL_ERROR, error.message, error.stack));
        }
    },
    async getUserNotificationList(bodyData, user) {
        try {
            if (!bodyData.conditions) {
                bodyData.conditions = {};
            }
            if (!bodyData.conditions.userId) {
                return Promise.reject(res.error(constant.HTML_STATUS_CODE.INVALID_DATA, 'Invalid User Id'));
            }
            if (!user) {
                return Promise.reject(res.error(constant.HTML_STATUS_CODE.INVALID_DATA, 'Invalid User'));
            }
            if (bodyData.conditions.userId !== user.userId) {
                return Promise.reject(res.error(constant.HTML_STATUS_CODE.INVALID_DATA, 'Invalid User Id'));
            }
            let limit = 0;
            let skip = 0;
            let page;
            let pagination = bodyData.pagination;
            if (pagination) {
                limit = isNaN(Number(pagination.limit)) ? 10 : Number(pagination.limit);
                page = Number(pagination.page) || 1;
                skip = (pagination.page ? pagination.limit * (pagination.page - 1) : 0);
            }
            if (limit <= 0) {
                limit = 0;
            }
            const userNotifications = await userDAO.getAllUserNotification(limit, skip, bodyData.conditions);
            return Promise.resolve(Object.assign(userNotifications, { limit, page }));
        } catch (error) {
            return Promise.reject(res.error(constant.HTML_STATUS_CODE.INTERNAL_ERROR, error.message, error.stack));
        }
    },

    // FRAME COLLECTOR

    async getAllStores(user) {
        try {
            if (user == null) {
                return Promise.reject(res.error(constant.HTML_STATUS_CODE.INVALID_url, 'Invalid url!.'));
            }
            if (user.role === constant.lookup.FRAME_COLLECTOR_ROLE) {
                const userData = await userDAO.getStoresWithPopulatedData({ userId: user.userId }, { otp: 0, otpExpiresIn: 0 })
                if (!userData) {
                    return Promise.reject(res.error(constant.HTML_STATUS_CODE.INVALID_DATA, 'user doesnot exist!'));
                }
                return _.pick(userData, ['_id', 'name', 'email', 'mobile', 'userId', 'mystores']);
            } else {
                return Promise.reject(res.error(constant.HTML_STATUS_CODE.FORBIDDEN, 'You do not have permission to perform this action'))
            }
        } catch (error) {
            return Promise.reject(res.error(constant.HTML_STATUS_CODE.INTERNAL_ERROR, error.message, error.stack));
        }
    },

    async AssignStoreToFrameCollector(userData, user) {
        try {
            if (userData == null) {
                return Promise.reject(res.error(constant.HTML_STATUS_CODE.INVALID_url, 'Invalid url!.'));
            }
            if (user.role === constant.lookup.ADMIN_ROLE || user.role === constant.lookup.SUPERADMIN_ROLE) {
                const createdFrameCollector = await userDAO.updateStore({ userId: userData.frame_collector_userId }, userData);
                return Promise.resolve(createdFrameCollector);
            } else {
                return Promise.reject(res.error(constant.HTML_STATUS_CODE.FORBIDDEN, 'You do not have permission to perform this action'))
            }
        } catch (error) {
            return Promise.reject(res.error(constant.HTML_STATUS_CODE.INTERNAL_ERROR, error.message, error.stack));
        }
    },

    async deleteStoreFromframeCollector(userData, user) {
        try {
            if (userData == null) {
                return Promise.reject(res.error(constant.HTML_STATUS_CODE.INVALID_DATA, 'Invalid url!.'));
            }
            // if (user.role === constant.lookup.ADMIN_ROLE || user.role === constant.lookup.SUPERADMIN_ROLE) {
            const objId = await userDAO.getOne({ userId: userData.frameCollectorUserId });
            if (objId == null) {
                return Promise.reject(res.error(constant.HTML_STATUS_CODE.INVALID_DATA, 'No data found.'));
            }
            const result = await userDAO.removeStore(userData);
            return result;
            // } else {
            //     return Promise.reject(res.error(constant.HTML_STATUS_CODE.FORBIDDEN, 'You do not have permission to perform this action'))
            // }
        } catch (error) {
            return Promise.reject(res.error(constant.HTML_STATUS_CODE.INTERNAL_ERROR, error.message, error.stack));
        }
    },

    // async changePassword(passwordDetail, user) {
    //     try {
    //         // check with old password
    //         if (passwordDetail == null || passwordDetail.oldPassword == null || passwordDetail.newPassword == null) {
    //             return Promise.reject(res.error(constant.HTML_STATUS_CODE.INVALID_DATA, 'To change password you need to send oldPassword and newPassword.'));
    //         }
    //         const userDetail = await userDAO.getById(user._id);
    //         if (userDetail == null) {
    //             return Promise.reject(res.error(constant.HTML_STATUS_CODE.INVALID_DATA, 'Invalid User'));
    //         }
    //         const isPasswordSame = await bcrypt.compare(passwordDetail.oldPassword, userDetail.password);
    //         if (!isPasswordSame) {
    //             return Promise.reject(res.error(constant.HTML_STATUS_CODE.INVALID_DATA, 'Invalid Old Password'));
    //         }
    //         userDetail.password = await bcrypt.hash(passwordDetail.newPassword, this.saltRounds);
    //         const updatedUser = await userDAO.update(user._id, userDetail);
    //         let smsBody = communicationHelper.changePasswordSMS(user.name);
    //         communicationUtility.sendSMS(user.phone, smsBody);
    //         let emailOptions = communicationHelper.studentChangedPasswordEmail(user.email, user.name);
    //         communicationUtility.sendMail(emailOptions);
    //         return Promise.resolve(updatedUser);
    //     } catch (error) {
    //         return reject(res.error(constant.HTML_STATUS_CODE.INTERNAL_ERROR, error.message, error.stack));
    //     }
    // },

    // async resetPassword(passwordDetail) {
    //     try {
    //         // check with old password
    //         if (passwordDetail == null || passwordDetail.oldPassword == null || passwordDetail.newPassword == null) {
    //             return Promise.reject(res.error(constant.HTML_STATUS_CODE.INVALID_DATA, 'To change password you need to send oldPassword and newPassword.'));
    //         }
    //         const userDetail = await userDAO.getById(user._id);
    //         if (userDetail == null) {
    //             return Promise.reject(res.error(constant.HTML_STATUS_CODE.INVALID_DATA, 'Invalid User'));
    //         }
    //         const isPasswordSame = await bcrypt.compare(passwordDetail.oldPassword, userDetail.password);
    //         if (!isPasswordSame) {
    //             return Promise.reject(res.error(constant.HTML_STATUS_CODE.INVALID_DATA, 'Invalid Old Password'));
    //         }
    //         userDetail.password = await bcrypt.hash(passwordDetail.newPassword, this.saltRounds);
    //         const updatedUser = await userDAO.update(user._id, userDetail);
    //         return Promise.resolve(true);
    //     } catch (error) {
    //         return reject(res.error(constant.HTML_STATUS_CODE.INTERNAL_ERROR, error.message, error.stack));
    //     }
    // },

    // async update(userDetail, user) {
    //     try {
    //         if (userDetail._id == null || userDetail.role == null) {
    //             return Promise.reject(res.error(constant.HTML_STATUS_CODE.INVALID_DATA, 'Invalid Data. _id and role in mandatory'));
    //         }
    //         const userData = await userDAO.getById(userDetail._id);
    //         if (userData == null) {
    //             return Promise.reject(res.error(constant.HTML_STATUS_CODE.INVALID_DATA, 'Invalid _id.'));
    //         }
    //         // Only same user can update own record or Admin can update
    //         if (userData.role !== constant.ROLE.STUDENT && userDetail.role !== user.role && user.role !== constant.ROLE.ADMIN) {
    //             return Promise.reject(res.error(constant.HTML_STATUS_CODE.UNAUTHORIZED, 'Unauthorized access.'));
    //         }

    //         // Only admin can update ADMIN
    //         if (userDetail.role === constant.ROLE.ADMIN && user.role !== constant.ROLE.ADMIN) {
    //             return Promise.reject(res.error(constant.HTML_STATUS_CODE.UNAUTHORIZED, 'Unauthorized access.'));
    //         }

    //         // Only Student/Admin can update STUDENT
    //         if (userDetail.role === constant.ROLE.STUDENT && user.role !== constant.ROLE.STUDENT && user.role !== constant.ROLE.ADMIN && user.role !== constant.ROLE.COUNSELLOR) {
    //             return Promise.reject(res.error(constant.HTML_STATUS_CODE.UNAUTHORIZED, 'Unauthorized access.'));
    //         }

    //         // Check id user with same email or username is present or not...
    //         const alreadyExistUser = await Promise.all([userDAO.getByEmail(userDetail.email), userDetail.username ? userDAO.getByUserName(userDetail.username) : Promise.resolve(false)]);
    //         if (alreadyExistUser[0] || alreadyExistUser[1]) {
    //             return Promise.reject(res.error(constant.HTML_STATUS_CODE.CONFLICT, 'User already exist with same ${alreadyExistUser[0] ? 'Email id' : (alreadyExistUser[1] ? 'Username' : 'detail')}'));
    //         }

    //         if (userDetail.role === constant.ROLE.STUDENT) {
    //             userDetail.password = Math.random().toString(36).substring(7);
    //             console.log(userDetail.password);
    //         }
    //         const oldPassword = userDetail.password;
    //         const hashedPassword = await bcrypt.hash(userDetail.password, this.saltRounds);
    //         if (hashedPassword == null) {
    //             return Promise.reject(res.error(constant.HTML_STATUS_CODE.INTERNAL_ERROR, 'Something went wrong with password hashing'));
    //         }
    //         userDetail.password = hashedPassword;
    //         const updatedRecord = await userDAO.update(userDetail._id, userDetail);
    //         // User is Updated, we can send verification link to user's email.
    //         let smsBody = communicationHelper.getWelcomeSMS(userDetail.role);
    //         communicationUtility.sendSMS(userDetail.phone, smsBody);
    //         let emailOptions = communicationHelper.getWelcomeEmail(userDetail.email, userDetail.role, userData.username, oldPassword);
    //         communicationUtility.sendMail(emailOptions);

    //         return updatedRecord;

    //     } catch (error) {
    //         return Promise.reject(res.error(constant.HTML_STATUS_CODE.INTERNAL_ERROR, error.message, error.stack));
    //     }

    // },

    // setFCM(fcmId, user) {
    //     try {
    //         return userDAO.setFCM(user._id, fcmId);
    //     } catch (error) {
    //         return Promise.reject(res.error(constant.HTML_STATUS_CODE.INTERNAL_ERROR, error.message, error.stack));
    //     }
    // }







};


module.exports = AuthService;