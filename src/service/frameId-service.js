
const _ = require('lodash');
const constant = require('../utils/constant');
const res = require('../utils/custom-response');
const frameDAO = require('../dao/frameId-dao');
const file = require('../service/file-service');

const frameIdService = {

    async createFrameId(req, user) {
        try {
            if (req == null || user == null) {
                return Promise.reject(res.error(constant.HTML_STATUS_CODE.INVALID_url, 'Invalid url!.'));
            }
            if (user.role !== constant.lookup.FRAME_COLLECTOR_ROLE) {
                return Promise.reject(res.error(constant.HTML_STATUS_CODE.FORBIDDEN, 'You do not have permission to perform this action'))
            }
            const storeExist = await frameDAO.getOne({ 'store.cust_code': req.body['cust_code'] });
            if (storeExist) { // UPDATE
                const frameIdExist = [];
                storeExist.store.frame.filter(function (value) {
                    if (value.frameID === req.body['frameId']) {
                        frameIdExist.push(value.frameId);
                        // return Promise.reject(res.error(constant.HTML_STATUS_CODE.CONFLICT, 'Frame id already exists!'));
                        // return;
                    }
                });
                if (!frameIdExist.length) {
                    if (req.files.media) {
                        req.body['media'] = await file.uploadToS3(req.files.media);
                    }
                    req.body['frameCollectorId'] = user._id;
                    req.body['tagName'] = req.body['cust_code'] + '-' + req.body.frameId
                    const updateFields = {
                        'store.frame': {
                            frameCollectorId: req.body['frameCollectorId'],
                            frameID: req.body['frameId'],
                            tagName: req.body['tagName'],
                            media: req.body['media']
                        }
                    }
                    const result = await frameDAO.update({ 'store.cust_code': req.body['cust_code'] }, updateFields);
                    return Promise.resolve(result);
                } else {
                    return Promise.reject(res.error(constant.HTML_STATUS_CODE.CONFLICT, 'Frame id already exists!'));
                }
            } else { //CREATE
                if (req.files.media) {
                    req.body['media'] = await file.uploadToS3(req.files.media);
                }
                req.body['frameCollectorId'] = user._id;
                req.body['tagName'] = req.body['cust_code'] + '-' + req.body.frameId
                const result = await frameDAO.create(req.body);
                return Promise.resolve(result);
            }
        } catch (error) {
            return Promise.reject(res.error(constant.HTML_STATUS_CODE.INTERNAL_ERROR, error.message, error.stack));
        }
    },
    async getFrameById(loggedInUser, frameId) {
        try {
            if (loggedInUser == null || frameId == null) {
                return Promise.reject(res.error(constant.HTML_STATUS_CODE.INVALID_url, 'Invalid url!.'));
            }
            const result = await frameDAO.getOne({ 'store.userId': frameId });
            if (result == null) {
                return Promise.reject(res.error(constant.HTML_STATUS_CODE.INVALID_url, `No frameId found.`));
            }
            return result;
        } catch (error) {
            return Promise.reject(res.error(constant.HTML_STATUS_CODE.INTERNAL_ERROR, error.message, error.stack));
        }
    },
    async listAllFrameId(frameData) {
        try {
            if (frameData == null) {
                return Promise.reject(res.error(constant.HTML_STATUS_CODE.INVALID_url, 'Invalid url!.'));
            }
            let limit;
            let skip;
            let page;
            let pagination = frameData.pagination;
            if (frameData.conditions.filter) {
                const result = await frameDAO.getAllFrameIdStatusFilter(frameData.conditions.filter);
                return result;
            }
            if (pagination) {
                limit = isNaN(Number(pagination.limit)) ? 10 : Number(pagination.limit);
                page = Number(pagination.page) || 1;
                skip = (pagination.page ? pagination.limit * (pagination.page - 1) : 0);
            } else {
                limit = 30; // default
                skip = 0;
            }

            frameData.conditions['auditFields.isDeleted'] = false;
            const result = await frameDAO.getAll(limit, skip, frameData.sort, frameData.conditions);
            return result;
        } catch (error) {
            return Promise.reject(res.error(constant.HTML_STATUS_CODE.INTERNAL_ERROR, error.message, error.stack));
        }
    },
    async updateFrameId(params, prodDetails) {
        try {
            if (prodDetails == null || params == null) {
                return Promise.reject(res.error(constant.HTML_STATUS_CODE.INVALID_DATA, 'Invalid payload !!.'));
            }
            const objId = await frameDAO.getOne({ 'store.ecpUserId': params.ecpUserId });
            if (objId == null) {
                return Promise.reject(res.error(constant.HTML_STATUS_CODE.INVALID_DATA, `No frameId found.`));
            }
            const result = await frameDAO.updateFrameIdStatus(params, prodDetails);
            return result;
        } catch (error) {
            return Promise.reject(res.error(constant.HTML_STATUS_CODE.INTERNAL_ERROR, error.message, error.stack));
        }
    },
    async updateAllFrameStatusOfEcp(user, params, bodyData) {
        try {
            if (bodyData == null || params == null) {
                return Promise.reject(res.error(constant.HTML_STATUS_CODE.INVALID_DATA, 'Invalid payload !!.'));
            }
            if (!params.ecpUserId || !user) {
                return Promise.reject(res.error(constant.HTML_STATUS_CODE.INVALID_DATA, 'Invalid User !!.'));
            }
            const objId = await frameDAO.getOne({ 'store.ecpUserId': params.ecpUserId });
            if (objId == null) {
                return Promise.reject(res.error(constant.HTML_STATUS_CODE.INVALID_DATA, `No frameId found.`));
            }
            const result = await frameDAO.updateFrameIdStatus(params, bodyData);
            return result;
        } catch (error) {
            return Promise.reject(res.error(constant.HTML_STATUS_CODE.INTERNAL_ERROR, error.message, error.stack));
        }
    },
    async deleteFrameId(frameData, user) {
        try {
            if (frameData == null) {
                return Promise.reject(res.error(constant.HTML_STATUS_CODE.INVALID_DATA, 'Invalid url!.'));
            }
            const objId = await frameDAO.getOne({ 'store.userId': frameData.frameId });
            if (objId == null) {
                return Promise.reject(res.error(constant.HTML_STATUS_CODE.INVALID_DATA, `No frameId found.`));
            }
            const deleteFrameCondition = {}
            deleteFrameCondition['auditFields.isDeleted'] = true;
            deleteFrameCondition['auditFields.deletedBy'] = user.id;
            const result = await frameDAO.updateFrameId({ 'store.userId': frameData.frameId }, deleteFrameCondition);
            return result;
        } catch (error) {
            return Promise.reject(res.error(constant.HTML_STATUS_CODE.INTERNAL_ERROR, error.message, error.stack));
        }
    },
};


module.exports = frameIdService;