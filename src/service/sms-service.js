const _ = require('lodash');
const mongo = require('mongodb');
const constant = require('./../utils/constant');
const res = require('../utils/custom-response');
const request = require('request');

const SMSService = {

    sendSMS(methodName, bodyData) {
        // if (process.env.NODE_ENV === 'prod') {
            return new Promise((resolve, reject) => {
                var options = {
                    method: 'POST',
                    url: constant.SMS_CREDENTIALS.base_url,
                    headers:
                    {
                        'apiKey': constant.SMS_CREDENTIALS.apiKey,
                        'cache-control': 'no-cache',
                        'content-type': 'application/x-www-form-urlencoded'
                    },
                    form:
                    {
                        senderId: constant.SMS_CREDENTIALS.senderId,
                        sendMethod: 'simpleMsg',
                        msgType: 'text',
                        mobile: bodyData.mobileNos,
                        msg: bodyData.message,
                        duplicateCheck: 'true',
                        format: 'json'
                    }
                };
                // resolve({data:"dsfsdfds"});
                request(options, function (error, response, body) {
                    console.log(methodName + ' Send SMS', bodyData.mobileNos);
                    if (error) reject(res.error(constant.HTML_STATUS_CODE.INTERNAL_ERROR, error.message, error.stack));

                    resolve(body);
                });
            })
        // } else {
        //     return false;
        // }


    }
}

module.exports = SMSService;