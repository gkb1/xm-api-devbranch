
const _ = require('lodash');
const constant = require('../utils/constant');
const res = require('../utils/custom-response');
const productDAO = require('../dao/product-dao');
const utils = require('./../utils/utilities');
const categoryDAO = require('../dao/category-dao');
const mongoose = require('mongoose');
const ObjectId = mongoose.Types.ObjectId;
const frameDAO = require('../dao/frameId-dao');
const userDAO = require('../dao/user-dao');

const ProductService = {
    async addProduct(req, user) {
        try {
            if (req == null) {
                return Promise.reject(res.error(constant.HTML_STATUS_CODE.INVALID_url, 'Invalid url!.'));
            }
            if (user.role === constant.lookup.ECP_CUSTOMER_ROLE) {
                const userObj = await userDAO.getOne({ userId: user.userId });
                if (!userObj) {
                    return Promise.reject(res.error(constant.HTML_STATUS_CODE.INVALID_DATA, 'Invalid User'));
                }
                if (!userObj.permission || !userObj.permission.isAllowedToCreatePrdt) {
                    return Promise.reject(res.error(constant.HTML_STATUS_CODE.INVALID_DATA, 'Not allowed to create !! Please contact system admin!'));
                }
            }
            req.body['productId'] = utils.getRandomString(6);
            let checkUnique = await productDAO.getOne({ "productId": req['productId'] });
            while (checkUnique) {
                req.body['productId'] = utils.getRandomString(6);
                checkUnique = await productDAO.getOne({ "productId": req['productId'] });
            }
            req.body.model = JSON.parse(req.body.model);
            req.body.brand = JSON.parse(req.body.brand);
            req.body.pricing = JSON.parse(req.body.pricing);
            req.body.category = JSON.parse(req.body.category);
            req.body.product_info = JSON.parse(req.body.product_info);
            req.body.frame_attributes = JSON.parse(req.body.frame_attributes);
            req.body.prdt_attributes = JSON.parse(req.body.prdt_attributes);
            if (req.body.pricing) {
                if (!req.body.pricing.actualPrice) {
                    req.body.pricing.actualPrice = req.body.pricing.sellingPrice;
                }
            }

            if (req.body.frame_attributes && req.body.frame_attributes.length > 0) {
                _.forEach(req.body.frame_attributes, frameInfo => {
                    if (!frameInfo._id) {
                        frameInfo._id = new ObjectId();
                    }
                })
            }

            if (req.body.prdt_attributes && req.body.prdt_attributes.length > 0) {
                _.forEach(req.body.prdt_attributes, prdt_attributeinfo => {
                    if (!prdt_attributeinfo.colour['cover_img'] || prdt_attributeinfo.colour['cover_img'] === '') {
                        delete prdt_attributeinfo.colour['cover_img'];
                    }
                    if (!prdt_attributeinfo._id) {
                        prdt_attributeinfo._id = new ObjectId();
                    }
                })
            }

            if (req.body.product_info && req.body.product_info.length > 0) {
                _.forEach(req.body.product_info, prdtInfo => {
                    if (!prdtInfo._id) {
                        prdtInfo._id = new ObjectId();
                    }
                })
            }
            //GENERATE SKU
            let SKU = {};
            SKU = await this.genSKU(req.body);
            if (!req.body.SKU) {
                req.body.SKU = SKU.product.productSKU;
            }
            console.log('SKU:', SKU);
            if (req.body.prdt_attributes) {
                if (req.body.prdt_attributes.length > 0) {
                    req.body.prdt_attributes['prdAttributeId'] = utils.getRandomString(6);
                    _.forEach(req.body.prdt_attributes, async (product, index) => {
                        if (product.colour) {
                            if (product.colour.media && product.colour.media.length > 0) {
                                product.colour.media = _.filter(product.colour.media, function (x) { return x.url !== null; });
                            }
                            SKU.colour = product.colour.digitCode;
                            // product.colour['SKU'] = req.body['SKU'] + (index + 1);
                            product.colour['SKU'] = SKU.product.productSKU + SKU.colour + SKU.frame.frameSKU;
                            console.log('COLOUR SKU: ', product.colour['SKU']);
                            if (!product.colour['frameId']) {
                                product.colour['frameId'] = utils.getRandomString(6);
                            }
                            product.colour['prdColorId'] = utils.getRandomString(6);
                            if (product.colour.store.length > 0) {
                                _.forEach(product.colour.store, async (store) => {
                                    SKU.store_code = store.store_code;
                                })
                            }
                            if (product.colour.size && product.colour.size.length > 0) {
                                _.forEach(product.colour.size, async (prodSize, j) => {
                                    SKU.size = prodSize.digitCode;
                                    prodSize['prdSizeId'] = utils.getRandomString(6);
                                    // size['SKU'] = product.colour['SKU'] + (j + 1);
                                    prodSize['SKU'] = SKU.product.productSKU +
                                        SKU.colour +
                                        SKU.frame.frameSKU +
                                        SKU.size +
                                        SKU.features +
                                        SKU.height +
                                        (SKU.store_code ? SKU.store_code : '') +
                                        SKU.actualPrice;
                                    console.log('SIZE SKU:   ', prodSize['SKU']);
                                })
                            }
                        }
                    });
                }
            }

            const result = await productDAO.create(req.body);
            return Promise.resolve(true)
        } catch (error) {
            return Promise.reject(res.error(constant.HTML_STATUS_CODE.INTERNAL_ERROR, error.message, error.stack));
        }
    },
    async getProductById(prodId) {
        try {
            // if (loggedInUser == null || prodId == null) {
            //     return Promise.reject(res.error(constant.HTML_STATUS_CODE.INVALID_url, 'Invalid url!.'));
            // }
            const result = await productDAO.getOne({ productId: prodId }, null);
            // if (loggedInUser.role === constant.lookup.CUSTOMER_ROLE) {
            //     const viewCount = result['view_count'] + 1;
            //     result = await productDAO.update(prodId, { view_count: viewCount });
            // }
            // if (result == null) {
            //     return Promise.reject(res.error(constant.HTML_STATUS_CODE.INVALID_url, `No product found.`));
            // }
            return result;
        } catch (error) {
            return Promise.reject(res.error(constant.HTML_STATUS_CODE.INTERNAL_ERROR, error.message, error.stack));
        }
    },
    async listAllProducts(bodyData) {
        try {
            if (bodyData == null) {
                return Promise.reject(res.error(constant.HTML_STATUS_CODE.INVALID_url, 'Invalid url!.'));
            }
            let limit;
            let skip;
            let page;
            let pagination = bodyData.pagination;
            if (pagination) {
                limit = isNaN(Number(pagination.limit)) ? 10 : Number(pagination.limit);
                page = Number(pagination.page) || 1;
                skip = (pagination.page ? pagination.limit * (pagination.page - 1) : 0);
            } else {
                limit = 30; // default
                skip = 0;
            }
            let catdetails;
            if (bodyData.conditions.childCatName && !bodyData.conditions.catName) {
                return Promise.reject(res.error(constant.HTML_STATUS_CODE.INVALID_DATA, 'Invalid Category Name !!'));
            }
            if (bodyData.conditions.catName) { // products by category
                const catdetails = await categoryDAO.getOne({ isRoot: true, name: { $regex: new RegExp('^' + bodyData.conditions.catName + '$', "i") } }, null);
                if (catdetails) {
                    bodyData.conditions['category._id'] = catdetails._id;
                    delete bodyData.conditions.catName;
                    if (bodyData.conditions.childCatName) {
                        const childCatdetails = await categoryDAO.getOne({ path: catdetails.path + '_' + bodyData.conditions.childCatName.toLowerCase(), name: { $regex: new RegExp('^' + bodyData.conditions.childCatName + '$', "i") } }, null);
                        if (childCatdetails) {
                            bodyData.conditions['category.childCategories._id'] = childCatdetails._id;
                        }
                        delete bodyData.conditions.childCatName;
                    }
                }
            }
            const result = await productDAO.getAll(limit, skip, bodyData.sort, bodyData.conditions);
            return result;
        } catch (error) {
            return Promise.reject(res.error(constant.HTML_STATUS_CODE.INTERNAL_ERROR, error.message, error.stack));
        }
    },
    async listAllProductsAttributer(bodyData) {
        try {
            if (bodyData == null) {
                return Promise.reject(res.error(constant.HTML_STATUS_CODE.INVALID_url, 'Invalid url!.'));
            }
            const result = await productDAO.getAllPrdAttributes(bodyData);
            if (result == null) {
                return Promise.reject(res.error(constant.HTML_STATUS_CODE.INVALID_DATA, `No product found.`));
            }
            return result;
        } catch (error) {
            return Promise.reject(res.error(constant.HTML_STATUS_CODE.INTERNAL_ERROR, error.message, error.stack));
        }
    },
    async updateProduct(prodId, prodDetails) {
        try {
            if (prodDetails == null || prodId == null) {
                return Promise.reject(res.error(constant.HTML_STATUS_CODE.INVALID_DATA, 'Invalid url!.'));
            }
            const selProduct = await productDAO.getOne({ productId: prodId });
            if (selProduct == null) {
                return Promise.reject(res.error(constant.HTML_STATUS_CODE.INVALID_DATA, `No product found.`));
            }
            if (!prodDetails.productId) {
                prodDetails.productId = prodId;
            }
            if (prodDetails.frame_attributes && prodDetails.frame_attributes.length > 0) {
                _.forEach(prodDetails.frame_attributes, frameInfo => {
                    if (!frameInfo._id) {
                        frameInfo._id = new ObjectId();
                    }
                })
            }
            if (prodDetails.product_info && prodDetails.product_info.length > 0) {
                _.forEach(prodDetails.product_info, prdtInfo => {
                    if (!prdtInfo._id) {
                        prdtInfo._id = new ObjectId();
                    }
                })
            }
            //GENERATE SKU
            let SKU = {};
            SKU = await this.genSKU(prodDetails);

            if (prodDetails.prdt_attributes) {
                if (prodDetails.prdt_attributes.length > 0) {
                    _.forEach(prodDetails.prdt_attributes, async (product, index) => {
                        if (!product['_id']) {
                            product['_id'] = new ObjectId();
                        }
                        if (!product['prdAttributeId']) {
                            product['prdAttributeId'] = utils.getRandomString(6);
                        }
                        if (!product.colour['cover_img'] || product.colour['cover_img'] !== '') {
                            delete product.colour['cover_img'];
                        }
                        if (product.colour) {
                            if (!product.colour['SKU']) {
                                // product.colour['SKU'] = prodDetails['SKU'] + (index + 1);
                                SKU.colour = product.colour.digitCode;
                                product.colour['SKU'] = SKU.product.productSKU + SKU.colour + SKU.frame.frameSKU;
                            }
                            if (!product.colour['frameId']) {
                                product.colour['frameId'] = utils.getRandomString(6);
                            }
                            if (!product.colour['prdColourId']) {
                                product.colour['prdColourId'] = utils.getRandomString(6);
                            }
                            if (product.colour.media && product.colour.media.length > 0) {
                                product.colour.media = _.filter(product.colour.media, function (x) { return x.url !== null; });
                            }
                            if (product.colour.size && product.colour.size.length > 0) {
                                _.forEach(product.colour.size, async (prodSize, j) => {
                                    if (!prodSize['prdSizeId']) {
                                        prodSize['prdSizeId'] = utils.getRandomString(6);
                                    }
                                    if (!prodSize['_id']) {
                                        prodSize['_id'] = new ObjectId();
                                    }
                                    if (!prodSize['SKU']) {
                                        // size['SKU'] = product.colour['SKU'] + (j + 1);
                                        SKU.size = prodSize.digitCode;
                                        prodSize['SKU'] = SKU.product.productSKU +
                                            SKU.colour +
                                            SKU.frame.frameSKU +
                                            SKU.size +
                                            SKU.features +
                                            SKU.height +
                                            (SKU.store_code ? SKU.store_code : '') +
                                            SKU.actualPrice;
                                    }
                                })
                            }
                        }
                    });
                }
            }
            const result = await productDAO.update(prodId, prodDetails);
            return result;
        } catch (error) {
            return Promise.reject(res.error(constant.HTML_STATUS_CODE.INTERNAL_ERROR, error.message, error.stack));
        }
    },
    async updateGKBPrice(prodId, prodDetails, user) {
        try {
            if (!user) {
                return Promise.reject(res.error(constant.HTML_STATUS_CODE.UNAUTHORIZED, 'Unauthorised !!.'));
            }
            if (user.role !== constant.lookup.SUPERADMIN_ROLE && user.role !== constant.lookup.ADMIN_ROLE && user.role !== constant.lookup.FRAME_MODERATOR_ROLE) {
                return Promise.reject(res.error(constant.HTML_STATUS_CODE.UNAUTHORIZED, 'Unauthorized Access.'));
            }
            if (prodDetails == null || prodId == null) {
                return Promise.reject(res.error(constant.HTML_STATUS_CODE.INVALID_DATA, 'Invalid url!.'));
            }
            const selProduct = await productDAO.getOne({ productId: prodId });
            if (selProduct == null) {
                return Promise.reject(res.error(constant.HTML_STATUS_CODE.INVALID_DATA, `No product found.`));
            }
            const result = await productDAO.update(prodId, prodDetails);
            return result;
        } catch (error) {
            return Promise.reject(res.error(constant.HTML_STATUS_CODE.INTERNAL_ERROR, error.message, error.stack));
        }
    },
    async updateProductAttribute(user, bodyData) {
        try {
            if (!user) {
                return Promise.reject(res.error(constant.HTML_STATUS_CODE.UNAUTHORIZED, 'Unauthorised !!.'));
            }
            if (!bodyData || !bodyData.conditions || !bodyData.conditions.productId) {
                return Promise.reject(res.error(constant.HTML_STATUS_CODE.INVALID_DATA, 'Invalid url!.'));
            }
            let product;
            if (bodyData.conditions.prdAttributeId && bodyData.conditions.prdAttributeId === '') {
                bodyData.conditions.prdAttributeId = null;
            }
            if (bodyData.conditions.prdAttributeId) {
                product = await productDAO.findOnePrdAttribute({ productId: bodyData.conditions.productId, 'prdt_attributes.prdAttributeId': bodyData.conditions.prdAttributeId });
            } else {
                product = await productDAO.getOne({ productId: bodyData.conditions.productId });
            }
            if (product == null) {
                return Promise.reject(res.error(constant.HTML_STATUS_CODE.INVALID_DATA, `No product found.`));
            }
            if (!bodyData.conditions.prdAttributeId) {
                // Create/Push Store into  prodAttribute Colour of store
                const prod = await productDAO.getOne({ productId: bodyData.conditions.productId });
                if (bodyData.updateFields) {
                    // if (bodyData.updateFields.prdt_attributes.length > 0) {
                    // _.forEach(prod.prdt_attributes, async (product, index) => {
                    if (!bodyData.updateFields['_id']) {
                        bodyData.updateFields['_id'] = new ObjectId();
                    }
                    if (!bodyData.updateFields['prdAttributeId']) {
                        bodyData.updateFields['prdAttributeId'] = utils.getRandomString(6);
                    }
                    if (bodyData.updateFields.colour && bodyData.updateFields.colour.size && bodyData.updateFields.colour.size.length > 0) {
                        if (bodyData.updateFields.colour.media && bodyData.updateFields.colour.media.length > 0) {
                            bodyData.updateFields.colour.media = _.filter(bodyData.updateFields.colour.media, function (p) {
                                return _.includes(p.url);
                            });
                            if (!bodyData.updateFields.colour['SKU']) {
                                bodyData.updateFields.colour['SKU'] = prod['SKU'] + (prod.prdt_attributes.length + 1);
                            }
                            if (!bodyData.updateFields.colour['frameId']) {
                                bodyData.updateFields.colour['frameId'] = utils.getRandomString(6);
                            }
                            if (!bodyData.updateFields.colour['prdColourId']) {
                                bodyData.updateFields.colour['prdColourId'] = utils.getRandomString(6);
                            }
                            _.forEach(bodyData.updateFields.colour.size, async (size, j) => {
                                if (!size['prdSizeId']) {
                                    size['prdSizeId'] = utils.getRandomString(6);
                                }
                                if (!size['SKU']) {
                                    size['SKU'] = bodyData.updateFields.colour['SKU'] + (j + 1);
                                }
                            })
                        }
                    }
                    // });
                    // }
                }
                const result = await productDAO.updateProdAttribute(bodyData.conditions.productId, bodyData.updateFields);
            } else {
                const storeIdIndex = product.prdt_attributes[0].colour.store.map(function (x) { return x._id; }).indexOf(bodyData.updateFields.colour.store[0]._id);
                if (storeIdIndex > -1) { // checking all existing store
                    return Promise.reject(res.error(constant.HTML_STATUS_CODE.INVALID_DATA, `Product Already added to store !!`));
                }
                // Update / Assign store to specified colour
                const result = await productDAO.updateSelProductsAttribute({ productId: bodyData.conditions.productId, prdAttributeId: bodyData.conditions.prdAttributeId }, bodyData.updateFields);
            }
            return Promise.resolve(true);
        } catch (error) {
            return Promise.reject(res.error(constant.HTML_STATUS_CODE.INTERNAL_ERROR, error.message, error.stack));
        }
    },
    async deleteProduct(prodId, user) {
        try {
            if (prodId == null) {
                return Promise.reject(res.error(constant.HTML_STATUS_CODE.INVALID_DATA, 'Invalid url!.'));
            }
            const objId = await productDAO.getOne({ productId: prodId });
            if (objId == null) {
                return Promise.reject(res.error(constant.HTML_STATUS_CODE.INVALID_DATA, `No product found.`));
            }
            const deleteProductCondition = {}
            deleteProductCondition['auditFields.isDeleted'] = true;
            deleteProductCondition['auditFields.deletedBy'] = user.id;
            const result = await productDAO.updateByCondition({ productId: prodId }, deleteProductCondition);
            return result;
        } catch (error) {
            return Promise.reject(res.error(constant.HTML_STATUS_CODE.INTERNAL_ERROR, error.message, error.stack));
        }
    },
    async approvOrReject(prodId, prodDetails, user) {
        try {
            if (user.role === constant.lookup.ADMIN_ROLE) {
                if (prodDetails == null || prodId == null) {
                    return Promise.reject(res.error(constant.HTML_STATUS_CODE.INVALID_DATA, 'Invalid url!.'));
                }
                const objId = await productDAO.getOne({ productId: prodId });
                if (objId == null) {
                    return Promise.reject(res.error(constant.HTML_STATUS_CODE.INVALID_DATA, `No product found.`));
                }
                if (prodDetails.isApproved) {
                    const result = await productDAO.updateByCondition({ productId: prodId }, prodDetails);
                    if (result.n > 0 || result.nModified > 0) {
                        return ({ message: "Product Approved!" });
                    }
                } else {
                    const result = await productDAO.updateByCondition({ productId: prodId }, prodDetails);
                    if (result.n > 0 || result.nModified > 0) {
                        return ({ message: "Product rejected!" });
                    }
                }
            } else if (user.role == constant.lookup.ECP_CUSTOMER_ROLE) {
                if (prodDetails == null || prodId == null) {
                    return Promise.reject(res.error(constant.HTML_STATUS_CODE.INVALID_DATA, 'Invalid url!.'));
                }
                const objId = await productDAO.getById(prodId);
                if (objId == null) {
                    return Promise.reject(res.error(constant.HTML_STATUS_CODE.INVALID_DATA, `No product found.`));
                }
                if (prodDetails.isActivate) {
                    const result = await productDAO.updateByCondition({ productId: prodId }, prodDetails);
                    if (result.n > 0 || result.nModified > 0) {
                        return ({ message: "Product Activated!" });
                    }
                } else {
                    const result = await productDAO.updateByCondition({ productId: prodId }, prodDetails);
                    if (result.n > 0 || result.nModified > 0) {
                        return ({ message: "Product De-activated!" });
                    }
                }
                return Promise.reject(res.error(constant.HTML_STATUS_CODE.UNAUTHORIZED, 'Access denied!.'));
            }
            else {
                return Promise.reject(res.error(constant.HTML_STATUS_CODE.UNAUTHORIZED, 'Access denied!.'));
            }
        } catch (error) {
            return Promise.reject(res.error(constant.HTML_STATUS_CODE.INTERNAL_ERROR, error.message, error.stack));
        }
    },
    async getTrendingProduct(loggedInUser) {
        try {
            if (loggedInUser == null) {
                return Promise.reject(res.error(constant.HTML_STATUS_CODE.INVALID_url, 'access denied!.'));
            }
            const result = await productDAO.getTrendingProduct();
            if (result == null) {
                return Promise.reject(res.error(constant.HTML_STATUS_CODE.INVALID_url, `No product found.`));
            }
            return result;
        } catch (error) {
            return Promise.reject(res.error(constant.HTML_STATUS_CODE.INTERNAL_ERROR, error.message, error.stack));
        }
    },
    async getProductByFrameId(frameId) {
        try {
            if (frameId == null) {
                return Promise.reject(res.error(constant.HTML_STATUS_CODE.INVALID_DATA, 'inavalid data!'));
            }
            const result = await productDAO.getByFrameId(frameId);
            // if (result == null) {
            //     return Promise.reject(res.error(constant.HTML_STATUS_CODE.INVALID_DATA, `No product found.`));
            // }
            return result;
        } catch (error) {
            return Promise.reject(res.error(constant.HTML_STATUS_CODE.INTERNAL_ERROR, error.message, error.stack));
        }
    },
    async removeStore(user, storeData) {
        try {
            if (!user) {
                return Promise.reject(res.error(constant.HTML_STATUS_CODE.INVALID_url, 'Unauthorised !!'));
            }
            if (storeData == null) {
                return Promise.reject(res.error(constant.HTML_STATUS_CODE.INVALID_DATA, 'Invalid url!.'));
            }
            const objId = await productDAO.getOne({ productId: storeData.productId });
            if (objId == null) {
                return Promise.reject(res.error(constant.HTML_STATUS_CODE.INVALID_DATA, `No store found.`));
            }
            const result = await productDAO.removeStore(storeData);
            return result;
        } catch (error) {
            return Promise.reject(res.error(constant.HTML_STATUS_CODE.INTERNAL_ERROR, error.message, error.stack));
        }
    },
    async searchByTagNo(user, params) {
        try {
            if (!user) {
                return Promise.reject(res.error(constant.HTML_STATUS_CODE.INVALID_DATA, 'Invalid User'));
            }
            if (user.role !== constant.lookup.SUPERADMIN_ROLE && user.role !== constant.lookup.ADMIN_ROLE && user.role !== constant.lookup.FRAME_MODERATOR_ROLE) {
                return Promise.reject(res.error(constant.HTML_STATUS_CODE.UNAUTHORIZED, 'Unauthorized Access.'));
            }
            if (!params || !params.tagNo) {
                return Promise.reject(res.error(constant.HTML_STATUS_CODE.INVALID_DATA, 'Invalid Payload !!'));
            }
            const frameResult = await frameDAO.getSelFrameByFrameId({ 'store.frame.tagName': params.tagNo });
            if (!frameResult || !frameResult.store || frameResult.store.frame.length <= 0) {
                return Promise.reject(res.error(constant.HTML_STATUS_CODE.INVALID_DATA, `Invalid Tag No !!`));
            }
            // const isFrameApproved = await frameDAO.getSelFrameByFrameId({ 'store.frame.tagName': params.tagNo, 'store.frame.status.isApproved': true });
            // if (!isFrameApproved) {
            //     return Promise.reject(res.error(constant.HTML_STATUS_CODE.INVALID_DATA, 'Tag No ' + params.tagNo + ' is not yet approved!!'));
            // }
            // if (frameResult.store.frame[0]) {
            //     if (!frameResult.store.frame[0].status.isApproved) {
            //         return Promise.reject(res.error(constant.HTML_STATUS_CODE.INVALID_DATA, 'Tag No ' + params.tagNo + ' is not yet approved!!'));
            //     }
            // }
            const result = await productDAO.getSelPrdtAttr({ 'prdt_attributes.colour.frameId': frameResult.store.frame[0].frameID });
            let isPhotographed = false;
            if (result && result.prdt_attributes[0] && result.prdt_attributes[0].colour) {
                if (result.prdt_attributes[0].colour.cover_img || (result.prdt_attributes[0].colour.media.length > 0)) {
                    isPhotographed = true;
                    if (result.prdt_attributes[0].colour.media.length > 0) {
                        const media = _.filter(result.prdt_attributes[0].colour.media, function (x) { return x.url !== null && x.url !== ''; });
                        if (!media || media.length <= 0) {
                            isPhotographed = false;
                        }
                    } 
                }
            }
            return Object.assign({ product: result },{ store: frameResult.store },{ isPhotographed: isPhotographed });
        } catch (error) {
            return Promise.reject(res.error(constant.HTML_STATUS_CODE.INTERNAL_ERROR, error.message, error.stack));
        }
    },
    async genSKU(skuDetails) {
        const sku = {
            product: {},
            frame: {}
        }
        // PRODUCT CATEGORY 
        if (skuDetails.category && skuDetails.category.childCategories) {
            const category = await categoryDAO.getOne({ categoryId: skuDetails.category.childCategories.categoryId });
            let catName = category.path.split('_');
            catName[catName.length - 1] === 'sunglasses' ? sku.product['prodCategory'] = '2' : sku.product['prodCategory'] = '1';
        }
        // GENDER
        switch (skuDetails.gender.toLowerCase()) {
            case 'men':
                sku.product['genderCategory'] = 1;
                break;
            case 'women':
                sku.product['genderCategory'] = 2;
                break;
            case 'kids':
                sku.product['genderCategory'] = 3;
                break;
            case 'unisex':
                sku.product['genderCategory'] = 4;
                break;
        }
        // PRICE
        parseInt(skuDetails.pricing.actualPrice) <= 2000
            ? (sku.product['priceRange'] = 1)
            : (parseInt(skuDetails.pricing.actualPrice) > 2001 && parseInt(skuDetails.pricing.actualPrice) <= 5000)
                ? (sku.product['priceRange'] = 2)
                : (sku.product['priceRange'] = 3);
        // BRAND
        sku.product['brand'] = skuDetails.brand.code;
        // MODEL
        sku.product['model'] = skuDetails.model.code;
        // PRODUCT ATTRIBUTE SKU
        sku.product['productSKU'] = sku.product.prodCategory + sku.product['priceRange'] + sku.product['genderCategory'] + sku.product['brand'] + sku.product['model'];
        _.forEach(skuDetails.frame_attributes, frame => {
            switch (frame.key_name) {
                case 'Frame Material':
                    sku.frame['material'] = frame.digitCode;
                    break;
                case 'Frame Type':
                    sku.frame['type'] = frame.digitCode
                    break;
                case 'Frame Shape':
                    sku.frame['shape'] = frame.digitCode
                    break;
            }
        })
        // FRAME ATTRIBUTE SKU 
        sku.frame['frameSKU'] = sku.frame['type'] + sku.frame['material'] + sku.frame['shape'];
        sku['features'] = 1;
        sku['height'] = 1;
        sku['actualPrice'] = await utils.getActulaPriceCode(skuDetails.pricing.actualPrice);
        sku.store_code = skuDetails.storeId;
        sku.generatedSku = sku.product.productSKU +
            // sku.colour +
            sku.frame.frameSKU +
            // size +
            sku.features +
            sku.height +
            (sku.store_code ? sku.store_code : '') +
            sku.actualPrice;
        return sku;
    },

    async generateSKU(skuDetails) {
        try {
            const product = await productDAO.getOne({ productId: skuDetails.productId });
            const generatedSku = await this.genSKU(product);
            // COLOUR
            const color = await productDAO.findOnePrdAttribute({ 'prdt_attributes.prdAttributeId': skuDetails.prdAttributeId });
            const sizes = [];
            if (color && color.prdt_attributes) {
                generatedSku.colour = color.prdt_attributes[0].colour.digitCode;
                // SIZE
                _.forEach(color.prdt_attributes[0].colour.size, size => {
                    if (size.isSelected) {
                        sizes.push(size.digitCode)
                    }
                })
            }
            const SKU = [];
            generatedSku.store_code = skuDetails.storeId;
            _.forEach(sizes, size => {
                const finalSku = generatedSku.product.productSKU +
                    generatedSku.colour +
                    generatedSku.frame.frameSKU +
                    size +
                    generatedSku.features +
                    generatedSku.height +
                    generatedSku.store_code +
                    generatedSku.actualPrice;
                SKU.push(finalSku)
            })
            const skuUpdated = await productDAO.updateSKUToStore(skuDetails, SKU);
            return skuUpdated;
        } catch (error) {
            return Promise.reject(res.error(constant.HTML_STATUS_CODE.INTERNAL_ERROR, error.message, error.stack));
        }
    },
    async getAllTagNamesToVerifySKU(bodyData) {
        try {
            if (bodyData == null) {
                return Promise.reject(res.error(constant.HTML_STATUS_CODE.INVALID_url, 'Invalid url!.'));
            }
            let limit;
            let skip;
            let page;
            let pagination = bodyData.pagination;
            if (pagination) {
                limit = isNaN(Number(pagination.limit)) ? 10 : Number(pagination.limit);
                page = Number(pagination.page) || 1;
                skip = (pagination.page ? pagination.limit * (pagination.page - 1) : 0);
            } else {
                limit = 30; // default
                skip = 0;
            }
            if (bodyData.conditions.tagNo) {
                bodyData.conditions['prdt_attributes.colour.store.tagName'] = { $regex: new RegExp(bodyData.conditions.tagNo, "ig") };
                delete bodyData.conditions.tagNo;
            }
            const result = await productDAO.getAllTagNamesToVerifySKU(limit, skip, bodyData.sort, bodyData.conditions);
            return result;
        } catch (error) {
            return Promise.reject(res.error(constant.HTML_STATUS_CODE.INTERNAL_ERROR, error.message, error.stack));
        }
    },
};


module.exports = ProductService;