var admin = require('firebase-admin');
var fcmConfig = require('../utils/fcmConfig.json');

admin.initializeApp({
    credential: admin.credential.cert(fcmConfig),
    databaseURL: "https://gkb-pay-83062.firebaseio.com"
  })

module.exports.sendNotification = function (data, req, res) {    
    const messaging = admin.messaging();
    return messaging.sendToDevice(data.token,data.payload , data.options)
}
