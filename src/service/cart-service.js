
const _ = require('lodash');
const constant = require('../utils/constant');
const res = require('../utils/custom-response');
const utils = require('../utils/utilities');
const cartDAO = require('../dao/cart-dao');

const cartService = {
    async createCart(req, user) {
        try {
            if (req == null) {
                return Promise.reject(res.error(constant.HTML_STATUS_CODE.INVALID_url, 'Invalid url!.'));
            }
            req.body['cartId'] = utils.getRandomString(6);
            req.body['userId'] = user._id;
            let checkUnique = await cartDAO.getOne({ "cartId": req['cartId'] });
            while (checkUnique) {
                req.body['cartId'] = utils.getRandomString(6);
                checkUnique = await cartDAO.getOne({ "cartId": req['cartId'] });
            }
            const result = await cartDAO.create(req.body);
            return Promise.resolve(result);
        } catch (error) {
            return Promise.reject(res.error(constant.HTML_STATUS_CODE.INTERNAL_ERROR, error.message, error.stack));
        }
    },
    async getCartItemById(loggedInUser, cartId) {
        try {
            if (loggedInUser == null || cartId == null) {
                return Promise.reject(res.error(constant.HTML_STATUS_CODE.INVALID_url, 'Invalid url!.'));
            }
            const result = await cartDAO.getOne({ cartId: cartId });
            if (result == null) {
                return Promise.reject(res.error(constant.HTML_STATUS_CODE.INVALID_url, `No cart found.`));
            }
            return result;
        } catch (error) {
            return Promise.reject(res.error(constant.HTML_STATUS_CODE.INTERNAL_ERROR, error.message, error.stack));
        }
    },
    async listAllCartItems(cartData) {
        try {
            if (cartData == null) {
                return Promise.reject(res.error(constant.HTML_STATUS_CODE.INVALID_url, 'Invalid url!.'));
            }
            let limit;
            let skip;
            let page;
            let pagination = cartData.pagination;
            if (pagination) {
                limit = isNaN(Number(pagination.limit)) ? 10 : Number(pagination.limit);
                page = Number(pagination.page) || 1;
                skip = (pagination.page ? pagination.limit * (pagination.page - 1) : 0);
            } else {
                limit = 30; // default
                skip = 0;
            }
            cartData.conditions['auditFields.isDeleted'] = false;
            const result = await cartDAO.getAll(limit, skip, cartData.sort, cartData.conditions);
            return result;
        } catch (error) {
            return Promise.reject(res.error(constant.HTML_STATUS_CODE.INTERNAL_ERROR, error.message, error.stack));
        }
    },
    async updateCart(params, prodDetails) {
        try {
            if (prodDetails == null || params == null) {
                return Promise.reject(res.error(constant.HTML_STATUS_CODE.INVALID_DATA, 'Invalid url!.'));
            }
            const objId = await cartDAO.getOne({ cartId: params.cartId });
            if (objId == null) {
                return Promise.reject(res.error(constant.HTML_STATUS_CODE.INVALID_DATA, `No cart found.`));
            }
            const result = await cartDAO.updateCart({ cartId: params.cartId, productId: params.productId }, prodDetails);
            return result;
        } catch (error) {
            return Promise.reject(res.error(constant.HTML_STATUS_CODE.INTERNAL_ERROR, error.message, error.stack));
        }
    },

    async deleteCart(cartData, user) {
        try {
            if (cartData == null) {
                return Promise.reject(res.error(constant.HTML_STATUS_CODE.INVALID_DATA, 'Invalid url!.'));
            }
            const objId = await cartDAO.getOne({ cartId: cartData.cartId });
            if (objId == null) {
                return Promise.reject(res.error(constant.HTML_STATUS_CODE.INVALID_DATA, `No cart found.`));
            }
            const result = await cartDAO.removeItemFormCart(cartData.productId);
            return result;
        } catch (error) {
            return Promise.reject(res.error(constant.HTML_STATUS_CODE.INTERNAL_ERROR, error.message, error.stack));
        }
    },
};

module.exports = cartService;